KDE Project Security Advisory
=============================

Title:          kauth: Insecure handling of arguments in helpers
Risk Rating:    Medium
CVE:            CVE-2019-7443
Versions:       KDE Frameworks < 5.55.0
Date:           9 February 2019


Overview
========
KAuth allows to pass parameters with arbitrary types to helpers running as root
over DBus. Certain types can cause crashes and trigger decoding arbitrary
images with dynamically loaded plugins.

Solution
========
Update to kauth >= 5.55.0

Or apply the following patch to kauth:
https://commits.kde.org/kauth/fc70fb0161c1b9144d26389434d34dd135cd3f4a

Credits
=======
Thanks to Fabian Vogt for the report and Albert Astals Cid for the fix.

