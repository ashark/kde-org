KDE Security Advisory: XMLHttpRequest vulnerability and kioslave input validation issues
Original Release Date: 2009-10-27
URL: http://www.kde.org/info/security/advisory-20091027-1.txt

0. References
        oCert: #2009-015 http://www.ocert.org/advisories/ocert-2009-015.html
        CVE: N/A

1. Systems affected:

        KDE until 4.3.2 included.
        KDE 4.3.3 and newer is not affected.

2. Overview:

        Ark input sanitization errors:
        The KDE archiving tool, Ark, performs insufficient validation which
        leads to specially crafted archive files, using unknown MIME types, to be
        rendered using a KHTML instance, this can trigger uncontrolled XMLHTTPRequests
        to remote sites.

        IO Slaves input sanitization errors:
        KDE protocol handlers perform insufficient input validation, an attacker can
        craft malicious URI that would trigger JavaScript execution. Additionally the
        'help://' protocol handler suffer from directory traversal. It should be noted
        that the scope of this issue is limited as the malicious URIs cannot be embedded
        in Internet hosted content.

        KMail input sanitization errors:
        The KDE mail client, KMail, performs insufficient validation which leads to
        specially crafted email attachments, using unknown MIME types, to be rendered
        using a KHTML instance, this can trigger uncontrolled XMLHTTPRequests to remote
        sites.

3. Impact:

        The exploitation of these vulnerabilities is unlikely according to Portcullis
        and KDE but the execution of active content is nonetheless unexpected and might
        pose a threat.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.

5. Patch:

        A patch for the XMLHttpRequest issue for KDE 3.5.5 - KDE 3.5.10 
        is available from ftp://ftp.kde.org/pub/kde/security_patches :

        a2f49aa8879a7e33bd271236514e5aac  xmlhttprequest_3.x.diff

        Patches for KDE 4.x have been committed to subversion, and are
        available from websvn.kde.org:
        http://websvn.kde.org/?view=revision&revision=1035539
        http://websvn.kde.org/?view=revision&revision=1030579
        http://websvn.kde.org/?view=revision&revision=938003
