---
aliases:
- ../announce-applications-19.08-beta
date: 2019-07-19
description: KDE Ships Applications 19.08 Beta.
layout: application
release: applications-19.07.80
title: KDE Ships Beta of KDE Applications 19.08
version_number: 19.07.80
version_text: 19.08 Beta
---

July 19, 2019. Today KDE released the beta of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/19.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

The KDE Applications 19.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.