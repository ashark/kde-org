---
title: Plasma 5.24.7 complete changelog
version: 5.24.7
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Kcm: Copy model data into the "Forget…" dialog. [Commit.](http://commits.kde.org/bluedevil/3f8f9ebd6730bdadd8c8bcd318dc9c280b632687) Fixes bug [#459855](https://bugs.kde.org/459855)
+ Kcm: Port TextField to onTextEdited signal to avoid recursive changes. [Commit.](http://commits.kde.org/bluedevil/f8f66626310a9567fd1d67a769a4ef9f9dfd8c67) Fixes bug [#459853](https://bugs.kde.org/459853)
+ Install po folder. [Commit.](http://commits.kde.org/bluedevil/95bad93e6c8a3d8c1a4f31732208d2b9a04ff50b) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Kstyle: Fix class name to fix indeterminate progress bars animations. [Commit.](http://commits.kde.org/breeze/83f105fe7662f9c23eec0e0adb53ad5271a6aad6) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Install po folder. [Commit.](http://commits.kde.org/discover/4f2b510c21507856821b481b28475aaca9d1fe6c) 
+ Request inhibiting sleep while transactions are on. [Commit.](http://commits.kde.org/discover/6a5b862298989b66854abf05039618d629fe7f69) Fixes bug [#401304](https://bugs.kde.org/401304)
+ Do not change the sorting among resources in the default backend. [Commit.](http://commits.kde.org/discover/984dab22dce7854529bd501e8c090a8038e0148a) Fixes bug [#451667](https://bugs.kde.org/451667)
+ Fwupd: Make sure we don't crash trying to print an error. [Commit.](http://commits.kde.org/discover/f958ce562719ac80876677b49c1bab2bd158791d) Fixes bug [#455132](https://bugs.kde.org/455132)
+ Kns: Make KNSReview aware of its possible lack of AtticaProvider. [Commit.](http://commits.kde.org/discover/886862cce10ba31caa7dedc6c4598a2301ba56ac) Fixes bug [#457145](https://bugs.kde.org/457145)
+ AppListPage: Make sure the PlaceholderMessage doesn't get in the way. [Commit.](http://commits.kde.org/discover/72a72a45d97dc63ae1cfd7bdc2d12f8f1fe62c19) Fixes bug [#457029](https://bugs.kde.org/457029)
+ Pk: Allow some error codes from offline updates. [Commit.](http://commits.kde.org/discover/08a42e91009f57dd2024f94e427f82e2fa3a7e29) Fixes bug [#443090](https://bugs.kde.org/443090)
+ Odrs: Make sure we don't fail when the application page is opened early. [Commit.](http://commits.kde.org/discover/442c31f4cb50283dc6ad2f7fb0f2286a00b5a921) Fixes bug [#426270](https://bugs.kde.org/426270)
+ Fix submitting usefulness. [Commit.](http://commits.kde.org/discover/e74e0745bf233e02450469632e49a3cb1f416643) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Install po folder. [Commit.](http://commits.kde.org/drkonqi/c47f9b2319bbe9f3fd6b30a4676a8eccfe9f70fa) 
{{< /details >}}

{{< details title="kde-cli-tools" href="https://commits.kde.org/kde-cli-tools" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kde-cli-tools/ea3034e5783e803726af7204f41e924de916d219) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kdeplasma-addons/5c146466b33552ace4c5dc220e28875e293c874d) 
+ Applets/weather: Make desktop widget big enough to fit Configure button. [Commit.](http://commits.kde.org/kdeplasma-addons/d2b68d7671f66a398606f2cc873da732a05560ca) Fixes bug [#419591](https://bugs.kde.org/419591)
+ [applets/comic] Disable most context menu actions when comic is not ready. [Commit.](http://commits.kde.org/kdeplasma-addons/352ecf9aa151e842f5304b8f4dc1fa7c7689b32f) Fixes bug [#406991](https://bugs.kde.org/406991)
+ Remove clipPath portion of SVG. [Commit.](http://commits.kde.org/kdeplasma-addons/4fae3fa7c9a21d2fc6224ea48e10525330ae46ce) Fixes bug [#399568](https://bugs.kde.org/399568)
{{< /details >}}

{{< details title="Gamma Monitor Calibration Tool" href="https://commits.kde.org/kgamma5" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kgamma5/b6fbf847e4b39fa3f75c17559f955701700aa714) 
{{< /details >}}

{{< details title="KDE Hotkeys" href="https://commits.kde.org/khotkeys" >}}
+ Install po folder. [Commit.](http://commits.kde.org/khotkeys/1d4aec83c7389d88bdcb87b2f14792d6fa68c3a4) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kinfocenter/3bc19fda385cd33e229c12da148dabb9b418955d) 
{{< /details >}}

{{< details title="KMenuEdit" href="https://commits.kde.org/kmenuedit" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kmenuedit/11cce533cf50789394f845be9b488e8c68beb5ee) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: reduce default height to make it fit on short screens. [Commit.](http://commits.kde.org/kscreen/0bba6fffc81698be73d26157169dfd3c35676dce) 
+ Install po folder. [Commit.](http://commits.kde.org/kscreen/046300cab513d77cc80ff74a43b245999103094c) 
+ Kcm: Bring back accidentally removed function call. [Commit.](http://commits.kde.org/kscreen/dddf9ecc805b27297295c7e27b8945a6d34263b3) 
+ Don't compute aspect ratio for empty screen geometries. [Commit.](http://commits.kde.org/kscreen/a327e16950de9fbb352be01485fb0275298d9921) Fixes bug [#456235](https://bugs.kde.org/456235)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Install po folder. [Commit.](http://commits.kde.org/kscreenlocker/f2d1f40fe66cfe9341d8eda145a7a96248c3741b) 
{{< /details >}}

{{< details title="KSSHAskPass" href="https://commits.kde.org/ksshaskpass" >}}
+ Install po folder. [Commit.](http://commits.kde.org/ksshaskpass/b0444c7af72d6172fd422cb1845aea4209df3c37) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Revert "Install po folder". [Commit.](http://commits.kde.org/ksystemstats/3a8de4d04543fd913022402b3116dc952a66705b) 
+ Install po folder. [Commit.](http://commits.kde.org/ksystemstats/5d883fd3334db6959b8428e9f06f35ddd64a998d) 
+ Install po folder. [Commit.](http://commits.kde.org/ksystemstats/f5db8261ffaaf8a3004199e0ba1d8ac9cc1b01e4) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ [x11client] Remove duplicate activities check. [Commit.](http://commits.kde.org/kwin/8bfa26282b97066626594eaaf111f20e2b1e8ca6) 
+ Sync activities after kwin restart. [Commit.](http://commits.kde.org/kwin/16c6c3d84795257e1576a20775d8e15f09c5aad3) Fixes bug [#438312](https://bugs.kde.org/438312)
+ Install po folder. [Commit.](http://commits.kde.org/kwin/42c13346a361592d850c78bbc8c662f04f952efa) 
+ Effects/colorpicker: Fix off-by-one error during coordinates conversion. [Commit.](http://commits.kde.org/kwin/c778d82dc43f07bb3727d7d3dbab62e183c20427) 
+ Backends/drm: fix blob updating. [Commit.](http://commits.kde.org/kwin/3af37c8a29af36eb773b0684237d61b2bd6b81f9) Fixes bug [#449285](https://bugs.kde.org/449285)
+ Effects/colorpicker: Fix picking colors. [Commit.](http://commits.kde.org/kwin/aa5fc42349052d040f45aca40749edce80ff8f1f) Fixes bug [#454974](https://bugs.kde.org/454974)
+ [kcm/kwindesktop] Emit rowsChanged signal to fix default state highlighting. [Commit.](http://commits.kde.org/kwin/4305425cb221139a7d9ed6cb954eb8e0b5627f5a) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Install po folder. [Commit.](http://commits.kde.org/libksysguard/fce02868f07cabc3d038dd170de48d132bcc4f3d) 
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Install po folder. [Commit.](http://commits.kde.org/milou/d7d668ec89d132df98026a278fb4c9955b72461c) 
{{< /details >}}

{{< details title="Plasma Browser Integration" href="https://commits.kde.org/plasma-browser-integration" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-browser-integration/298d17ac950a70febdcf15d5d9894b57d428799d) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Remove the check for > 0. [Commit.](http://commits.kde.org/plasma-desktop/7e1dec42ba12cdbf768ba7fba613e837d645b02c) 
+ Use relayout locking. [Commit.](http://commits.kde.org/plasma-desktop/234cd860532449f017ecbbca6a8caad5473fcf8b) Fixes bug [#413645](https://bugs.kde.org/413645)
+ Install po folder. [Commit.](http://commits.kde.org/plasma-desktop/5047e34ff84f9dc47010a38e6b58a169d5b84c70) 
+ Desktoptoolbox: fix flickering on closing. [Commit.](http://commits.kde.org/plasma-desktop/51a36f960f48dd5c18fb73b53924dafcc7414e07) Fixes bug [#417849](https://bugs.kde.org/417849)
+ Applets/kickoff: fix crash when dragging items not in favorite page on Wayland. [Commit.](http://commits.kde.org/plasma-desktop/5b89b659fa8e5cd1eff36061ac19e1bf31c15cad) Fixes bug [#449426](https://bugs.kde.org/449426). Fixes bug [#450215](https://bugs.kde.org/450215)
+ Folderview: Fix grid overflow property so it properly reports overflow state. [Commit.](http://commits.kde.org/plasma-desktop/508a7b385f8747f326fab4f1843b2a24870b3961) Fixes bug [#419878](https://bugs.kde.org/419878)
+ Toolboxes: set default position to topcenter. [Commit.](http://commits.kde.org/plasma-desktop/14579be682ad9329bf5a27112d5e294b331cd8a2) See bug [#457814](https://bugs.kde.org/457814)
+ Taskmanager: Use proper QUrls for recent document actions. [Commit.](http://commits.kde.org/plasma-desktop/615c286f3be390fa6dc5eb39f139ddf7bf39c2f1) Fixes bug [#457685](https://bugs.kde.org/457685)
+ [desktop/package] Fix inconsistent press-to-enter-edit-mode behavior. [Commit.](http://commits.kde.org/plasma-desktop/52151c72e25bba286f860e2eb483a8c642bd9a14) Fixes bug [#456994](https://bugs.kde.org/456994)
+ Fix mouse settings not being loaded when a mouse is connected. [Commit.](http://commits.kde.org/plasma-desktop/0351cdfc9210f6f88863419534250ab7b8ce04ed) Fixes bug [#435113](https://bugs.kde.org/435113)
+ [applets/pager] Fix button group management on configuration page. [Commit.](http://commits.kde.org/plasma-desktop/fffa899345df967ed815104cf88de8466d97b374) Fixes bug [#456525](https://bugs.kde.org/456525)
+ [kcms/landingpage] Strip whitespace at the beginning of a telemetry description. [Commit.](http://commits.kde.org/plasma-desktop/723e15b46ab44526aa77fdfe4e561eb44e27672d) 
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-firewall/02ff184eb6e66ea0e233b96e928c78e280561715) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-mobile/f16842c1480c0e806bad2709e2e8ca1fbe879d8f) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-nm/444974ecfee18a61804ce86531e17aaad22f1c73) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Don't crash when the server doesn't respond. [Commit.](http://commits.kde.org/plasma-pa/b33f3a4c0f4c5b7b7c193370165e799e38c9dddd) Fixes bug [#454647](https://bugs.kde.org/454647). Fixes bug [#437272](https://bugs.kde.org/437272)
+ Install po folder. [Commit.](http://commits.kde.org/plasma-pa/b6f03d5417cf80cc01634e8af17e59aedc722e96) 
+ VolumeMonitor: Don't set stream on source output monitor. [Commit.](http://commits.kde.org/plasma-pa/08309d318539eb90717e44122180f43c43daff36) 
{{< /details >}}

{{< details title="Plasma SDK" href="https://commits.kde.org/plasma-sdk" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-sdk/00675bd23dc97b21f0644f46b2d0ff1e5be4349c) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-systemmonitor/a1eb8855d8ac24831f275a1e49a296105f4009d7) 
{{< /details >}}

{{< details title="plasma-thunderbolt" href="https://commits.kde.org/plasma-thunderbolt" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plasma-thunderbolt/f08f9aaf1c4d1873b300f841b0362a2821eb2be4) 
+ Avoid combining smart pointers and qobject parent ownership. [Commit.](http://commits.kde.org/plasma-thunderbolt/6ea3f5393e84ce9122f081c9b5c5ae47ee81f752) Fixes bug [#439192](https://bugs.kde.org/439192)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Introduce a lock that blocks relayouts and config writes. [Commit.](http://commits.kde.org/plasma-workspace/0a01c8910309fb9f289fe0aa58492e106d154548) Fixes bug [#413645](https://bugs.kde.org/413645)
+ [dataengines/geolocation] Port from KIO::http_post to QNetworkAccessManager. [Commit.](http://commits.kde.org/plasma-workspace/d693026676cc6bf2b7c23e9ff4b620679cf15d10) Fixes bug [#449984](https://bugs.kde.org/449984). Fixes bug [#457341](https://bugs.kde.org/457341)
+ [applets/systemtray] Don't manually destroy innerContainment. [Commit.](http://commits.kde.org/plasma-workspace/432d7c4b51c5a1f17af327d770266b3fe81e5ae5) Fixes bug [#420245](https://bugs.kde.org/420245)
+ Set setInteractiveAuthorizationAllowed on SetPassword call. [Commit.](http://commits.kde.org/plasma-workspace/b66f8019a3db00aa1e4e1c54d19f55c9cbff4d12) Fixes bug [#459309](https://bugs.kde.org/459309)
+ Notifications KCM: X-GNOME-UsesNotifications is a bool. [Commit.](http://commits.kde.org/plasma-workspace/11faf7b371e5b70cbb1789addd0e1c9e4d3ceb56) 
+ Revert "Revert "Prevent panel going out of screen boundaries"". [Commit.](http://commits.kde.org/plasma-workspace/579075f53a49c4e248b5d83a43ee83a903b06272) See bug [#353975](https://bugs.kde.org/353975). See bug [#438114](https://bugs.kde.org/438114)
+ Fonts: honor & present system defaults. [Commit.](http://commits.kde.org/plasma-workspace/dc24b7c1c4e2c9dfb74090f716da029e44e209ff) Fixes bug [#416140](https://bugs.kde.org/416140)
+ Restore previous scope pointer. [Commit.](http://commits.kde.org/plasma-workspace/fbf678488aa6bd3a0802f0b91d656dfe82ab3ffa) 
+ Delay ksplash until after env is set up. [Commit.](http://commits.kde.org/plasma-workspace/8817cce67ed039a538936d0041d33387a40dd1c6) Fixes bug [#458865](https://bugs.kde.org/458865)
+ Applets/kicker: use better API to determine apps' AppStream IDs. [Commit.](http://commits.kde.org/plasma-workspace/07fe092fc008580ae767d046785c9e08b94aa70f) Fixes bug [#458812](https://bugs.kde.org/458812)
+ Kcms/users: don't let "Choose File…" text overflow. [Commit.](http://commits.kde.org/plasma-workspace/aa9d743bfd916c4380fbd26afb067f6dab4cac35) Fixes bug [#458614](https://bugs.kde.org/458614)
+ [kcms/style] Fix setting to default and apply button enablement. [Commit.](http://commits.kde.org/plasma-workspace/0bf8fcafc728bf160edb1bba7818f3fa86c06682) 
+ Kcms/users : Fix focus for new user input. [Commit.](http://commits.kde.org/plasma-workspace/8ebd0f52db9971e262ac2065dcdce1854c0900c4) Fixes bug [#458377](https://bugs.kde.org/458377)
+ [kcms/style] Consider GTK theme setting when computing default state. [Commit.](http://commits.kde.org/plasma-workspace/9e1390fc7564e65f7c79fdc2c34a57c7178a56f5) Fixes bug [#458292](https://bugs.kde.org/458292)
+ Applets/kicker: fix app icon loading logic to better handle relative paths. [Commit.](http://commits.kde.org/plasma-workspace/f0a3a400d63939e70004e6df76536ce538b428c3) Fixes bug [#457965](https://bugs.kde.org/457965)
+ Kcminit: Allow running modules by their name. [Commit.](http://commits.kde.org/plasma-workspace/2092c57179c075c0c66c937ae5e43d9e60528b8a) See bug [#435113](https://bugs.kde.org/435113)
+ Applets/notifications: Fix displaying header progress indicator on different DPI. [Commit.](http://commits.kde.org/plasma-workspace/e462a0e3fcfbbb03dd7ebfc6ff7b29ef9403e20e) Fixes bug [#435004](https://bugs.kde.org/435004)
+ Lookandfeelmanager: Write colors before color scheme. [Commit.](http://commits.kde.org/plasma-workspace/376c74f582a41a8e0eec7c4443d8b7401be97f0d) Fixes bug [#421745](https://bugs.kde.org/421745)
+ Fix appstream runner results appearing before apps/kcms. [Commit.](http://commits.kde.org/plasma-workspace/bfdd84f3cb5f939ceb76dcea679c0a8c7b4d0eca) Fixes bug [#457600](https://bugs.kde.org/457600)
+ [dataengines/weather/dwd] Check if jobs failed. [Commit.](http://commits.kde.org/plasma-workspace/c27b15ed78ad0bce35e3aa7dc82d5f16555bbbc8) 
+ [dataengines/weather/dwd] Properly detect empty reply. [Commit.](http://commits.kde.org/plasma-workspace/7b483096893bc95fb6cf6fd62c3fdece1bbdef9e) Fixes bug [#457606](https://bugs.kde.org/457606)
+ Applets/clipboard: press Ctrl+S to save in the edit page. [Commit.](http://commits.kde.org/plasma-workspace/c39fda6b19a39137b937a8eafaaed363872bf907) 
+ KRunner: Set location before showing. [Commit.](http://commits.kde.org/plasma-workspace/8824f3c5d2f02ccce26d39d3c186d3c202a78933) Fixes bug [#447096](https://bugs.kde.org/447096)
+ [FIX] Unable to remove manually added wallpaper. [Commit.](http://commits.kde.org/plasma-workspace/ac6f1c858e1e6fa1f0a97f52769585d38ea098f3) Fixes bug [#457019](https://bugs.kde.org/457019)
+ [applet/{analog,digital}-clock] Use `onPressed: wasExpanded = ...` idiom. [Commit.](http://commits.kde.org/plasma-workspace/d61a5b6fa536bb08690eafe2b59a83e95941a81c) 
{{< /details >}}

{{< details title="Plymouth KControl Module" href="https://commits.kde.org/plymouth-kcm" >}}
+ Install po folder. [Commit.](http://commits.kde.org/plymouth-kcm/28908968445fe0df5e5f2036f0d6fcd9c15e2a06) 
{{< /details >}}

{{< details title="polkit-kde-agent-1" href="https://commits.kde.org/polkit-kde-agent-1" >}}
+ Install po folder. [Commit.](http://commits.kde.org/polkit-kde-agent-1/ebe5de15b0e21ec2d983a5b5f1728b52d77e19a7) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Install po folder. [Commit.](http://commits.kde.org/powerdevil/c4cfb29a4b3e2a4a553e45ed3e7c08efe0db5bd9) 
+ Fix profile switching in the brightness actions. [Commit.](http://commits.kde.org/powerdevil/35fb7f501e6e0057d6fef8a0f9da425d58dfd27a) Fixes bug [#394945](https://bugs.kde.org/394945)
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Install po folder. [Commit.](http://commits.kde.org/qqc2-breeze-style/c725e03bb875624d0bddc9ffa811fb8d43afbd1d) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Install po folder. [Commit.](http://commits.kde.org/sddm-kcm/90018ac88e37e7a650988cbd5e23381a46626c8a) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Install po folder. [Commit.](http://commits.kde.org/systemsettings/2a28d80107984d871e5cc2ed1ebc0d75e36988a0) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Install po folder. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/980154316c8ccbdc0a1c794dd85749019fc94884) 
+ [screenchooser] Clip listviews. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4430ea5aacb4c76141cce3c561327cdc99275e9a) 
{{< /details >}}

