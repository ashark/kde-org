------------------------------------------------------------------------
r1004326 | mueller | 2009-07-29 21:08:01 +0000 (Wed, 29 Jul 2009) | 2 lines

bump akonadi require

------------------------------------------------------------------------
r1005102 | tmcguire | 2009-07-31 10:41:10 +0000 (Fri, 31 Jul 2009) | 23 lines

Backport r1004645 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1001119 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r1001119 | winterz | 2009-07-22 18:05:16 +0200 (Wed, 22 Jul 2009) | 12 lines
  
  Merged revisions 1001092 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1001092 | winterz | 2009-07-22 11:50:55 -0400 (Wed, 22 Jul 2009) | 5 lines
    
    more crash guards
    kolab/issue3769
    
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1005103 | tmcguire | 2009-07-31 10:42:02 +0000 (Fri, 31 Jul 2009) | 31 lines

Backport r1004647 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1001354 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepimlibs

................
  r1001354 | winterz | 2009-07-23 01:47:36 +0200 (Thu, 23 Jul 2009) | 20 lines
  
  Merged revisions 1001350 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1001350 | winterz | 2009-07-22 19:34:04 -0400 (Wed, 22 Jul 2009) | 14 lines
    
    A better fix for dealing with invalid attendee status strings in Kolab.
    If the status is not one of the 5 valid strings, then make the internal
    setting Attendee::None.
    
    Once the internal setting Attendee::None is written back to the server, it will
    become needs-action.
    
    In no case is an invalid status string written back to the server.
    
    hopefully fixes kolab/issue3766
    MERGE: e4,trunk
    DO_NOT_BACKPORT: 4.3 (new i18n string)
  ........
................


------------------------------------------------------------------------
r1007460 | tmcguire | 2009-08-05 19:31:23 +0000 (Wed, 05 Aug 2009) | 8 lines

Backport r1005872 by mkoller from trunk to the 4.3 branch:

CCBUG: 201900

Allow ( ) inside URLs.
Also add a unit test.


------------------------------------------------------------------------
r1008037 | mkoller | 2009-08-06 18:34:00 +0000 (Thu, 06 Aug 2009) | 6 lines

backport: BUG: 141699, BUG: 40920

Handle the case when the POP server closes the connection after returning the result,
e.g. +OK 0 messages
after the LIST command

------------------------------------------------------------------------
r1008812 | winterz | 2009-08-08 12:53:24 +0000 (Sat, 08 Aug 2009) | 12 lines

Backport r1008762 by cgiboudeaux from trunk to the 4.3 branch:

Add the Chinese holidays.

The file was provided by Patrick Nagel (mail at patrick-nagel dot net). Thank you.

CCBUG: 202911
BACKPORT: 4.3




------------------------------------------------------------------------
r1009437 | djarvie | 2009-08-10 00:53:22 +0000 (Mon, 10 Aug 2009) | 2 lines

Improve apidox

------------------------------------------------------------------------
r1011841 | winterz | 2009-08-16 01:20:50 +0000 (Sun, 16 Aug 2009) | 8 lines

Backport r1011838 by winterz from trunk to the 4.3 branch:

Do not write VALUE=TEXT property for custom properties starting with "X-MOZ", which 
should take care of "X-MOZILLA" too.

CCBUG: 203227


------------------------------------------------------------------------
r1011954 | winterz | 2009-08-16 13:00:49 +0000 (Sun, 16 Aug 2009) | 7 lines

Backport r1011953 by winterz from trunk to the 4.3 branch:

Don't crash when formatting the recurrence string for a yearly recurrence on a day-of-week.
CCBUG: 203999
MERGE: e4,4.3


------------------------------------------------------------------------
r1013328 | tmcguire | 2009-08-19 15:25:59 +0000 (Wed, 19 Aug 2009) | 11 lines

Backport r1012151 by tmcguire from trunk to the 4.3 branch:

Don't remove my whitespace if I set the PreserceSpaces flag.
Strange is that the code seems to do that intentionally, but it is so old that svn annotate doesn't say anything useful.


Add small unit test for that.

BUG :204101


------------------------------------------------------------------------
r1013329 | tmcguire | 2009-08-19 15:26:48 +0000 (Wed, 19 Aug 2009) | 7 lines

Backport r1012907 by tmcguire from trunk to the 4.3 branch:

Now I know why the code didn't simply replace all spaces with nbsps: That would break
word-wrapping.
Therefore, take this into account and adjust the tests for it.


------------------------------------------------------------------------
r1013331 | tmcguire | 2009-08-19 15:27:44 +0000 (Wed, 19 Aug 2009) | 7 lines

Backport r1012912 by tmcguire from trunk to the 4.3 branch:

Fix spaces one and for all, hopefully.
I forgot that in a sequence of spaces, all must be non-breaking, otherwise the breaking one
isn't taken into account.


------------------------------------------------------------------------
r1013754 | winterz | 2009-08-20 17:46:26 +0000 (Thu, 20 Aug 2009) | 10 lines

Backport r1013753 by winterz from trunk to the 4.3 branch:

merge SVN commit 1013751 from e4 kdepimlibs to trunk

make sure to convert Attachment labels from Utf8

Sorry Thomas, you'll need to record this for us.
MERGE: 4.3


------------------------------------------------------------------------
r1013810 | winterz | 2009-08-20 19:43:58 +0000 (Thu, 20 Aug 2009) | 7 lines

Backport r1013809 by winterz from trunk to the 4.3 branch:

don't crash if the calendar passed to the FreeBusy is zero.
CCBUG: 186597
MERGE: 4.3


------------------------------------------------------------------------
r1015198 | mkoller | 2009-08-24 21:22:31 +0000 (Mon, 24 Aug 2009) | 9 lines

Backport r1015195 by mkoller from trunk to the 4.3 branch:

CCBUG: 202445

Try much better to discover a URL, even if it is enclosed with
some sort of brackets/quotes and even interrupted by line breaks or white space.
RFC3986 explains this in appendix C


------------------------------------------------------------------------
r1015975 | tnyblom | 2009-08-26 18:01:11 +0000 (Wed, 26 Aug 2009) | 6 lines

Backport r1015973
Generete error when emailaddress ends with a dot (.)

CCBUG: 139477


------------------------------------------------------------------------
r1016225 | mueller | 2009-08-27 07:55:58 +0000 (Thu, 27 Aug 2009) | 2 lines

version number bump

------------------------------------------------------------------------
r1016229 | tnyblom | 2009-08-27 08:23:00 +0000 (Thu, 27 Aug 2009) | 6 lines

Revert r1015973 pending a better solution.

Keep the unittests but adopted to the old behaviour.

CCBUG: 139477

------------------------------------------------------------------------
