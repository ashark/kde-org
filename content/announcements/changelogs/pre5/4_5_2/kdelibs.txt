------------------------------------------------------------------------
r1168845 | trueg | 2010-08-28 03:20:47 +1200 (Sat, 28 Aug 2010) | 2 lines

Backport: 2 bug fixes in Entiry management: do not crash when comparing empty entities and fix ref counting.

------------------------------------------------------------------------
r1168878 | adawit | 2010-08-28 05:24:05 +1200 (Sat, 28 Aug 2010) | 3 lines

Improve the documentation for the recently added search related API
functions...

------------------------------------------------------------------------
r1168895 | mart | 2010-08-28 07:28:26 +1200 (Sat, 28 Aug 2010) | 2 lines

backport: store the size for formfactor only if not empty

------------------------------------------------------------------------
r1168909 | adawit | 2010-08-28 08:23:46 +1200 (Sat, 28 Aug 2010) | 1 line

Make the function API documentation as clear as possible to avoid ambiguity
------------------------------------------------------------------------
r1169026 | aseigo | 2010-08-28 19:35:38 +1200 (Sat, 28 Aug 2010) | 2 lines

backport: remove any stale config data to avoid accidentally merging old and existing containment configs

------------------------------------------------------------------------
r1169445 | mludwig | 2010-08-29 21:25:27 +1200 (Sun, 29 Aug 2010) | 6 lines

- Add a 'restore' method to 'Sonnet::BackgroundChecker', which allows it to read a given spell check configuration and then to pass it on to the filter object that is used internally. The implementation is similar to what is done inside 'Sonnet::Highlighter'.
- Use the restore method inside KatePart to finally fix bug 244907 for KatePart as well.

Back-port of commit 1169440.


------------------------------------------------------------------------
r1169563 | orlovich | 2010-08-30 05:33:49 +1200 (Mon, 30 Aug 2010) | 8 lines

Create a StatusBarExtension and make sure to propagate the statusbar set on it to the child khtmlpart 
so if a plugin (e.g. adblock helper) tries to do statusbar stuff we use the konqueror statusbar and 
don't end up creating a second/kmainwindow one 

First part of fix for #234624

CCBUG:234624

------------------------------------------------------------------------
r1169902 | mludwig | 2010-08-30 20:57:43 +1200 (Mon, 30 Aug 2010) | 6 lines

Fix detection of nested math modes for LaTeX syntax highlighting.

Patch by Thomas Braun.

Back-port of commit 1169900.

------------------------------------------------------------------------
r1169949 | zepires | 2010-08-30 23:37:36 +1200 (Mon, 30 Aug 2010) | 1 line

UnderLGPL entity added
------------------------------------------------------------------------
r1170033 | orlovich | 2010-08-31 03:43:04 +1200 (Tue, 31 Aug 2010) | 10 lines

At least don't intrepret ? as a wildcard in strings that don't have *. We really 
should escape it even if they do (using WildcardUnix), but QRegExp's wildcard parsing looks so busted 
my brain is rebelling against writing a guaranteed-to-be-wrong escaping function --- probably being 
overpedantic pedantic here, though.

I guess the proper fix would be to do our own parser; and to add some of the URL-specific magic 
adblock+ has while I am at it... 

CCBUG: 249394

------------------------------------------------------------------------
r1170326 | trueg | 2010-09-01 02:20:08 +1200 (Wed, 01 Sep 2010) | 1 line

Pedantic iterators
------------------------------------------------------------------------
r1170418 | dfaure | 2010-09-01 07:48:08 +1200 (Wed, 01 Sep 2010) | 3 lines

Backport r1170378: Skip remote directories, no point in trying to do a file_copy on them, and then failing on that.
(and then kfilepreviewgenerator would keep trying over and over again...)

------------------------------------------------------------------------
r1170431 | mart | 2010-09-01 08:49:42 +1200 (Wed, 01 Sep 2010) | 3 lines

backport the earlier initialization of the size
patch by Mivhael Seiwert

------------------------------------------------------------------------
r1170541 | dfaure | 2010-09-01 23:15:02 +1200 (Wed, 01 Sep 2010) | 2 lines

Backport r1170535: Fixed KSelectAction not setting the tooltip+whatsthis+statustip on the widgets it creates. Bug 205293.

------------------------------------------------------------------------
r1170674 | dfaure | 2010-09-02 08:03:08 +1200 (Thu, 02 Sep 2010) | 2 lines

Backport r1170673: cut down on the number of fstat() calls during mimetype determination.

------------------------------------------------------------------------
r1170675 | dfaure | 2010-09-02 08:21:35 +1200 (Thu, 02 Sep 2010) | 2 lines

Fix compilation after last commit (no worries, this is internal API, it's just used directly by the unittest)

------------------------------------------------------------------------
r1170780 | scripty | 2010-09-02 15:18:47 +1200 (Thu, 02 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1171002 | ahartmetz | 2010-09-03 01:26:04 +1200 (Fri, 03 Sep 2010) | 5 lines

Backport r1170756, fix SSL metadata sometimes not getting to the KIO::Job.
That means that the shield icon will always appear in Konqi's URL bar when
it should.
CCBUG: 220243

------------------------------------------------------------------------
r1171076 | mwolff | 2010-09-03 04:19:20 +1200 (Fri, 03 Sep 2010) | 1 line

backport 1171070: clear marks before clearing the buffer, prevents crash on reload when marks are available
------------------------------------------------------------------------
r1171146 | stikonas | 2010-09-03 09:38:12 +1200 (Fri, 03 Sep 2010) | 1 line

Update user.entities.
------------------------------------------------------------------------
r1171342 | dfaure | 2010-09-04 01:29:40 +1200 (Sat, 04 Sep 2010) | 3 lines

Backport: Fix regression in KArchive::copyTo for ZIP files: use the uncompressed device, not the raw QFile.
 (caught by karchivetest already, but nobody ran it for some time)

------------------------------------------------------------------------
r1171367 | dfaure | 2010-09-04 02:46:09 +1200 (Sat, 04 Sep 2010) | 2 lines

André's wish is my command. Backport r1171326.

------------------------------------------------------------------------
r1171653 | mwolff | 2010-09-05 07:09:43 +1200 (Sun, 05 Sep 2010) | 3 lines

backport r1171638: only ignore next modified on disk signal on window blocked event, when view is visible

CCBUG: 188101
------------------------------------------------------------------------
r1171803 | chani | 2010-09-05 20:07:17 +1200 (Sun, 05 Sep 2010) | 3 lines

backport r1171799:
don't test isEnabled, it can be changed for >1 reason

------------------------------------------------------------------------
r1171993 | pletourn | 2010-09-06 11:51:59 +1200 (Mon, 06 Sep 2010) | 3 lines

Accept the shortcut override event to prevent its conversion to
a shortcut event

------------------------------------------------------------------------
r1171998 | zwabel | 2010-09-06 12:49:36 +1200 (Mon, 06 Sep 2010) | 2 lines

Backport r1149168: Make reverse transformation of ranges/cursors work

------------------------------------------------------------------------
r1172178 | trueg | 2010-09-07 04:44:25 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: Fixed handling of ComparisonTerm::inverted() if the sub term is invalid
------------------------------------------------------------------------
r1172307 | trueg | 2010-09-07 09:22:08 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: Use KFileItem::mostLocalUrl() to get previews for local files returned from search results
------------------------------------------------------------------------
r1172360 | lvsouza | 2010-09-07 14:49:38 +1200 (Tue, 07 Sep 2010) | 6 lines

Backport 1172359 by lvsouza from trunk to the 4.5 branch:

qSort expects a "bool f(a, b)" function not 'int f(a, b)'. The latter function
will always return a positive value for different files, which will be interpreted as true and
consequently nothing will be sorted.

------------------------------------------------------------------------
r1172416 | ehamberg | 2010-09-07 20:31:30 +1200 (Tue, 07 Sep 2010) | 3 lines

backport of r1171636: new, improved syntax files for haskell


------------------------------------------------------------------------
r1172476 | trueg | 2010-09-07 22:34:25 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: apparently KFileItem::isLocalFile() does not take UDS_LOCAL_PATH into account.
------------------------------------------------------------------------
r1172479 | trueg | 2010-09-07 22:41:41 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: workaround for a Virtuoso bug
------------------------------------------------------------------------
r1172522 | lueck | 2010-09-08 00:27:48 +1200 (Wed, 08 Sep 2010) | 1 line

was renamed to kbuildsycoca4
------------------------------------------------------------------------
r1172524 | lueck | 2010-09-08 00:32:23 +1200 (Wed, 08 Sep 2010) | 1 line

revert 1172522, wrong branch
------------------------------------------------------------------------
r1172681 | lunakl | 2010-09-08 08:07:19 +1200 (Wed, 08 Sep 2010) | 7 lines

Backport r1172680:
Use kioexec also for the case of opening a remote URL with an app
that claims remote url support but doesn't support the particular
protocol (e.g. OOo and smb:).
http://svn.reviewboard.kde.org/r/5280/


------------------------------------------------------------------------
r1172685 | ilic | 2010-09-08 08:26:52 +1200 (Wed, 08 Sep 2010) | 1 line

Do not load catalogs for languages below the default (i.e. the language of the messages in the code), since that later confuses fallback sequence.
------------------------------------------------------------------------
r1172732 | pletourn | 2010-09-08 10:57:03 +1200 (Wed, 08 Sep 2010) | 2 lines

Ensure highlighting before calling attribute()

------------------------------------------------------------------------
r1172870 | scripty | 2010-09-08 15:21:30 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1172902 | trueg | 2010-09-08 19:41:59 +1200 (Wed, 08 Sep 2010) | 4 lines

Backport: Never directly access the main model in ResourceData. Instead go through
mainModel(), thus, properly locking the initialization. This fixes crashes
at Nepomuk startup.

------------------------------------------------------------------------
r1172906 | lueck | 2010-09-08 20:02:45 +1200 (Wed, 08 Sep 2010) | 3 lines

backport from trunk r1172905:
More user friendly error handling if a requested help docbook was not found:
try to load a docbook khelpcenter/documentationnotfound with help + infos to solve this issue
------------------------------------------------------------------------
r1173013 | mwolff | 2010-09-09 00:21:03 +1200 (Thu, 09 Sep 2010) | 2 lines

backport r1172670:
    make sure that the returned default style has the correct background
------------------------------------------------------------------------
r1173040 | tmcguire | 2010-09-09 01:23:43 +1200 (Thu, 09 Sep 2010) | 4 lines

Backport r1173038 by pokrzywka:

set the ok variable to true in getWin32RegistryValue() upon success

------------------------------------------------------------------------
r1173045 | mlaurent | 2010-09-09 01:26:55 +1200 (Thu, 09 Sep 2010) | 9 lines

backport:
Revert my change to fix emoticon in kmail2, it works fine in khtml/qwebkit
but breaks in tooltip (it uses a QLabel which doesnt' interpret file:/// ...)
I didn't find a method to fix it directly in kemoticon lib.
For the moment just kmail2 needs file:/// and we didn't release it.
So we have time to find a method to fix it.



------------------------------------------------------------------------
r1173077 | trueg | 2010-09-09 03:15:07 +1200 (Thu, 09 Sep 2010) | 1 line

Backport: Fixed a deadlock when one thread is listing resources while the other one is changing one of them
------------------------------------------------------------------------
r1173285 | adawit | 2010-09-09 19:34:29 +1200 (Thu, 09 Sep 2010) | 1 line

Backported commit r1173284 from trunk
------------------------------------------------------------------------
r1173467 | trueg | 2010-09-10 00:48:57 +1200 (Fri, 10 Sep 2010) | 1 line

Backport: Do not add the sort variable as select var if we are using an aggregate function
------------------------------------------------------------------------
r1173602 | aseigo | 2010-09-10 08:15:36 +1200 (Fri, 10 Sep 2010) | 3 lines

taking an item also requires re-setting the parent
BUG:250420

------------------------------------------------------------------------
r1173880 | rkcosta | 2010-09-11 05:36:42 +1200 (Sat, 11 Sep 2010) | 21 lines

Backport r1173851, r1173866, r1173868 and r1173878.

r1173851 by rich:
- Fix wildcard ssl handling. We now correctly handle wildcards, rather than
  using shell globs. This removes the same issue as QTBUG-4455.  In addition,
I've fixed http://www.westpoint.ltd.uk/advisories/wp-10-0001.txt for konqueror.

r1173866 by whiting:
QString::lower doesnt exist, changed it to QString::toLower to make it build
again

r1173868 by ahartmetz:
This started out as a build fix that was already committed.  While I'm at
it make more things const and do hostname case normalization in the hostname
matching function itself so it's less error-prone (I might reuse that function
elsewhere).  Also make it static; less symbols in the resulting binary and make
it clear that it uses no class members.

r1173878 by ahartmetz:
SVN_SILENT improved comment about hostname matching

------------------------------------------------------------------------
r1173887 | aseigo | 2010-09-11 06:01:44 +1200 (Sat, 11 Sep 2010) | 3 lines

backport of private pointer deletion
BUG:250402

------------------------------------------------------------------------
r1173894 | orlovich | 2010-09-11 06:26:06 +1200 (Sat, 11 Sep 2010) | 3 lines

Fix parsing of cookies marked as cross-domain by kio_http.
(Noticed while debugging disqus stuff)

------------------------------------------------------------------------
r1173903 | mommertz | 2010-09-11 07:33:22 +1200 (Sat, 11 Sep 2010) | 3 lines

check if firstPage is valid
fixes segmentation fault after clearing the cache

------------------------------------------------------------------------
r1173945 | aseigo | 2010-09-11 10:40:19 +1200 (Sat, 11 Sep 2010) | 2 lines

syncToAction declaration

------------------------------------------------------------------------
r1173947 | mommertz | 2010-09-11 11:01:22 +1200 (Sat, 11 Sep 2010) | 5 lines

improved discardCache function
- if the theme changed recreate the cache
- if the theme is the same just clear it
- on colorscheme change keep svgElementsCache as element names and sizes are unchanged

------------------------------------------------------------------------
r1173985 | aseigo | 2010-09-11 13:26:53 +1200 (Sat, 11 Sep 2010) | 2 lines

values less than 0 are completely valid

------------------------------------------------------------------------
r1174160 | mlaurent | 2010-09-12 00:13:22 +1200 (Sun, 12 Sep 2010) | 2 lines

Backport: fix mem leak

------------------------------------------------------------------------
r1174163 | mlaurent | 2010-09-12 00:17:51 +1200 (Sun, 12 Sep 2010) | 2 lines

Backport fix mem leak

------------------------------------------------------------------------
r1174257 | orlovich | 2010-09-12 05:54:26 +1200 (Sun, 12 Sep 2010) | 4 lines

Merged revision:r1174255 | orlovich | 2010-09-11 13:47:32 -0400 (Sat, 11 Sep 2010) | 3 lines

Add the HTML5-referenced exception codes we were missing (I'll only need one of them for now, 
but I see little reason not to have them all).
------------------------------------------------------------------------
r1174273 | orlovich | 2010-09-12 07:09:21 +1200 (Sun, 12 Sep 2010) | 4 lines

Add a way of cloning value-like types, so I can implement HTML5 cloning algorithm
w/o KDE_EXPORTING all the wrapper types.


------------------------------------------------------------------------
r1174275 | orlovich | 2010-09-12 07:12:30 +1200 (Sun, 12 Sep 2010) | 2 lines

Woops, need to commit this too.

------------------------------------------------------------------------
r1174515 | mblumenstingl | 2010-09-13 06:24:17 +1200 (Mon, 13 Sep 2010) | 4 lines

Backport r1174512.

Fixed infinite loops in the config loader if the type of the config element was "point", "rect" or "size".

------------------------------------------------------------------------
r1174598 | alexmerry | 2010-09-13 10:04:55 +1200 (Mon, 13 Sep 2010) | 5 lines

Backport r1174597: Use the incrementing index to emit the signal.

CCBUG: 223487


------------------------------------------------------------------------
r1174739 | dakon | 2010-09-13 20:02:25 +1200 (Mon, 13 Sep 2010) | 1 line

fix KFontChooser() creation with bool passed as flags
------------------------------------------------------------------------
r1175076 | mpyne | 2010-09-14 15:07:34 +1200 (Tue, 14 Sep 2010) | 11 lines

Backport major optimization of KIconLoader to KDE Platform 4.5.2.

This optimization is a backport of r1174319, and causes items that are
retrieved from the process-shared data cache (KSharedDataCache) to also be
stored in KIconLoader's process-local pixmap cache (QCache<>) to save the expense
of reading in the PNG data if the icon is requested in the same process in the
future.

This is backported because of the large speedup, and because this was always the
intended effect, after (silent) approval from kde-core-devel.

------------------------------------------------------------------------
r1175176 | skelly | 2010-09-15 00:13:32 +1200 (Wed, 15 Sep 2010) | 1 line

Backport bug workaround.
------------------------------------------------------------------------
r1175324 | shaforo | 2010-09-15 06:50:51 +1200 (Wed, 15 Sep 2010) | 5 lines

use standard Qt header references to make it compile on Mac (macports-based)

CCMAIL:sebas@kde.org


------------------------------------------------------------------------
r1175340 | shaforo | 2010-09-15 07:09:41 +1200 (Wed, 15 Sep 2010) | 3 lines

more Qt include discipline


------------------------------------------------------------------------
r1175375 | grasch | 2010-09-15 09:15:37 +1200 (Wed, 15 Sep 2010) | 3 lines

Bugfix: Copying installed files to target directory was broken on Windows
backport r1175372

------------------------------------------------------------------------
r1175657 | skelly | 2010-09-16 01:52:53 +1200 (Thu, 16 Sep 2010) | 1 line

Backport r1175655
------------------------------------------------------------------------
r1175719 | skelly | 2010-09-16 05:04:38 +1200 (Thu, 16 Sep 2010) | 7 lines

Don't create a QItemSelection with duplicate ranges.

The 'source' selection model might have a selection which includes
a particular index and its parent. We need to be careful to not
include the overlap twice in the result.

CCBUG: 251274
------------------------------------------------------------------------
r1175930 | tilladam | 2010-09-16 19:33:21 +1200 (Thu, 16 Sep 2010) | 3 lines

Don't crash on Maemo, where QLineEdit doesn't have a context menu, report an
error upwards.

------------------------------------------------------------------------
r1176224 | mpyne | 2010-09-17 15:54:51 +1200 (Fri, 17 Sep 2010) | 2 lines

Backport correct timeout for pthread_mutex_timedlock in KSharedDataCache to KDE Platform 4.5.2.

------------------------------------------------------------------------
r1176329 | ppenz | 2010-09-17 23:30:44 +1200 (Fri, 17 Sep 2010) | 5 lines

Backport of SVN commit 1176328: Applications may not delete the directory model before deleting the KFilePreviewGenerator instance. However it is very easy for applications to violate this, so instead of crashing provide a warning for the application developer. Thanks a lot to Sebastian Sauer for the investigations and the patch!

CCBUG: 196681
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1176342 | ppenz | 2010-09-18 00:08:29 +1200 (Sat, 18 Sep 2010) | 2 lines

Backport of SVN commit 1176334 by dfaure: This is KDE code, use kWarning rather than qWarning.

------------------------------------------------------------------------
r1176381 | skelly | 2010-09-18 01:58:17 +1200 (Sat, 18 Sep 2010) | 1 line

Backport r1176380
------------------------------------------------------------------------
r1176491 | pletourn | 2010-09-18 07:45:23 +1200 (Sat, 18 Sep 2010) | 4 lines

Don't pop context when inside {}

CCBUG:243791

------------------------------------------------------------------------
r1176555 | scripty | 2010-09-18 14:38:27 +1200 (Sat, 18 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176881 | pletourn | 2010-09-19 11:42:49 +1200 (Sun, 19 Sep 2010) | 4 lines

Implicit int to bool conversion strikes again

CCBUG:248733

------------------------------------------------------------------------
r1176892 | pletourn | 2010-09-19 12:36:53 +1200 (Sun, 19 Sep 2010) | 4 lines

Undo damage done by refactoring...

CCBUG:250171

------------------------------------------------------------------------
r1176898 | pletourn | 2010-09-19 13:01:13 +1200 (Sun, 19 Sep 2010) | 4 lines

Revert r1099555

CCBUG:247965

------------------------------------------------------------------------
r1176917 | scripty | 2010-09-19 14:47:25 +1200 (Sun, 19 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177115 | zwabel | 2010-09-20 01:38:19 +1200 (Mon, 20 Sep 2010) | 1 line

Backport R1177114: Always correctly emit the version of the textRemoved(..) signal that includes the old text, never only emit the version without the text.
------------------------------------------------------------------------
r1177230 | mart | 2010-09-20 08:06:57 +1200 (Mon, 20 Sep 2010) | 2 lines

backport check on corona

------------------------------------------------------------------------
r1177303 | scripty | 2010-09-20 14:48:20 +1200 (Mon, 20 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177496 | mwolff | 2010-09-20 23:37:27 +1200 (Mon, 20 Sep 2010) | 3 lines

backport r1177078: make sure we unescape variables also when no un-escaped variable exists

CCBUG: 251547
------------------------------------------------------------------------
r1177583 | mart | 2010-09-21 04:27:45 +1200 (Tue, 21 Sep 2010) | 2 lines

backport:fix taketab behaviour

------------------------------------------------------------------------
r1177648 | lueck | 2010-09-21 07:44:44 +1200 (Tue, 21 Sep 2010) | 1 line

backport: change gui strings from ktts to Jovie
------------------------------------------------------------------------
r1177843 | mart | 2010-09-22 00:43:58 +1200 (Wed, 22 Sep 2010) | 2 lines

backport: items in groups no longer have any border

------------------------------------------------------------------------
r1177900 | dfaure | 2010-09-22 04:11:51 +1200 (Wed, 22 Sep 2010) | 4 lines

Obey the checkbox "Apply inactive window colors effects" -- when unchecked, don't apply the effects.
Together with unchecking "Inactive selection changes color", this gets rid of unnecessary repaints
when switching between gdb and the debugged window :-)

------------------------------------------------------------------------
r1177961 | dfaure | 2010-09-22 07:57:24 +1200 (Wed, 22 Sep 2010) | 2 lines

backport to the intended branch: initialize vars, in case the effects are disabled

------------------------------------------------------------------------
r1178026 | abryant | 2010-09-22 13:39:40 +1200 (Wed, 22 Sep 2010) | 2 lines

Truncate the length of a tooltip's main text and sub text to 5000 characters.

------------------------------------------------------------------------
r1178425 | abryant | 2010-09-23 14:25:55 +1200 (Thu, 23 Sep 2010) | 4 lines

Delete the tooltip window while it's hidden, and recreate it when shown.
This works around an X bug which can cause plasma tooltips to make "holes" in windows under certain conditions.
BUG:249232

------------------------------------------------------------------------
r1178428 | scripty | 2010-09-23 14:50:25 +1200 (Thu, 23 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1178707 | orlovich | 2010-09-24 04:08:40 +1200 (Fri, 24 Sep 2010) | 4 lines

Merged revision:r1178705 | orlovich | 2010-09-23 12:01:48 -0400 (Thu, 23 Sep 2010) | 3 lines

Range check these. 
BUG: 251949
------------------------------------------------------------------------
r1178798 | mart | 2010-09-24 10:03:56 +1200 (Fri, 24 Sep 2010) | 2 lines

backport: signalplotter methods are invokable

------------------------------------------------------------------------
r1179261 | aseigo | 2010-09-25 11:36:07 +1200 (Sat, 25 Sep 2010) | 9 lines

backport SVN commit 1179203 by zack:

Fix transformation artifacts.

The widget does have contents (even if it's inherited) which
was confusing graphicsview and causing wonky artifacts on 
transformations of all widgets that were inside Plasma::ScrollWidget.


------------------------------------------------------------------------
r1179502 | neundorf | 2010-09-26 06:22:37 +1300 (Sun, 26 Sep 2010) | 10 lines

-make us compatible with cmake >= 2.8.3

Otherwise, when using cmake >= 2.8.3, we would get errors because our copy of
FindPackageHandleStandardArgs.cmake doesn't support all arguments of the version
coming with cmake >= 2.8.3, but find_package() would use e.g. FindPNG.cmake from
cmake, which calls FPHSA(), which would get the one from us.

Alex


------------------------------------------------------------------------
r1179696 | scripty | 2010-09-26 16:13:37 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180068 | ruberg | 2010-09-27 11:11:12 +1300 (Mon, 27 Sep 2010) | 3 lines

Added CMakeParseArguments.cmake for being installed. Was forgotten in the backport of CMakeParseArguments.cmake


------------------------------------------------------------------------
r1180081 | aacid | 2010-09-27 11:58:59 +1300 (Mon, 27 Sep 2010) | 2 lines

Make sure applicationLanguageList returns languages this app is actually translated into

------------------------------------------------------------------------
r1180092 | cfeck | 2010-09-27 12:35:41 +1300 (Mon, 27 Sep 2010) | 2 lines

Fix crash when no icon group and size is specified (backport r1180089)

------------------------------------------------------------------------
r1180217 | dfaure | 2010-09-28 00:59:51 +1300 (Tue, 28 Sep 2010) | 4 lines

Backport 1180215:
Restore the default disabled palette (oops) while still not doing an inactive palette by default.
This matches KColorCm::loadOptions() in the kcm.

------------------------------------------------------------------------
r1180254 | tilladam | 2010-09-28 02:31:48 +1300 (Tue, 28 Sep 2010) | 2 lines

Provide an implementation of match() that forwards to the source model properly.

------------------------------------------------------------------------
r1180266 | dfaure | 2010-09-28 02:48:32 +1300 (Tue, 28 Sep 2010) | 2 lines

backport crash fix

------------------------------------------------------------------------
r1180436 | abryant | 2010-09-28 14:19:58 +1300 (Tue, 28 Sep 2010) | 2 lines

Don't crash if hideWidget() is called via the tipWidget it deletes.

------------------------------------------------------------------------
r1180816 | mpyne | 2010-09-29 15:49:40 +1300 (Wed, 29 Sep 2010) | 12 lines

Backport two KSharedDataCache fixes to KDE Platform 4.5.2.

The initial commit was r1180814, and fixes the following:

* Fix an error in defragmentation that could cause corruption of the index table if
  the very last page in the cache was in use during defragmentation. This very
  possibly fixes bug 243573.
* A more reliable (well, in theory) check is performed to tell if the cache version
  has unexpectedly changed for a cache that was actually in use.

CCBUG:243573

------------------------------------------------------------------------
r1181025 | grossard | 2010-09-30 09:52:04 +1300 (Thu, 30 Sep 2010) | 2 lines

added a translator

------------------------------------------------------------------------
r1181093 | ahartmetz | 2010-09-30 13:26:37 +1300 (Thu, 30 Sep 2010) | 5 lines

Backport r1181081 from trunk:
  Fix clearing the cache; processSlice() only returns true when it's done.
CCBUG: 236608
CCBUG: 245808

------------------------------------------------------------------------
r1181329 | jsimon | 2010-10-01 07:22:44 +1300 (Fri, 01 Oct 2010) | 4 lines

Backport 1181321 from trunk:

Fix painting bug on Mac OS X (though it in theory effects all platforms): sizeHint() for file item delegates was too small by one pixel because a qreal value got rounded off instead of up. So instead of "I'm a folder" the displayed file name in e.g. Dolphin was simply "I'm a fo...". Note that this was only the case for the "Short View".

------------------------------------------------------------------------
r1181384 | mueller | 2010-10-01 10:11:44 +1300 (Fri, 01 Oct 2010) | 2 lines

bump version

------------------------------------------------------------------------
