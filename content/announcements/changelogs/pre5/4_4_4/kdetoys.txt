------------------------------------------------------------------------
r1121549 | sboehmann | 2010-05-02 04:48:59 +1200 (Sun, 02 May 2010) | 5 lines

Backport r1121528
Fix: `--time' command line option works properly now

BUG: 230149

------------------------------------------------------------------------
r1122245 | sboehmann | 2010-05-03 22:38:59 +1200 (Mon, 03 May 2010) | 5 lines

Backporting r1122243:
fix possible unwanted double notifications when using the reminder functionality.

CCBUG: 190182

------------------------------------------------------------------------
