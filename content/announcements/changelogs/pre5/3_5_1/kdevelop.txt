2005-11-30 22:49 +0000 [r484476]  mpyne

	* branches/KDE/3.5/kdevelop/lib/cppparser/lexer.h: Backport annma's
	  fix for KDevelop bug 88181 (Inaccurate listing of project TODOs)
	  to KDE 3.5. BUG:88181

2005-12-04 21:03 +0000 [r485544]  neundorf

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.h:
	  -fix parsing of makefiles containing variables -fix add/remove
	  file from project Alex

2005-12-06 13:58 +0000 [r486007]  ivor

	* branches/KDE/3.5/kdevelop/lib/catalog/catalog.cpp: Fix db-open
	  return code equality typo from change [480504]

2005-12-13 08:02 +0000 [r488094-488093]  alanezust

	* branches/KDE/3.5/kdevelop/parts/documentation/plugins/qt/docqtplugin.cpp:
	  Made qt documentation plugins have editable titles (so you can
	  rename them to qt4 and qt3, for example).

	* branches/KDE/3.5/kdevelop/parts/documentation/plugins/doxygen/docdoxygenplugin.cpp:
	  Doxygen doc plugins should have editable titles too.

2005-12-19 21:36 +0000 [r489834]  mueller

	* branches/KDE/3.5/kdevelop/parts/outputviews/directorystatusmessagefilter.cpp:
	  fix strict aliasing warnings

2005-12-22 03:38 +0000 [r490482]  mattr

	* branches/KDE/3.5/kdevelop/doc/kdevelop/project-management.docbook,
	  branches/KDE/3.5/kdevelop/doc/kdevelop/automake-manager.png
	  (added): finish fixing bug 111993. Thanks to annma for fixing.
	  BUG: 111993

2005-12-25 20:28 +0000 [r491362]  cramblitt

	* branches/KDE/3.5/kdevelop/Doxyfile.am: Fix make uninstall

2005-12-27 23:58 +0000 [r491918]  mhunter

	* branches/KDE/3.5/kdevelop/lib/widgets/propeditor/propertymachinefactory.cpp,
	  branches/KDE/3.5/kdevelop/parts/tipofday/tips: What's This?
	  consistency fixes

2006-01-02 14:23 +0000 [r493453]  scripty

	* branches/KDE/3.5/kdevelop/pics/toolbar/hi22-action-dbgjumpto.png,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/win32hello/win32hello.png,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/win32gui/win32gui.png:
	  Remove svn:executable from some typical non-executable files
	  (goutte)

2006-01-10 08:55 +0000 [r496268]  mueller

	* branches/KDE/3.5/kdevelop/languages/python/app_templates/pyqt/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/bash/app_templates/bashhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opieapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/konqnavpanel/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/khello2/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/rubyhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/dcopservice/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/gnomeapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/chello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/perl/app_templates/perlhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cppsdlhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cmakelibc/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opieinput/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/khello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opiemenu/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/gnome2mmapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/qtruby/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/win32hello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/sql/app_templates/sqlsimple/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/java/app_templates/superwaba/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/kapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cppcurseshello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevlang/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopiaapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/pascal/app_templates/fpcgtk/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kpartplugin/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/gtk2mmapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/noatunui/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kcmodule/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/clanlib/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/pascal/app_templates/fpcsharedlib/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kicker/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/haskell/app_templates/haskellhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ada/app_templates/adahello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kioslave/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/chello_gba/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevpart/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cmakesimple/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kscreensaver/Makefile.am,
	  branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kofficepart/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/prc-tool/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opieapplet/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kscons_kmdi/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/python/app_templates/pythonhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/java/app_templates/javahello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kxt/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/win32gui/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakesimple/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kfileplugin/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/pascal/app_templates/fpchello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/wxhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/python/app_templates/pytk/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/fortran/app_templates/fortranhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/java/app_templates/kappjava/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cmakesimplec/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opietoday/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/generichello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevpart2/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kateplugin/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opienet/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kbearplugin/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/kxt/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/dcopservice/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cpphello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/pascal/app_templates/pascalhello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdedcop/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/cmakelibcpp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/php/app_templates/phphello/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kpartapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kbearimportfilter/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kateplugin2/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/qtrubyapp/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kscons_kxt/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kmake/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/noatunvisual/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kmod/Makefile.am:
	  remove the usage of checking for gzip and using $(GZIP) for
	  referring to it. there is a big problem with this attempt: $GZIP
	  is used for default (site wide) flags to gzip, and contains stuff
	  like "-9" (compress best by default). The configure check however
	  does not overwrite environment variables if they're already set -
	  therefore it tried to execute "-9", which fails. Just use gzip
	  directly, we don't have a replacement anyway.

2006-01-13 20:11 +0000 [r497774]  rdale

	* branches/KDE/3.5/kdevelop/parts/grepview/grepviewwidget.cpp: *
	  Don't show any 'find in files' search results that are inside svn
	  directories.

2006-01-16 14:09 +0000 [r498865]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp: *
	  If a ruby class had both an 'attr_reader' and an 'attr_writer'
	  with the same name, it caused the class parser to loop.

2006-01-16 14:47 +0000 [r498881]  rdale

	* branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp: *
	  The ruby class parser wasn't recognising class/module methods of
	  the form 'def self.mymethod()', only 'def MyClass.mymethod()'.

2006-01-17 19:32 +0000 [r499419]  neundorf

	* branches/KDE/3.5/kdevelop/buildtools/custommakefiles/customprojectpart.cpp:
	  improved regexp for parsing makefile targets:
	  http://lists.kde.org/?l=kdevelop-devel&m=113398882810606&w=2 Alex

2006-01-17 19:40 +0000 [r499421]  mattr

	* branches/KDE/3.5/kdevelop/debian (removed): remove the debian
	  directory by request of Jeremy Laine 1 down, 2 to go CCMAIL:
	  Jeremy Laine <jeremy.laine@m4x.org>

2006-01-19 15:59 +0000 [r500184]  coolo

	* branches/KDE/3.5/kdevelop/configure.in.in: updating version

