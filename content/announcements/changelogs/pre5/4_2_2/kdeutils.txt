------------------------------------------------------------------------
r934154 | metellius | 2009-03-02 13:25:04 +0000 (Mon, 02 Mar 2009) | 2 lines

Backporting revision 934144
BUG: 185632 Fix a racing condition that caused extraction to freeze sometimes while showing dialogs.
------------------------------------------------------------------------
r934431 | metellius | 2009-03-03 03:28:52 +0000 (Tue, 03 Mar 2009) | 2 lines

Backporting revision 934429
BUG: 185592 BUG: 185662 BUG: 178347 Replacing randomly crashing recursivelister with QDirIterator. Some small zip-plugin fixes also included.
------------------------------------------------------------------------
r934964 | dakon | 2009-03-04 10:51:30 +0000 (Wed, 04 Mar 2009) | 4 lines

also extract strings in transactions subdirectory

Thanks to Burkhard Lück for finding this.

------------------------------------------------------------------------
r935442 | scripty | 2009-03-05 07:50:16 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r936967 | dakon | 2009-03-08 18:49:16 +0000 (Sun, 08 Mar 2009) | 4 lines

Fix a crash when searching for "casper" on keyservers

The code in trunk will change rapidly soon when I convert the search results to a proper item model to allow filtering and sorting. Once I actually found out what's really going wrong here (i.e. why GPG sends to "pub" lines directly after each other) I'll do a proper fix in 4.2. For now it simply does not crash anymore.

------------------------------------------------------------------------
r937030 | lueck | 2009-03-08 21:47:47 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r938065 | scripty | 2009-03-11 08:38:50 +0000 (Wed, 11 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938838 | dakon | 2009-03-13 10:17:31 +0000 (Fri, 13 Mar 2009) | 2 lines

i18n -> i18np

------------------------------------------------------------------------
r938982 | dakon | 2009-03-13 13:41:39 +0000 (Fri, 13 Mar 2009) | 2 lines

Fix "Open Editor" from systray context menu if leftclick action was set to open editor

------------------------------------------------------------------------
r939263 | metellius | 2009-03-14 14:39:00 +0000 (Sat, 14 Mar 2009) | 2 lines

Backporting revision 939255
Add a destructor to the BatchExtract class
------------------------------------------------------------------------
r939264 | metellius | 2009-03-14 14:39:06 +0000 (Sat, 14 Mar 2009) | 2 lines

Backporting revision 939256
Added moveToThread to threading support to avoid random crashes. Added preprocessor flags for disabling/enabling threading of jobs.
------------------------------------------------------------------------
r939273 | metellius | 2009-03-14 14:51:24 +0000 (Sat, 14 Mar 2009) | 1 line

...and enable job threading (was disabled after the following commit)
------------------------------------------------------------------------
r939757 | toma | 2009-03-15 16:41:50 +0000 (Sun, 15 Mar 2009) | 2 lines

Backport docbook fix

------------------------------------------------------------------------
r941237 | scripty | 2009-03-19 07:48:01 +0000 (Thu, 19 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941616 | metellius | 2009-03-20 02:54:46 +0000 (Fri, 20 Mar 2009) | 2 lines

Backporting revision 941612
Two medium big changes: (1) Archives now have to call finished(bool) when they have finished the work. The upside is that they now have an event loop available, and can be more interactive while working. (2) userQuery is now a protected function to be called, and not a signal to be emitted.
------------------------------------------------------------------------
r941617 | metellius | 2009-03-20 02:55:01 +0000 (Fri, 20 Mar 2009) | 2 lines

Backporting revision 941613
Allow plugins to either use the eventLoop by calling setWaitForFinishedSignal(true), or finished the processing when the main method returns.
------------------------------------------------------------------------
r942593 | scripty | 2009-03-22 07:42:59 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944042 | dakon | 2009-03-24 21:58:54 +0000 (Tue, 24 Mar 2009) | 1 line

remove debugging code, yes this case is hit
------------------------------------------------------------------------
r944777 | metellius | 2009-03-26 03:22:08 +0000 (Thu, 26 Mar 2009) | 2 lines

Backporting revision 944776
Find a new autofilename if the previous one already exists.
------------------------------------------------------------------------
