---
aliases:
- ../changelog4_5_3to4_5_4
hidden: true
title: KDE 4.5.4 Changelog
---

<h2>Changes in KDE 4.5.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_5_4/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kate">kate</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Make commenting a Doxygen comment block work. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=102316">102316</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193692&amp;view=rev">1193692</a>. </li>
        <li class="bugfix normal">Correctly comment empty lines. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=233210">233210</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193979&amp;view=rev">1193979</a>. </li>
        <li class="bugfix crash">Fix crash when pressing Ctrl+R. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=254279">254279</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1198198&amp;view=rev">1198198</a>. </li>
      </ul>
      </div>
      <h4><a name="kdesu">kdesu</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Reset DBUS_SESSION_BUS_ADDRESS to prevent the su'd process from connecting to a session bus it rejects immediately. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=217620">217620</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1196647&amp;view=rev">1196647</a>. </li>
        <li class="bugfix normal">Do not let extra output confuse kdesu and make it think a wrong password has been entered. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=25690">25690</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193637&amp;view=rev">1193637</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not load nspluginviewer unconditionally when in Akregator. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=208780">208780</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193091&amp;view=rev">1193091</a>. </li>
      </ul>
      </div>
      <h4><a name="kio">kio</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Make KIO::AccessManager::createRequest handle all QNetworkAccessManager operations. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250118">250118</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193669&amp;view=rev">1193669</a>. </li>
      </ul>
      </div>
      <h4><a name="kioslave">kioslave</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix digest authentication for calculation of A2 in the HTTP kioslave. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=254441">254441</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193765&amp;view=rev">1193765</a>. </li>
      </ul>
      </div>
      <h4><a name="kdecore">kdecore</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Re-add lock failure detection to KSharedDataCache, which was inadvertently removed prior to the 4.5 release. See SVN commit <a href="http://websvn.kde.org/?rev=1193778&amp;view=rev">1193778</a>. </li>
        <li class="bugfix crash">Don't crash if KSharedDataCache::evictionPolicy() is called on a cache that is somehow unmapped. See SVN commit <a href="http://websvn.kde.org/?rev=1193778&amp;view=rev">1193778</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_5_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="dolphin">Dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix issue that folders in Details View are not automatically opened
when dragging items and "expandable folders" is disabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=237731">237731</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1196239&amp;view=rev">1196239</a>. </li>
        <li class="bugfix normal">Don't reset the preview settings when changing other settings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=252254">252254</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193353&amp;view=rev">1193353</a>. </li>
        <li class="bugfix normal">Fix keyboard navigation problems in Details View if files with very long names are present in the current folder. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=257401">257401</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1199124&amp;view=rev">1199124</a>. </li>
      </ul>
      </div>
      <h4><a name="konqueror">Konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix possible crash when opening a folder in a new Konqueror tab in the Details View mode. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=257035">257035</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1198141&amp;view=rev">1198141</a>. </li>
        <li class="bugfix normal">Fix possible crash when using Konqueror for file management and changing the settings. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=240374">240374</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1199277&amp;view=rev">1199277</a>. </li>
      </ul>
      </div>
      <h4><a name="kxkb">kxkb</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crash in destructor if xkb was not initialized. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=256405">256405</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1194451&amp;view=rev">1194451</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_5_4/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kompare">Kompare</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix some false positives in the check for malformed diffs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=249976">249976</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1193416&amp;view=rev">1193416</a>. </li>
      </ul>
      </div>
    </div>