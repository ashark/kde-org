------------------------------------------------------------------------
r1031525 | mlaurent | 2009-10-05 11:23:00 +0000 (Mon, 05 Oct 2009) | 2 lines

Backport: fix mem leak

------------------------------------------------------------------------
r1031786 | scripty | 2009-10-06 03:01:05 +0000 (Tue, 06 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1033679 | mart | 2009-10-10 18:54:46 +0000 (Sat, 10 Oct 2009) | 3 lines

backport the conversion to qulonglong for tweets ids: twitter is too big
now

------------------------------------------------------------------------
r1034000 | astromme | 2009-10-11 18:09:17 +0000 (Sun, 11 Oct 2009) | 3 lines

Backport 1033988 to 4.3.x
CCBUG: 202826

------------------------------------------------------------------------
r1034968 | scripty | 2009-10-14 03:15:02 +0000 (Wed, 14 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1035265 | astromme | 2009-10-14 16:18:58 +0000 (Wed, 14 Oct 2009) | 8 lines

Fix authentication to Remember The Milk. Applet now complies with RTM API TOS. Contains
string changes in the stable branch, permission was asked on i18n mailing list.

BUG: 210267
CCBUG: 199224
GUI: Applet preferences screen changed to reflect new authentication method


------------------------------------------------------------------------
r1035856 | weilbach | 2009-10-16 02:33:58 +0000 (Fri, 16 Oct 2009) | 2 lines

Only increment pointer when text is not empty and therefore only when message is actually being sent.

------------------------------------------------------------------------
r1035864 | scripty | 2009-10-16 03:18:36 +0000 (Fri, 16 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1036051 | mart | 2009-10-16 12:13:57 +0000 (Fri, 16 Oct 2009) | 3 lines

toUlongLong()
CCBUG:200475

------------------------------------------------------------------------
r1040316 | scripty | 2009-10-26 04:18:29 +0000 (Mon, 26 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1040917 | scripty | 2009-10-27 04:36:55 +0000 (Tue, 27 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
