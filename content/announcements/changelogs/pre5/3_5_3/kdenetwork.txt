2006-03-17 21:39 +0000 [r519808]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h:
	  bump version

2006-03-18 01:11 +0000 [r519854]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.cpp:
	  fix file open check

2006-03-18 16:32 +0000 [r520037]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscaraccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/closeconnectiontask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Backport fix for bug 115772: kopete crash after change my icq
	  status to online. CloseConnectionTask is deleted when its root
	  connection is closed. CCBUG: 115772

2006-03-20 11:04 +0000 [r520558]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp:
	  Backport allowing foo@company.com as accountId

2006-03-20 17:03 +0000 [r520732]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/libkirc/kircengine_commands.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ui/irceditaccountwidget.cpp:
	  Backport 530730 to make use of contact specific encodings and
	  default to utf-8

2006-03-23 18:03 +0000 [r521849]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  detect WPA and WPA2 networks FEATURE: discussed on k-c-d,
	  approved by Dirk Mueller KDE4 commit follows

2006-03-25 21:00 +0000 [r522513]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/configcrypto.ui: should
	  fix a i18n issue ("Open" was taken from kdelibs when it
	  shouldn't):

2006-03-28 18:40 +0000 [r523648]  thiago

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/configcrypto.ui: Fix XML
	  validity

2006-04-07 13:49 +0000 [r527262]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwprotocol.cpp:
	  Go Busy when Busy is selected, not Idle. Whoops!

2006-04-09 00:34 +0000 [r527646]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.cpp:
	  fix automatic spellchecking when turning off rich text

2006-04-17 12:58 +0000 [r530711]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.h:
	  Backport keep-alive fix

2006-04-18 00:26 +0000 [r530904]  rohanpm

	* branches/KDE/3.5/kdenetwork/kget/slaveevent.cpp,
	  branches/KDE/3.5/kdenetwork/kget/dlgIndividual.cpp,
	  branches/KDE/3.5/kdenetwork/kget/slave.h,
	  branches/KDE/3.5/kdenetwork/kget/slaveevent.h,
	  branches/KDE/3.5/kdenetwork/kget/dlgIndividual.h,
	  branches/KDE/3.5/kdenetwork/kget/slave.cpp: Store file sizes in
	  64 bit integers instead of 32 bit. This should fix weird output
	  when downloading files greater than 4GB.

2006-04-19 19:24 +0000 [r531653]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/speed.cpp,
	  branches/KDE/3.5/kdenetwork/wifi/speed.h: followed advice from
	  k-c-d ("suspicious code") the pointer never got exposed to the
	  outside world since it is private, but still the suggestion was
	  good

2006-04-21 08:31 +0000 [r532113]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteonlinestatus.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/knotification.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteviewmanager.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetechatsession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteaccountmanager.cpp:
	  Backport problem found by
	  http://lists.kde.org/?l=kde-core-devel&m=114539794621241&w=2

2006-04-21 08:36 +0000 [r532115]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/addcontactwizard/addcontactwizard.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/outgoingtransfer.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/texteffect/texteffectplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/history/historylogger.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatview.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp:
	  Backport fix for bugs reported on
	  http://lists.kde.org/?l=kde-core-devel&m=114539794621241&w=2

2006-04-21 14:57 +0000 [r532285]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteviewmanager.cpp:
	  Fixed missing parentheses introduced in revision 532113. BUG:
	  126016

2006-04-24 21:50 +0000 [r533477]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimuserinfo.cpp:
	  fix crash reported on
	  http://lists.kde.org/?l=kde-core-devel&m=114539794621241&w=2

2006-04-25 15:13 +0000 [r533785]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.cpp:
	  Backport for the 120117 fix and some other bugs.

2006-04-27 16:03 +0000 [r534713]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/networkscanning.cpp: an empty
	  WEP column means no encryption. stupid bug. thanks to
	  CCMAIL:fverri@primobyte.it

2006-04-29 12:20 +0000 [r535358]  ivor

	* branches/KDE/3.5/kdenetwork/wifi/networkscanning.cpp: Missing
	  brace

2006-04-30 21:25 +0000 [r535976]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/kopete/contactlist/kopetemetacontactlvi.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberbasecontact.cpp:
	  Backport fix: Use the following method to determine the status
	  message to be displayed (the term "contact" should be read as
	  "contact of the metacontact" and "online" as "not offline"): *
	  Display the new status message if - the new status message is not
	  empty and - the contact who set it is online or there are no
	  contacts online at all. * Otherwise display the first non-empty
	  status message among all contacts online---or offline if there
	  are no contacts online at all. * If no status message is
	  displayed yet display no status message at all. CCBUG: 116614

2006-05-01 14:07 +0000 [r536148]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/plugins/connectionstatus/connectionstatusplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/connectionstatus/connectionstatusplugin.h:
	  backport fix for 101669

2006-05-01 14:14 +0000 [r536151]  granberry

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.h:
	  Backport changes to smppdcs too.

2006-05-15 09:59 +0000 [r540997]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/plugins/webpresence/webpresenceplugin.cpp:
	  fix esoteric memory leak (CID 2117)

2006-05-15 12:16 +0000 [r541040]  mueller

	* branches/KDE/3.5/kdenetwork/lanbrowsing/lisa/main.cpp: fix
	  usage() (CID 2066)

2006-05-15 16:40 +0000 [r541142]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/kopete/contactlist/kopetecontactlistview.cpp:
	  fix crash (CID 2159)

2006-05-15 16:44 +0000 [r541145]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/usersearchtask.cpp:
	  fix memory leak

2006-05-17 16:49 +0000 [r541933]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libyahoo2/Makefile.am:
	  fix build. Patch by Jan Ritzerfeld. Thanks

2006-05-18 23:37 +0000 [r542319]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicemodelpool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicemodelpool.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.cpp:
	  Backporting fixes made in 0.12

2006-05-22 02:16 +0000 [r543435]  howells

	* branches/KDE/3.5/kdenetwork/kppp/DB/Provider/Ireland/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kppp/DB/Provider/Ireland/UVT
	  (removed): backport 543434

2006-05-22 02:21 +0000 [r543439]  howells

	* branches/KDE/3.5/kdenetwork/kppp/DB/Provider/United_Kingdom/UTV
	  (added),
	  branches/KDE/3.5/kdenetwork/kppp/DB/Provider/Ireland/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kppp/DB/Provider/United_Kingdom/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kppp/DB/Provider/Ireland/UTV
	  (removed): backport fix for 119751 CCBUG: 119751

2006-05-22 16:22 +0000 [r543706]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp:
	  fix corner case crash

2006-05-22 16:30 +0000 [r543713]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp:
	  Prevent contacts from being added to a server side group called
	  Top Level

2006-05-22 17:18 +0000 [r543740]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwcontact.cpp:
	  Commit in haste, repent at leisure.

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

