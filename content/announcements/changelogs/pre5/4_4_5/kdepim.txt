------------------------------------------------------------------------
r1132307 | djarvie | 2010-05-30 21:18:47 +1200 (Sun, 30 May 2010) | 2 lines

Bug 237288: fixed deferral of non-recurring alarms not working

------------------------------------------------------------------------
r1132315 | djarvie | 2010-05-30 21:27:00 +1200 (Sun, 30 May 2010) | 1 line

Update version number
------------------------------------------------------------------------
r1132349 | tokoe | 2010-05-31 01:20:55 +1200 (Mon, 31 May 2010) | 2 lines

Backport fix that prevents possible crashes

------------------------------------------------------------------------
r1132536 | djarvie | 2010-05-31 11:32:54 +1200 (Mon, 31 May 2010) | 11 lines

Workaround for crash due to presumed Qt bug.
The crash can very occasionally happen when changing from a list of calendars
which requires vertical scroll bars, to a list whose text is very slightly
wider but which doesn't require scroll bars at all. (The suspicion is that the
width is such that it would require horizontal scroll bars if the vertical
scroll bars were still present.) This can result in a recursive call to
ResourceView::viewportEvent() with a Resize event.

The crash only occurs if the ResourceSelector happens to have exactly (within
one pixel) the "right" width to create the crash.

------------------------------------------------------------------------
r1133742 | smartins | 2010-06-02 22:50:17 +1200 (Wed, 02 Jun 2010) | 8 lines

Backport r1132814 by smartins to branch 4.4:

Fix bug where the item selected in EditorDetails didn't match with the one in the AttendeeEditor's mNameEdit.

Caused duplicate attendees to appear.

CCBUG: 230374

------------------------------------------------------------------------
r1133775 | smartins | 2010-06-03 00:36:47 +1200 (Thu, 03 Jun 2010) | 2 lines

Remove some copy/paste mistake i did.

------------------------------------------------------------------------
r1133919 | djarvie | 2010-06-03 08:53:33 +1200 (Thu, 03 Jun 2010) | 3 lines

Fix loss of time zone specification for date only alarms when converting a
pre-2.3.2 calendar, if start-of-day time in calendar is not midnight.

------------------------------------------------------------------------
r1133996 | winterz | 2010-06-03 15:03:26 +1200 (Thu, 03 Jun 2010) | 4 lines

change the Exec command to use --encrypt-sign rather than --sign-encrypt
BUG: 240536
MERGE: trunk

------------------------------------------------------------------------
r1134126 | smartins | 2010-06-04 01:14:06 +1200 (Fri, 04 Jun 2010) | 5 lines

Backport r1134093 by smartins to branch 4.4:

Do not show events before setDates() was called, otherwise we
see day view for some milisecs then weekview.

------------------------------------------------------------------------
r1134216 | djarvie | 2010-06-04 05:08:51 +1200 (Fri, 04 Jun 2010) | 1 line

Allow time zone button to work in read-only mode
------------------------------------------------------------------------
r1134252 | djarvie | 2010-06-04 07:19:33 +1200 (Fri, 04 Jun 2010) | 1 line

Remove warning
------------------------------------------------------------------------
r1134262 | djarvie | 2010-06-04 07:44:10 +1200 (Fri, 04 Jun 2010) | 4 lines

For edit dialog invoked by alarm window Edit button:
- tidy up on dialog deletion;
- ensure dialog is deleted.

------------------------------------------------------------------------
r1134508 | smartins | 2010-06-05 01:19:37 +1200 (Sat, 05 Jun 2010) | 6 lines

Backport r1134499 by smartins to branch 4.4:

s/calendarIncidenceRemoved/calendarIncidenceDeleted/
The calendar observer interface has calendarIncidenceDeleted, not Removed.


------------------------------------------------------------------------
r1134703 | scripty | 2010-06-05 14:02:33 +1200 (Sat, 05 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135733 | scripty | 2010-06-08 14:33:38 +1200 (Tue, 08 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1135906 | smartins | 2010-06-08 21:44:26 +1200 (Tue, 08 Jun 2010) | 8 lines

Backport r1135903 by smartins from trunk to branch 4.4:

Checking mDueCheck/mStartCheck when mTimeButton is already selected changes the selected timezone from Float to the system timezone.

Checking mTimeButton when mDueCheck/mStartCheck are already selected should do the same thing.

CCBUG: 231152

------------------------------------------------------------------------
r1135950 | mlaurent | 2010-06-09 01:28:51 +1200 (Wed, 09 Jun 2010) | 2 lines

Allow to get default charset

------------------------------------------------------------------------
r1135974 | mlaurent | 2010-06-09 02:52:47 +1200 (Wed, 09 Jun 2010) | 3 lines

Backport:
Fix default header values

------------------------------------------------------------------------
r1136014 | mlaurent | 2010-06-09 05:26:03 +1200 (Wed, 09 Jun 2010) | 2 lines

Fix reset to default templates

------------------------------------------------------------------------
r1136246 | mlaurent | 2010-06-09 22:06:36 +1200 (Wed, 09 Jun 2010) | 2 lines

Backport: fix crash

------------------------------------------------------------------------
r1136291 | mlaurent | 2010-06-10 00:28:57 +1200 (Thu, 10 Jun 2010) | 2 lines

Not necessary to duplicate entry

------------------------------------------------------------------------
r1136419 | cfeck | 2010-06-10 06:50:22 +1200 (Thu, 10 Jun 2010) | 4 lines

SVN_SILENT compile

BUG: 241228

------------------------------------------------------------------------
r1136485 | scripty | 2010-06-10 13:57:28 +1200 (Thu, 10 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1136660 | cgiboudeaux | 2010-06-10 23:44:12 +1200 (Thu, 10 Jun 2010) | 1 line

Bump versions.
------------------------------------------------------------------------
r1136675 | cgiboudeaux | 2010-06-11 00:32:08 +1200 (Fri, 11 Jun 2010) | 5 lines

Backport r1136672 from trunk to 4.4:
Fix the icons and shortcuts with RTL.
CCBUG: 205288


------------------------------------------------------------------------
r1136777 | winterz | 2010-06-11 05:27:27 +1200 (Fri, 11 Jun 2010) | 7 lines

backport SVN commit 1136773 by winterz from trunk:

in CalPrintTodos::print(), increase the distance between the "Priority"
and "Summary" header prints so they have less chance of overlapping
in different translations. Also use a smaller font for the header prints.
kolab/issue4414 

------------------------------------------------------------------------
r1137053 | mlaurent | 2010-06-12 02:06:33 +1200 (Sat, 12 Jun 2010) | 3 lines

Backport:
Fallback to home if documentPath doesn't exist on current account

------------------------------------------------------------------------
r1137205 | winterz | 2010-06-12 11:13:44 +1200 (Sat, 12 Jun 2010) | 5 lines

merge forward SVN commit 1137200 by winterz:

write the free-busy-url attribute if the contact has a Free/Busy URL.
kolab/issue4401

------------------------------------------------------------------------
r1137634 | smartins | 2010-06-14 08:06:21 +1200 (Mon, 14 Jun 2010) | 6 lines

Keep the real sub-category in the UserRole so we can use it for filtering.

MERGE: trunk, e35?

BUG: 171484

------------------------------------------------------------------------
r1137807 | smartins | 2010-06-14 23:02:09 +1200 (Mon, 14 Jun 2010) | 4 lines

Backport r1137804 by smartins from trunk to branch 4.4:

Only archive a completed to-do if it's children are also complete, otherwise we get a crash because of orphan to-dos.

------------------------------------------------------------------------
r1137884 | winterz | 2010-06-15 03:10:52 +1200 (Tue, 15 Jun 2010) | 6 lines

merge SVN commit 1137881 by winterz:

include <cstdio> for the FILE definition.
http://reviewboard.kde.org/r/4294
CCMAIL: avg@icyb.net.ua

------------------------------------------------------------------------
r1138034 | scripty | 2010-06-15 14:47:08 +1200 (Tue, 15 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1139101 | mlaurent | 2010-06-17 22:22:46 +1200 (Thu, 17 Jun 2010) | 4 lines

Backport fix from trunk.
Not sure that we can reproduce it in 4.4 but better to test it.


------------------------------------------------------------------------
r1139288 | winterz | 2010-06-18 17:05:39 +1200 (Fri, 18 Jun 2010) | 4 lines

add boost header location to the includes.
BUG: 242035
MERGE: none (already in trunk)

------------------------------------------------------------------------
r1139404 | golnazn | 2010-06-18 19:46:33 +1200 (Fri, 18 Jun 2010) | 1 line

BUG: 226560 Remove trailing slashes from the blog url, before attaching "xmlrpc.php" at the end of it.
------------------------------------------------------------------------
r1139498 | mlaurent | 2010-06-19 00:39:29 +1200 (Sat, 19 Jun 2010) | 3 lines

Backport:
don't show outbox 

------------------------------------------------------------------------
r1139515 | tmcguire | 2010-06-19 02:04:09 +1200 (Sat, 19 Jun 2010) | 2 lines

When changing the identity, change the crypto format as well.

------------------------------------------------------------------------
r1139640 | smartins | 2010-06-19 08:28:22 +1200 (Sat, 19 Jun 2010) | 4 lines

Fix unitialized variable.

MERGE: none (ok everywere else)

------------------------------------------------------------------------
r1139643 | winterz | 2010-06-19 08:41:32 +1200 (Sat, 19 Jun 2010) | 2 lines

remove the wrong comment... planner is here and compiled and it works

------------------------------------------------------------------------
r1139654 | winterz | 2010-06-19 09:20:04 +1200 (Sat, 19 Jun 2010) | 8 lines

in slotAttachFileResult(), when checking the mimetype from the TransferJob,
make sure to also look for mimetype aliases (per dfaure) and then be careful
still about 0 KMimeType::Ptr's.

BUG: 228644
MERGE: trunk


------------------------------------------------------------------------
r1139926 | tokoe | 2010-06-20 01:13:25 +1200 (Sun, 20 Jun 2010) | 2 lines

Backport of bugfix #242063

------------------------------------------------------------------------
r1139927 | tokoe | 2010-06-20 01:14:54 +1200 (Sun, 20 Jun 2010) | 2 lines

Backport of bugfix #240635

------------------------------------------------------------------------
r1140328 | tokoe | 2010-06-20 22:37:22 +1200 (Sun, 20 Jun 2010) | 4 lines

Backport of bugfix #238375

CCMAIL: wstephenson@kde.org

------------------------------------------------------------------------
r1140329 | tokoe | 2010-06-20 22:38:59 +1200 (Sun, 20 Jun 2010) | 2 lines

Backport of fix to disable actions after removing all address books

------------------------------------------------------------------------
r1140720 | mlaurent | 2010-06-21 23:55:53 +1200 (Mon, 21 Jun 2010) | 2 lines

not necessary to show outbox folder when we expire mail

------------------------------------------------------------------------
r1141034 | scripty | 2010-06-22 14:47:10 +1200 (Tue, 22 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141482 | wstephens | 2010-06-23 09:33:18 +1200 (Wed, 23 Jun 2010) | 3 lines

Backport r1141475 by smartins (Editing recurring events removes prior incidences)
BUG: 226394

------------------------------------------------------------------------
r1141604 | scripty | 2010-06-23 14:08:02 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141864 | tokoe | 2010-06-24 05:37:18 +1200 (Thu, 24 Jun 2010) | 2 lines

Backport of fix for enable/disable 'New Contact (Group)' action

------------------------------------------------------------------------
r1141971 | cfeck | 2010-06-24 10:25:03 +1200 (Thu, 24 Jun 2010) | 2 lines

SVN_SILENT Fix build with automoc4 from trunk

------------------------------------------------------------------------
r1141977 | cfeck | 2010-06-24 11:15:30 +1200 (Thu, 24 Jun 2010) | 2 lines

SVN_SILENT oops

------------------------------------------------------------------------
r1142734 | tokoe | 2010-06-26 02:20:08 +1200 (Sat, 26 Jun 2010) | 2 lines

Backport fix for autocompletion of contacts in KMail composer

------------------------------------------------------------------------
