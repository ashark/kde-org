------------------------------------------------------------------------
r1133355 | mfuchs | 2010-06-02 03:17:25 +1200 (Wed, 02 Jun 2010) | 3 lines

Backport r1132290
Groupicons are correctly painted when scrolling the view.
CCBUG:191523
------------------------------------------------------------------------
r1134050 | pali | 2010-06-03 21:37:43 +1200 (Thu, 03 Jun 2010) | 2 lines

Fix configuration for linux skype client - disable call notify event

------------------------------------------------------------------------
r1136238 | pali | 2010-06-09 21:41:31 +1200 (Wed, 09 Jun 2010) | 5 lines

Skype protocol for Kopete
 - update TODO file
 - fix SkypeActionHandler
 - create better config file for skype - add Kopete to authorized apps

------------------------------------------------------------------------
r1136649 | fschaefer | 2010-06-10 23:07:04 +1200 (Thu, 10 Jun 2010) | 6 lines

Kopete: remove paragraph about webcam support from the MSN protocol description.

KDE4-versions of Kopete do not support it anymore.

Backport of r1132437

------------------------------------------------------------------------
r1137864 | mfuchs | 2010-06-15 02:10:33 +1200 (Tue, 15 Jun 2010) | 3 lines

Always set focus to KGet download window when started via Konqueror. Thx llunak for the tip.
Fix is a hack, better (not backportable since there are changes in kdelibs needed) solution in trunk.
CCBUG:228477
------------------------------------------------------------------------
r1139112 | mfuchs | 2010-06-17 23:00:11 +1200 (Thu, 17 Jun 2010) | 4 lines

Do not remove finished files downloaded with metalink if some of the files in the metalink weren't downloaded.
Do not ask the user if he want to overwrite a file selected in metalink if it has been downloaded already.
Change the status to stopped if the seleciton of a metalink changes, thus enabling that transfer to start.
CCBUG:241924
------------------------------------------------------------------------
r1139164 | nlecureuil | 2010-06-18 02:13:03 +1200 (Fri, 18 Jun 2010) | 3 lines

Fix check in testCanDecode()
BUG: 237608

------------------------------------------------------------------------
r1139706 | scripty | 2010-06-19 14:11:57 +1200 (Sat, 19 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141603 | scripty | 2010-06-23 14:07:50 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
