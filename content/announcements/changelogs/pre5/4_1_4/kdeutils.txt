------------------------------------------------------------------------
r879824 | scripty | 2008-11-04 07:34:43 +0000 (Tue, 04 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r883105 | scripty | 2008-11-12 08:14:11 +0000 (Wed, 12 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r884986 | dakon | 2008-11-16 13:28:24 +0000 (Sun, 16 Nov 2008) | 1 line

do not delete input text if user cancels passphrase dialog
------------------------------------------------------------------------
r890769 | scripty | 2008-11-30 07:17:06 +0000 (Sun, 30 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r891216 | kossebau | 2008-12-01 12:46:48 +0000 (Mon, 01 Dec 2008) | 1 line

fixed: data to encode has been taken always starting at the offset 0, not the selection
------------------------------------------------------------------------
r891312 | dakon | 2008-12-01 17:14:52 +0000 (Mon, 01 Dec 2008) | 4 lines

Don't ask to save an empty text

BUG:173405

------------------------------------------------------------------------
r891884 | scripty | 2008-12-03 08:22:10 +0000 (Wed, 03 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r892292 | dakon | 2008-12-03 23:38:12 +0000 (Wed, 03 Dec 2008) | 4 lines

save sort column and order

backport of r891876 by mlaurent

------------------------------------------------------------------------
r893664 | scripty | 2008-12-07 07:51:50 +0000 (Sun, 07 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r906027 | dakon | 2009-01-05 12:57:47 +0000 (Mon, 05 Jan 2009) | 6 lines

Backport r873301 by bbroeksema from trunk

Start search on hitting enter in the line edit of the Keyserver dialog.

BUG:179322

------------------------------------------------------------------------
r906182 | brandybuck | 2009-01-05 18:31:24 +0000 (Mon, 05 Jan 2009) | 3 lines

Backport from trunk. Fixes include 118347, 139602, 140630, 146158,
154688, 164129, 164521, and 173929.

------------------------------------------------------------------------
r906402 | scripty | 2009-01-06 07:22:22 +0000 (Tue, 06 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
