------------------------------------------------------------------------
r1085195 | winterz | 2010-02-04 15:33:15 +0000 (Thu, 04 Feb 2010) | 2 lines

don't exclude _p.h files by default.

------------------------------------------------------------------------
r1087858 | lueck | 2010-02-09 17:24:14 +0000 (Tue, 09 Feb 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1089738 | dhaumann | 2010-02-13 21:26:57 +0000 (Sat, 13 Feb 2010) | 2 lines

backport: set filter correctly when opening the dialog

------------------------------------------------------------------------
r1090483 | dhaumann | 2010-02-15 11:16:14 +0000 (Mon, 15 Feb 2010) | 3 lines

backport fix: Kate file browser plugin: filtering broken
CCBUG: 226529

------------------------------------------------------------------------
r1090941 | dhaumann | 2010-02-16 11:36:42 +0000 (Tue, 16 Feb 2010) | 7 lines

backport SVN commit 1090938 by dhaumann:

fix crash in switching session with multiple mainwindows
(by using a Qt::QueuedConnection for the action invocation)

CCBUG: 227008

------------------------------------------------------------------------
r1091083 | mwolff | 2010-02-16 17:07:45 +0000 (Tue, 16 Feb 2010) | 1 line

backport 1091074: fix ghns install dir of snippets and also put them to ktexteditor_snippets since they are generic and shared between kate and kdevelop
------------------------------------------------------------------------
r1091441 | shaforo | 2010-02-16 21:58:28 +0000 (Tue, 16 Feb 2010) | 2 lines

back-port location generation fix

------------------------------------------------------------------------
r1091733 | dhaumann | 2010-02-17 12:19:11 +0000 (Wed, 17 Feb 2010) | 5 lines

backport SVN commit 1091476 by cullmann:
don't cut caption, kwin is clever enough to shrink it itself

CCBUG: 190853

------------------------------------------------------------------------
r1091906 | lueck | 2010-02-17 21:05:15 +0000 (Wed, 17 Feb 2010) | 1 line

update scereenshots and remove obsoletes also in branch
------------------------------------------------------------------------
r1091955 | dhaumann | 2010-02-17 23:07:40 +0000 (Wed, 17 Feb 2010) | 2 lines

backport: implement MdiContainer to expose the activeView.

------------------------------------------------------------------------
r1091963 | aacid | 2010-02-17 23:22:12 +0000 (Wed, 17 Feb 2010) | 4 lines

backport r1091962 | aacid | 2010-02-17 23:20:55 +0000 (Wed, 17 Feb 2010) | 2 lines

do not extract the column names of a KTimeZoneWidget when embedded in a .ui file

------------------------------------------------------------------------
r1092624 | mwolff | 2010-02-19 10:57:23 +0000 (Fri, 19 Feb 2010) | 3 lines

backport: r1092473 by jowenn
Really delete the files from disc, not only from the view, otherwise they will of course appear at next start again.

------------------------------------------------------------------------
r1093065 | scripty | 2010-02-20 04:31:01 +0000 (Sat, 20 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1093257 | dhaumann | 2010-02-20 12:33:12 +0000 (Sat, 20 Feb 2010) | 24 lines

backport SVN commit 1093240 by dhaumann:

Always use the encoding the user specified when opening files with the KEncodingFileDialog.

Background:
Kwrite does the following steps:
1. creates a new document
2. set the document encoding
3. open the document

Kate did the following:
1. create a new document
2. set the document encoding
3. call SessionConfigInterface::readSessionConfig on the document
   --> this read the cached encoding
4. open the document (with the wrong encoding in 3)

Now step 3 is replaced by the ParameterizedSessionConfigInterface, which reads the sessoin
config with the falg SkipEncoding.

Btw: Encoding Autodetection does nothing in the Open/Save config dialog. Anyone an idea???

CCBUG: 125618

------------------------------------------------------------------------
r1093927 | shaforo | 2010-02-21 18:27:51 +0000 (Sun, 21 Feb 2010) | 2 lines

backport bug fixes

------------------------------------------------------------------------
r1093937 | shaforo | 2010-02-21 18:45:08 +0000 (Sun, 21 Feb 2010) | 3 lines

BUG: 218566
Jump to previous tab when closing one

------------------------------------------------------------------------
r1093942 | shaforo | 2010-02-21 18:50:10 +0000 (Sun, 21 Feb 2010) | 2 lines

setDocumentMode, this is nice especially for Mac OS X

------------------------------------------------------------------------
r1093960 | shaforo | 2010-02-21 19:51:05 +0000 (Sun, 21 Feb 2010) | 4 lines

CCBUG: 227399
bypass PageDown and PageUp to XliffTextEdit


------------------------------------------------------------------------
r1096436 | kkofler | 2010-02-26 17:10:42 +0000 (Fri, 26 Feb 2010) | 4 lines

Fix Cervisia not to unconditionally enable auto spell checking, honor the systemwide setting introduced in kdelibs 4.4 instead (kde#228587).
(Backport revision 1096434 from trunk.)

CCBUG: 228587
------------------------------------------------------------------------
