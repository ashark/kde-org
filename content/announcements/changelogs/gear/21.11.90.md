---
aliases:
- ../../fulllog_releases-21.11.90
title: KDE Gear 21.11.90 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-search" title="akonadi-search" link="https://commits.kde.org/akonadi-search" >}}
+ Convert test file to UTF-8. [Commit.](http://commits.kde.org/akonadi-search/edd6be5624af163058702b83a714ddd891522610) 
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Emit readiness when creating new archive. [Commit.](http://commits.kde.org/ark/1f3a3b554056d4d3cdec651b4add086c0dcbe42b) Fixes bug [#445610](https://bugs.kde.org/445610)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix updating window and tab titles. [Commit.](http://commits.kde.org/dolphin/1657216474020b9b345409f423d75ee34156e1d3) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix i18n arguments for playlist view "N tracks" message. [Commit.](http://commits.kde.org/elisa/63c8c7791aab2ac0ec19c7ab77c26d22d0ce3644) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Color the borders of items in the Agenda view. [Commit.](http://commits.kde.org/eventviews/11f645b55e12f772edcee0624cbabeea127fd17c) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Auto set print orientation. [Commit.](http://commits.kde.org/gwenview/905369978f6e1ceedf3fabcd6e010850074ed7cf) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Also hide the issuer field when not present. [Commit.](http://commits.kde.org/itinerary/5c87108d58df0f9077e213a653dc359b25dee4a6) 
+ Don't color health certificates without a verified signature green. [Commit.](http://commits.kde.org/itinerary/48da86518f8a19ee8e70c9e9225e13ca83bfbaa3) 
+ NL CoronaCheck certificates specify no birth year, show that correctly. [Commit.](http://commits.kde.org/itinerary/084cddf1eb4342ecc37e0b7fd3be9849aa59a325) 
+ Handle fields not provided by the NL CoronaCheck certificates. [Commit.](http://commits.kde.org/itinerary/6a5710cde88f1d0a380a59c0b9362558acc026b7) 
+ Hide the health certificate swipe view when there are no certificates. [Commit.](http://commits.kde.org/itinerary/94702df9d567392632c15a718e1950a1095cdef9) 
+ Fix opening attached documents. [Commit.](http://commits.kde.org/itinerary/f9d332752e3a14593855a15622cbf7fc8f091fcd) 
{{< /details >}}
{{< details id="juk" title="juk" link="https://commits.kde.org/juk" >}}
+ Remove bogus kitemmodels dependency. [Commit.](http://commits.kde.org/juk/008eb122f921d0c8a83d02f9f9b75584942817fb) 
{{< /details >}}
{{< details id="kamoso" title="kamoso" link="https://commits.kde.org/kamoso" >}}
+ Update objectIdFromProperties, fallback to device.path when object.id is empty str. [Commit.](http://commits.kde.org/kamoso/ae0bed1cb1eb805a7b7127a2804bda3dd7ba661a) 
+ Properly use StackView.replace. [Commit.](http://commits.kde.org/kamoso/ab8285e2a245f985bbcb7dad8d104da672d31c2d) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Add more options for clangd. [Commit.](http://commits.kde.org/kate/12951bb607f7d590dc52fe404f7d7515ad6dddf3) 
{{< /details >}}
{{< details id="kbreakout" title="kbreakout" link="https://commits.kde.org/kbreakout" >}}
+ Level 16: Convert one of the two GiftIncreaseSpeed to GiftDecreaseSpeed. [Commit.](http://commits.kde.org/kbreakout/70abb4071631df07b41094dd02b7f1ce83697014) 
{{< /details >}}
{{< details id="kcachegrind" title="kcachegrind" link="https://commits.kde.org/kcachegrind" >}}
+ Fix hang in line-break algorithm. [Commit.](http://commits.kde.org/kcachegrind/5e73b3e6c816b3330f02d2628112da6245a7cf3d) Fixes bug [#428917](https://bugs.kde.org/428917)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Fix unicode-decoding mistake in isInGsmAlphabet method. [Commit.](http://commits.kde.org/kdeconnect-kde/2da77bdc50e0359f3cfa3428ab8450e97b47a9b6) 
+ Always show notification when receiving files. [Commit.](http://commits.kde.org/kdeconnect-kde/1f3c9399c3b1e80c0c3a4bfa55870797bfdf8980) Fixes bug [#417823](https://bugs.kde.org/417823)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Spacer tool: Don't allow independant move of grouped items. [Commit.](http://commits.kde.org/kdenlive/c1b0f27582bbb97932d5a400aa189042739582db) See bug [#443324](https://bugs.kde.org/443324)
+ Fix tool label width in statusbar. [Commit.](http://commits.kde.org/kdenlive/369f7baef306b8bbfd7dc0a0e50565487e1776d8) 
+ Fix crash moving clip with mixes in insert/overwrite mode. [Commit.](http://commits.kde.org/kdenlive/5a2c8849a809bf4c91e7a864b21f20e38438f85a) 
+ Fix group move with mix sometimes broken. [Commit.](http://commits.kde.org/kdenlive/0ed64d951e46fcdca7f18fdf3fde78bd9e72a69b) 
+ Fix errors/crash in insert mode (lift/extract) with mixes. [Commit.](http://commits.kde.org/kdenlive/676b79c16f99bd623951728aee1b67c1ae7e1edf) 
+ Fix crash using spacer tool on grouped clips with a clip in the group positioned before spacer start operation. [Commit.](http://commits.kde.org/kdenlive/504154d1ecd773b3f4854436138cc21082265d0c) Fixes bug [#443324](https://bugs.kde.org/443324)
+ Fix mix cut pos lost when switching mix composition. [Commit.](http://commits.kde.org/kdenlive/e0899fa5c5f6f51a24063395e9d9fb75b1a66bf9) 
+ Luma transition: add option to control alpha channel (fixes transition on clips with different aspect ratios). [Commit.](http://commits.kde.org/kdenlive/ae24f6d6ae66f608f8a1eb9862bd062e11fa66e5) 
+ Insert/overwrite mode: delete mixes on clip move. [Commit.](http://commits.kde.org/kdenlive/72f0ce9980b0f4dff89c65b5709a82fc3e7fa96e) 
+ Fix end resize bug. [Commit.](http://commits.kde.org/kdenlive/4c470753c85deefd62f3bdaf5a35f32865500296) 
+ Fix cannot move clip left when only 1 frame space. [Commit.](http://commits.kde.org/kdenlive/149cdf5e1da8effa4db9709e264d6f1804de138a) 
+ Remove useless string duplication. [Commit.](http://commits.kde.org/kdenlive/3e6d06eac3069491dc21a676115c76df94973396) 
+ Fix blank length calculation allowing incorrect 1 frame overlap in some cases. [Commit.](http://commits.kde.org/kdenlive/6c2a5c15dbaeb18b80082d2a5f8a46ac4f606d9f) 
+ Fix crash on undo mix cut. [Commit.](http://commits.kde.org/kdenlive/59cc1202d21c75ec5033bd677157c7dd5a071203) 
+ Fix left resize regression. [Commit.](http://commits.kde.org/kdenlive/1374bd44753def20dc8c3dc437fc731c5125b412) 
+ Fix right mouse click behavior on monitor when effect scene is displayed. [Commit.](http://commits.kde.org/kdenlive/462b430d72b47ec8e30cc0b5c81ebc3dde6d6098) 
+ Another round of mix resize issues, with added tests. [Commit.](http://commits.kde.org/kdenlive/026f210934395db000194df9b4a841bcc9197d36) 
+ Another fix for mix resize corruption. [Commit.](http://commits.kde.org/kdenlive/cbf2af356c42b5337938e9a99e649f12f0057495) 
+ Fix another clip marker issue introduced with old code for Ripple. [Commit.](http://commits.kde.org/kdenlive/48c605b64a07a33675dd2d389ecebe7cce716e68) 
+ Fix some mix resize issues allowing to create invalid mixes. [Commit.](http://commits.kde.org/kdenlive/46440d9eabf420dab5689561b46eeb6dd7231c83) 
+ Fix broken find/replace breaking timeline clip markers display. [Commit.](http://commits.kde.org/kdenlive/956009fc69c3225053b496c024415274e036f5eb) 
+ Fix some 1 frame clip mix incorrectly detected as invalid. [Commit.](http://commits.kde.org/kdenlive/86b662f7eab999efdf077f3fc8dc3573d4a8b7f1) 
+ Appstream Data: the manual is now at https://docs.kdenlive.org. [Commit.](http://commits.kde.org/kdenlive/045c214f5a9e36eeb19578dcb460e71a2cf29315) 
+ Extract frame from timeline monitor now correctly disables proxy to  create a full res image. [Commit.](http://commits.kde.org/kdenlive/2cf317dbed8308f46857f749c96fc8e1ba53657a) 
+ Fix MLT api change causing startup crash on movit (Movit still not usable). [Commit.](http://commits.kde.org/kdenlive/b24148d53ec26651f48f4bf6fe368d9861eea155) See bug [##442880](https://bugs.kde.org/#442880)
+ Track name edit: fix focus issue, enable F2 shortcut. [Commit.](http://commits.kde.org/kdenlive/816c93790d434909f31f615937f9ffaa0b7278e6) Fixes bug [#440185](https://bugs.kde.org/440185)
+ "Go to clip start/end" should use clip under cursor if none is selected. [Commit.](http://commits.kde.org/kdenlive/21cd94995de06a05b223909688aaf623107ab42f) Fixes bug [#440024](https://bugs.kde.org/440024)
+ Fix transcoding of title clips. [Commit.](http://commits.kde.org/kdenlive/7b7d27196313609ea530bb53d98c2b22bb313832) 
+ Typewriter effect should not be blacklisted!. [Commit.](http://commits.kde.org/kdenlive/7e1a2251e42f59ccdd29f45a3b28a96e81360697) Fixes bug [#445232](https://bugs.kde.org/445232). See bug [#436113](https://bugs.kde.org/436113)
+ Fix "Select Transition" should select mixes too. [Commit.](http://commits.kde.org/kdenlive/e1cd395adb4741a8b9610e6bf91bf523bb418325) Fixes bug [#440023](https://bugs.kde.org/440023)
+ Fix display of timeline usage in clip monitor. [Commit.](http://commits.kde.org/kdenlive/a7c06a570bf0d5bd60fc0620efccec34174ee8b2) 
+ Show timeline usage in clip monitor. [Commit.](http://commits.kde.org/kdenlive/d4575061d29c9161f6de511fdbe18bb4d17aa675) 
+ Fix default project path ignored on creating new project. [Commit.](http://commits.kde.org/kdenlive/49699b4bb9f888d879d3d2263c3ba4110195db3f) Fixes bug [#444595](https://bugs.kde.org/444595)
+ Fix warning. [Commit.](http://commits.kde.org/kdenlive/f70f952e50fee3907d5230ac2d1b9a704fa0568a) 
+ Fix audio/video only drag from bin. [Commit.](http://commits.kde.org/kdenlive/10ea22996eeededa87fa551231c059cbd2b33764) 
+ Hide audio mix from transitions list in same track and composition stack. [Commit.](http://commits.kde.org/kdenlive/1790aaf3a1e8e220f84f36a0d96a180a7eba3d58) 
+ Fix possible crash in url effect parameter. [Commit.](http://commits.kde.org/kdenlive/a3a5582bf255d3b9969b61443c295e48bb9cf9b4) 
+ Fix crash on close. [Commit.](http://commits.kde.org/kdenlive/5a09ccff643acb0775434f2d8bd9c622999c28a1) 
+ Fix video only clips displaying audio icon. [Commit.](http://commits.kde.org/kdenlive/9ffe861498afe5b7e8219cdfb18db3834dd72485) 
+ Allow closing secondary bin. [Commit.](http://commits.kde.org/kdenlive/0ab118277f486f5f18a3b0d7cd70867c8afb7488) 
+ Also display usage icon on non AV clips. [Commit.](http://commits.kde.org/kdenlive/1e2d827bc33515187aaaed8cf30ab677bb969f4a) 
+ Improve visibilty of bin clip  usage (colored icons). [Commit.](http://commits.kde.org/kdenlive/bcf6bdca4281d0a8d5c6dd5027ab7cbfd7a29683) 
+ Bin icon view: clips used in timeline use bold font for name. [Commit.](http://commits.kde.org/kdenlive/5563df92c94b66de61bb83050336ea8cd21c7b20) 
+ Bin icon view: make audio /video drag icons more visible on hover. [Commit.](http://commits.kde.org/kdenlive/6741f01b76741acd02ccfd6d0773e6a6f0fb0fbf) 
+ Switch multicam view to qtblend composition to avoid monitor preview scaling issues. [Commit.](http://commits.kde.org/kdenlive/e7915cb35723db2cd18175051b09656c8f7a8e03) 
+ Multiple bins: put folder name as widget title, enable up button and double click to enter folder in secondary bins. [Commit.](http://commits.kde.org/kdenlive/b04dedff04424b1075bff2f80c959d90a6e67f5a) 
+ Added UI for the frei0r_transparency effect. [Commit.](http://commits.kde.org/kdenlive/d311eda6ec5a3d96d1e277f6b25ae6d1f532db16) 
+ Hide secondary bin title bar. [Commit.](http://commits.kde.org/kdenlive/0c3df81fcf9ace3e3cf70464eafdb602dec4c9e5) 
+ Fix incorrect QList insert. [Commit.](http://commits.kde.org/kdenlive/c89ff3ef824c4bd0afa8e287509e432bc4b8e5e2) 
+ Fix render time overlapping text button. [Commit.](http://commits.kde.org/kdenlive/ae64fa29cbe0c313bad3d0fe8647fad6e4557cf7) 
+ Attempt to fix slideshow detection on Windows. [Commit.](http://commits.kde.org/kdenlive/32cc9bace95b7962f95b830339488ae8fe19966a) 
+ Remove old custom function to append shortcut to action tooltip, causing shortcuts to appear twice. [Commit.](http://commits.kde.org/kdenlive/7962e1670fe7e1868470456c6b1d6ae998a5fce5) 
+ Keyframe import: display user friendly param name in combobox, fix import of opacity. [Commit.](http://commits.kde.org/kdenlive/bbac0b2203ee5bb64beb3ce040abcdbfb084cfe1) 
+ Fix affine (Transform) opacity is now in the 0-1 range with MLT7, fix cairo affine blend composition default params. [Commit.](http://commits.kde.org/kdenlive/957e7597907e83d2884c350ee989ee073827c3da) 
+ Disable duplicate keyframe when cursor is over a keyframe. [Commit.](http://commits.kde.org/kdenlive/03b4df1f7b614d99c47c02edb0a8dbda8b41f7e2) 
+ Ctrl+A in bin will select all items in current folder. Switching from tree view to icon view keeps selection. [Commit.](http://commits.kde.org/kdenlive/de9bf0f014833266c4ed52bd044d055a70fe1ab8) 
+ Allow seeking by clicking on zoombar when not zoomed. [Commit.](http://commits.kde.org/kdenlive/4f9b6c6bf05d1be1e2e06a9e7d7c9bd415178959) 
+ Fix keyframe incorrectly moved when attempting to seek in keyframe view. [Commit.](http://commits.kde.org/kdenlive/deb51c9fa5a02d4b5d8bd957dba58e4613decd15) 
+ Titler: prevent selecting inexisting font. [Commit.](http://commits.kde.org/kdenlive/d7c528b26f13df73378761323291fb6abdba0fd8) 
+ Fix some issues in multiple bin. [Commit.](http://commits.kde.org/kdenlive/dafd50fd412e46128e3a669e39d474ffb6b4147f) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Revert changes to the test reference data by code adjustment scripts. [Commit.](http://commits.kde.org/kdepim-addons/52a98c5c18a7b38c4530731a1073e3b568f67ae6) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Fix POP3 setup wizard defaults to unencrypted connections. [Commit.](http://commits.kde.org/kdepim-runtime/35447bd04e8c12afac524e1c4556ef3db088e014) Fixes bug [#423426](https://bugs.kde.org/423426)
+ Const'ify pointer. [Commit.](http://commits.kde.org/kdepim-runtime/584644ba1e9734cfcf5c5ce2e65e727a12cadee8) 
{{< /details >}}
{{< details id="kdev-php" title="kdev-php" link="https://commits.kde.org/kdev-php" >}}
+ New in this release
{{< /details >}}
{{< details id="kdev-python" title="kdev-python" link="https://commits.kde.org/kdev-python" >}}
+ New in this release
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ Fix the app shell script to run with zsh. [Commit.](http://commits.kde.org/kdevelop/f3bde6dcd4767396b5fce7572be565f345adf271) Fixes bug [#442481](https://bugs.kde.org/442481)
{{< /details >}}
{{< details id="kgeography" title="kgeography" link="https://commits.kde.org/kgeography" >}}
+ New usa_mississippi flag. [Commit.](http://commits.kde.org/kgeography/fb462e6e0318fb655ba591c2e26f4732afae408c) Fixes bug [#445670](https://bugs.kde.org/445670)
{{< /details >}}
{{< details id="kimap" title="kimap" link="https://commits.kde.org/kimap" >}}
+ Treat SSL handshake errors as fatal also when using STARTTLS. [Commit.](http://commits.kde.org/kimap/cbd3a03bc1d2cec48bb97570633940bbf94c34fa) See bug [#423424](https://bugs.kde.org/423424)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add a new set of VDV certificates. [Commit.](http://commits.kde.org/kitinerary/ebfa6d433a361d319db36ebb4719e85c41fa7f6d) 
+ Skip invalid VDV certificates rather than terminating the download. [Commit.](http://commits.kde.org/kitinerary/1eacbb2d514939c51113438271f897b2fbd010a7) 
+ Add SPDX markers for the generated qrc file. [Commit.](http://commits.kde.org/kitinerary/384e611c13e99914eefbd6c6965356099c3434f0) 
+ Fix the Windows build. [Commit.](http://commits.kde.org/kitinerary/e99e7421b4ee5fd5eee45302038c48e7ff39d02e) 
+ Recursively search for a context date from a context MIME node. [Commit.](http://commits.kde.org/kitinerary/a106a81843d0ae2f4f1d59f5ada8423203a917a7) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Make it const + remove mem leak. [Commit.](http://commits.kde.org/kmail/28ec99393523da71dc8e89acf0f2694394fc2c3b) 
+ Add modify menu + add tooltip. [Commit.](http://commits.kde.org/kmail/a48d12942c64d64b02dd450ec13ef2f1a6acc8ca) 
+ Add "modify" item. [Commit.](http://commits.kde.org/kmail/ab88b08da8048a7b29b42b68cb2f1ea8b4db752c) 
+ Add separator. [Commit.](http://commits.kde.org/kmail/1c4646bfb691bcc7dfc7484365c89a7617ef0891) 
+ Add some tooltips. [Commit.](http://commits.kde.org/kmail/aefe54c1388f9d60e9291381ab551e53feb478a5) 
+ Fix white-on-yellow text on ServerLabel tooltip. [Commit.](http://commits.kde.org/kmail/3d927d8be8385a5cc500372ee2a76455d2c8376f) 
+ Add missing icon. [Commit.](http://commits.kde.org/kmail/7249f37108341317a5f24203cc04e43dac1b2eee) 
+ Improve autotest. [Commit.](http://commits.kde.org/kmail/6b9364bf0f7f14771ff63c133f63f0ac7d92bc4f) 
+ Allow to show original message. [Commit.](http://commits.kde.org/kmail/4426d4a2453d13eb6bdbfa98fbaf67f468c15c2b) 
+ Const'ify variable. [Commit.](http://commits.kde.org/kmail/395fa922c228eae13220abb1eb7edeef7115b230) 
+ Add new autotests. [Commit.](http://commits.kde.org/kmail/9d3046bd86d1e86a76e2107d34aa02717d1e4783) 
+ Add tooltips here. [Commit.](http://commits.kde.org/kmail/503ff5e6472f938ef0730fe6ac6ef2814e16c8ad) 
+ Make sure that we don't switch to same folder. [Commit.](http://commits.kde.org/kmail/18fc92135240915f44456de37aaaacf3d09cec19) 
+ Create dialog on stack + const'ify pointer. [Commit.](http://commits.kde.org/kmail/97e8b513ca5d95aa746c058bc9d665414d2bd826) 
+ Limit the number of history. [Commit.](http://commits.kde.org/kmail/93d32af35c37f90f39bbae09477e9916a58d93ce) 
+ Reset treeview otherwise we can't see all items. [Commit.](http://commits.kde.org/kmail/3980b4884f2aa04c5e9e5e74f1150cff085777ed) 
+ Debug-- + fix resize column. [Commit.](http://commits.kde.org/kmail/526ccd6eadb5d9374574e4c1647487039d473a2e) 
+ Remove historyswitchfoldermanager. [Commit.](http://commits.kde.org/kmail/d05787b437444f3f032a66a163941d41b7044979) 
+ Use collectionswitchertreeviewmanager. [Commit.](http://commits.kde.org/kmail/399ee267cc6cb96f50bf439424d3ae96deeb6e36) 
+ Continue to implement model. [Commit.](http://commits.kde.org/kmail/8f2b59f5527c94fd1270418b1f015f903a718f6a) 
+ Add model. [Commit.](http://commits.kde.org/kmail/d17edf9b48c0c9d81f313adec0826a385364679c) 
+ Assign parentWidget. [Commit.](http://commits.kde.org/kmail/f70e72aa532fb98b82b230553de40b4b5a5bf121) 
+ Add  manager. [Commit.](http://commits.kde.org/kmail/96e28cd6cd12f69969e21249d4901501408c393a) 
+ Add collectionswitchertreeview widget. [Commit.](http://commits.kde.org/kmail/e3d0f6ebfb090b37b587e0c8b3ec08cbfbc8a56b) 
+ Move in own repository. [Commit.](http://commits.kde.org/kmail/d054f8578cde79069607e409cda1c11288576744) 
{{< /details >}}
{{< details id="kolourpaint" title="kolourpaint" link="https://commits.kde.org/kolourpaint" >}}
+ Auto set print orientation. [Commit.](http://commits.kde.org/kolourpaint/1e1ed4a39f1c8af20b9e2c3a296e786be77294bd) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix changing non-Breeze scrollbar colors at runtime. [Commit.](http://commits.kde.org/konsole/564ae1fa96012c0ac81c7ca1ed29c99afc511e50) 
+ Adapt scrollbar to terminal color scheme for Breeze widget style again. [Commit.](http://commits.kde.org/konsole/8d2582b000f2a3dcf6ec946f70af8f9aa753fa02) 
+ Revert "Let the scrollbar follow the app color scheme". [Commit.](http://commits.kde.org/konsole/3e6520027431a70fe173dc4c55a91b194b615a7b) 
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ GIT SILENT Change BRANCH_GROUP to stable-kf5-qt5. [Commit.](http://commits.kde.org/korganizer/6e8245459ee20079a437f75fe2206c3ab51cec51) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Fiox recent emoji order. [Commit.](http://commits.kde.org/kpimtextedit/45dc025ddb4f903e8b90baa07125a610838905d6) 
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Fix seek error when filling device with random data or zeroes. [Commit.](http://commits.kde.org/kpmcore/29109ffc760e9d772b06b2a4ff76df45498596bc) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Don't truncate path line segment lengths to integers. [Commit.](http://commits.kde.org/kpublictransport/c03b683e718b6ad01131761698b829871bedab25) 
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Fix clean parallel build. [Commit.](http://commits.kde.org/krfb/6794b9d9fb3e382ac8c693e39e0d2901242a7452) 
{{< /details >}}
{{< details id="ktuberling" title="ktuberling" link="https://commits.kde.org/ktuberling" >}}
+ Make sure the saved files have the extension they should have. [Commit.](http://commits.kde.org/ktuberling/6ba5b71bfdaeaa396a631fa3d1a57499b01469c8) Fixes bug [#414040](https://bugs.kde.org/414040)
{{< /details >}}
{{< details id="kwordquiz" title="kwordquiz" link="https://commits.kde.org/kwordquiz" >}}
+ Fix code that tries to add extension if it's missing. [Commit.](http://commits.kde.org/kwordquiz/e5814f40103b13b4acb496a605fbb5cc6954d18e) Fixes bug [#444998](https://bugs.kde.org/444998)
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Add icon on button. [Commit.](http://commits.kde.org/libksieve/75b55c032f55732fc767263d99b2bced532ccae9) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Presentation: Don't hide toolbar if we're not really leaving the window. [Commit.](http://commits.kde.org/okular/1f00898af00fc414fe7ad486e521fc56159dcb5f) Fixes bug [#444427](https://bugs.kde.org/444427)
+ Fix zoom actions not being updated correctly. [Commit.](http://commits.kde.org/okular/975bf990edbd920bbbe326b1519945f7e859c4f6) Fixes bug [#440173](https://bugs.kde.org/440173)
+ Okularcore doesn't need dbus. [Commit.](http://commits.kde.org/okular/2de5c96e528247243025b869022628d64f28fba8) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Better to use save here. [Commit.](http://commits.kde.org/pim-sieve-editor/8971520542b3106e9da19da675606eb75bb5c10d) 
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Use KIO::JobUiDelegate for opening other application. [Commit.](http://commits.kde.org/spectacle/410fcc3b6fae86490ebcdd2fce040b9ff046a090) Fixes bug [#445530](https://bugs.kde.org/445530)
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Merge origin/master @ 5821f74 to release/21.12. [Commit.](http://commits.kde.org/umbrello/eeeaceeb84bbd8ec9530d040f711e98cf10d704c) 
+ Merge origin/master @ 788fcb8 to release/21.12. [Commit.](http://commits.kde.org/umbrello/3ce01b6a6131651f1d2a2ac1e0ff426caad1d784) 
+ Umbrello/dialogs/dialog_utils.cpp function remakeTagEditFields : Address compiler warning about unused parameter `o`. [Commit.](http://commits.kde.org/umbrello/5821f7482da8c8622b12007da9d8d71a5a92defb) 
+ Https://scan.coverity.com/projects/3327 Coverity fixes listed by CID :. [Commit.](http://commits.kde.org/umbrello/788fcb8b4f830ca617a4296442ac22c8c057f737) See bug [#340646](https://bugs.kde.org/340646)
{{< /details >}}
{{< details id="zanshin" title="zanshin" link="https://commits.kde.org/zanshin" >}}
+ Install translations. [Commit.](http://commits.kde.org/zanshin/ed122f089742b698c76e48abc23dd61e45e3d1e6) 
{{< /details >}}
