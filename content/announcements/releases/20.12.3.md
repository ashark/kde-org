---
publishDate: 2021-03-04 10:00:00
title: 20.12.3 Releases
---

Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service.

Today they all get new bugfix source releases.

Distro and app store packagers should update their application packages.

+ [20.12 release notes](https://community.kde.org/Releases/20.12_Release_Notes) for information on tarballs and known issues.
+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [20.12.3 source info page](https://kde.org/info/releases-20.12.3/)
+ [20.12.3 full changelog](https://kde.org/announcements/changelogs/releases/20.12.3/)
