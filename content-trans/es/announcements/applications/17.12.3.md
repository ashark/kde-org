---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE lanza las Aplicaciones de KDE 17.12.3
layout: application
title: KDE lanza las Aplicaciones de KDE 17.12.3
version: 17.12.3
---
Hoy, 8 de marzo de 2018, KDE ha lanzado la tercera actualización de estabilización para las <a href='../17.12.0'>Aplicaciones KDE 17.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 25 correcciones de errores registradas, se incluyen mejoras en Kontact, Dolphin, Gwenview, JuK, KGet, Okular y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Akregator ya no borra la lista de fuentes «feeds.opml» tras ocurrir un error.
- El modo a pantalla completa de Gwenview opera ahora sobre el nombre de archivo correcto tras cambiar su nombre.
- Se han identificado y corregido algunos bloqueos raros de Okular.
