---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nueva infraestructura: KActivitiesStats, una biblioteca para acceder al uso de datos estadísticos recopilados por el gestor de actividades de KDE.

### Todas las infraestructuras

Qt &gt;= 5.4 es ahora necesario (es decir, Qt 5.3 ya no se permite).

### Attica

- Añadir variante const para el método getter

### Baloo

- Centralizar el tamaño del lote en la configuración
- Eliminar el código que bloquea la indexación de archivos de texto sin formato que no tengan la extensión .txt (fallo 358098).
- Comprobar tanto el nombre del archivo como su contenido para determinar el tipo MIME (error 353512).

### BluezQt

- ObexManager: Dividir los mensajes de error de los objetos que faltan.

### Iconos Brisa

- añadir iconos de Lokalize para Brisa
- Sincronizar los iconos de las aplicaciones entre Brisa y Brisa oscuro.
- Actualizar los iconos del tema y eliminar los grupos de Kicker de corregir iconos del sistema de aplicaciones.
- Añadir el uso de xpi para los complementos de firefox (fallo 359913).
- Actualizar el icono de Okular con el correcto.
- Añadir compatibilidad con iconos de ktnef app
- Se han añadido los iconos «kmenueditor», «kmouse» y «knotes».
- Cambiar el icono de sonido silenciado para que use «-» para el estado silenciado en lugar de solo el color rojo (fallo 360953).
- Permitir el uso del tipo MIME «djvu» (error 360136).
- Añadir un enlace en lugar de una doble entrada.
- Añadir el icono «ms-shortcut» para gnucash (fallo 360776).
- Cambiar el fondo de pantalla a uno genérico.
- Actualizar los iconos para que usen una imagen de fondo genérica.
- añadir el icono para konqueror (error 360304)
- Añadir el icono «process-working» para la animación de avance en KDE (fallo 360304).
- Añadir el icono «install» para el software y actualizar el icono «update» con el color correcto.
- Se han añadido los iconos de emblema «add» y «remove» para la selección de Dolphin. También se ha añadido el icono «mount».
- Se ha eliminado la hoja de estilos de los iconos de las aplicaciones «analogclock» y «kickerdash».
- Sincronizar Brisa y Brisa oscuro (error 360294).

### Módulos CMake adicionales

- Corregir «_ecm_update_iconcache» para que actualice solo la ubicación de instalación.
- Revertir «ECMQtDeclareLoggingCategory: Incluir &lt;QDebug&gt; con el archivo generado».

### Integración con Frameworks

- Recurrir a la implementación «QCommonStyle» de «standardIcon».
- Definir un tiempo de espera predeterminado para el cierre del menú.

### KActivities

- Se han eliminado comprobaciones del compilador ahora que todas las infraestructuras necesitan C++11.
- Se ha eliminado «ResourceModel» de QML, ya que ha sido sustituido por «KAStats::ResultModel».
- Se ha corregido la inserción en un «QFlatSet» vacío que devolvía un iterador no válido.

### KCodecs

- Simplificar código (qCount -&gt; std::count, de cosecha propia isprint -&gt; QChar::isPrint).
- Detección de codificación: Corregir el fallo en el uso incorrecto de «isprint» (fallo 357341).
- Se ha corregido un fallo debido a una variable sin inicializar (error 357341).

### KCompletion

- KCompletionBox: Forzar ventana sin bordes y no definir el foco.
- «KCompletionBox» *not* debe ser una ayuda emergente.

### KConfig

- Permitir el uso de «QStandardPaths» para obtener ubicaciones dentro de los archivos de escritorio.

### KCoreAddons

- Se ha corregido «kcoreaddons_desktop_to_json()» en Windows.
- src/lib/CMakeLists.txt: Se ha corregido el enlazado de la biblioteca «Threads».
- Añadir código de relleno para permitir la compilación en Android.

### KDBusAddons

- Evitar la introspección de la interfaz DBus cuando no se usa.

### KDeclarative

- Uso uniforme de «std::numeric_limits».
- [DeclarativeDragArea] No sobrescribir «text» de los datos MIME.

### Soporte de KDELibs 4

- Se ha corregido un enlace obsoleto en el docbook de «kdebugdialog5».
- No perder «Qt5::Network» como biblioteca necesaria para el resto de las comprobaciones de la configuración.

### KDESU

- Definir macros de funcionalidades para activar la compilación con «musl libc».

### KEmoticons

- KEmoticons: Se ha corregido un fallo cuando «loadProvider» no funciona por algún motivo.

### KGlobalAccel

- Hacer que «kglobalaccel5» se pueda matar correctamente, lo que corrige un apagado extremadamente lento.

### KI18n

- Usar los idiomas de Qt locales del sistema como último recurso cuando no se está usando UNIX.

### KInit

- Se ha limpiado y refactorizado la adaptación «xcb» de «klauncher».

### KIO

- FavIconsCache: Sincronizar tras escribir para que otras aplicaciones lo vean, y evitar un fallo en la destrucción.
- Se han corregido muchos problemas de hilos en «KUrlCompletion».
- Corregir fallo en el menú de cambio de nombre (error 360488)
- KOpenWithDialog: Se ha corregido el título de la ventana y el texto de descripción (error 359233).
- Permitir un mejor despliegue multiplataforma de los esclavos de E/S empaquetando la información del protocolo en los metadatos del complemento.

### KItemModels

- KSelectionProxyModel: Simplificar el manejo de la eliminación de filas, simplificar la lógica de deseleccionar.
- KSelectionProxyModel: Volver a crear el mapeo durante la eliminación solo si es necesario (fallo 352369).
- KSelectionProxyModel: Borrar los mapeos de «firstChild» solo para el nivel principal.
- KSelectionProxyModel: Asegurar la emisión de señales correcta cuando se elimina el último elemento seleccionado.
- Hacer que se pueda buscar en «DynamicTreeModel» con el rol «display».

### KNewStuff

- No fallar si los archivos «.desktop» están ausentes o dañados.

### KNotification

- Contemplar el clic con el botón izquierdo sobre iconos heredados en la bandeja del sistema (fallo 358589).
- Usar el indicador «X11BypassWindowManagerHint» solo en la plataforma X11.

### Framework para paquetes

- Tras instalar un paquete, cargarlo.
- No fallar si el paquete existe y está actualizado.
- Se ha añadido «Package::cryptographicHash(QCryptographicHash::Algorithm)».

### KPeople

- Definir el URI del contacto como URI de persona en «PersonData» cuando no existe ninguna persona.
- Definir un nombre la para la conexión de la base de datos.

### KRunner

- Importar plantilla de lanzador de KAppTemplate.

### KService

- Corregir la nueva advertencia de «kbuildsycoca» cuando un tipo MIME hereda de un alias.
- Corregir el manejo de «x-scheme-handler/*» en «mimeapps.list».
- Se ha solucionado la gestión de x-scheme-handler/* en el análisis de mimeapps.list (error 358159)

### KTextEditor

- Revertir "Página de configuración de Abrir/Cerrar: Usar el término «Carpeta» en lugar de «Directorio»".
- forzar UTF-8
- Página de configuración de Abrir/Cerrar: Usar el término «Carpeta» en lugar de «Directorio».
- kateschemaconfig.cpp: Usar filtros correctos en los diálogos de abrir/cerrar (fallo 343327).
- c.xml: Usar el estilo predeterminado para controlar las palabras clave del flujo.
- isocpp.xml: usar el estilo predeterminado «dsControlFlow» para controlar las palabras clave del flujo.
- c/isocpp: Añadir más tipos estándares de C.
- «KateRenderer::lineHeight()» devuelve un «int».
- Impresión: Usar el tamaño del texto del esquema de impresión seleccionado (fallo 356110).
- Aceleración de cmake.xml: Usar «WordDetect» en lugar de «RegExpr».
- Cambiar la anchura de los tabuladores de 8 a 4.
- Se ha corregido el cambio de color de la línea actual.
- Se ha corregido la selección del elemento de terminación con el ratón (error 307052).
- Añadido el archivo de resaltado de sintaxis para gcode
- Se ha corregido el dibujo del fondo de la selección del minimapa.
- Se ha corregido la codificación de «gap.xml» (usar UTF-8).
- Corregidos los bloques de comentarios anidados (error 358692)

### KWidgetsAddons

- Tener en cuenta los márgenes del contenido al calcular las sugerencias de tamaño.

### KXMLGUI

- Corregir que la edición de las barras de herramientas pierdan las acciones conectadas.

### NetworkManagerQt

- ConnectionSettings: Inicializar el tiempo de espera del ping de la puerta de enlace.
- Nuevos tipos de conexiones TunSetting y Tun.
- Crear dispositivos para todos los tipos conocidos.

### Iconos de Oxígeno

- Instalar «index.theme» en el mismo directorio donde siempre había estado.
- Instalar en «oxygen/base/» para que el movimiento de iconos de las aplicaciones no entre en conflicto con la versión instalada por dichas aplicaciones.
- Replicar los enlaces simbólicos de los iconos Brisa.
- Añadir los nuevos iconos «emblem-added» y «emblem-remove» para sincronización con Brisa.

### Framework de Plasma

- [calendario] Corregir que la miniaplicación del calendario no borrara la selección al ocultarse (fallo 360683).
- Actualizar el icono de sonido para que use hojas de estilos.
- actualizado el icono de audio desactivado (error 360953)
- Corregir la creación forzada de miniaplicaciones cuando Plasma es inmutable.
- [Nodo de atenuación] No mezclar la opacidad de forma separada (fallo 355894).
- [Svg] No volver a analizar la configuración en respuesta a «Theme::applicationPaletteChanged».
- Diálogo: Definir los estados de «SkipTaskbar/Pager» antes de mostrar la ventana (fallo 332024).
- Volver a introducir la propiedad «busy» en «Applet».
- Asegurar que el archivo de exportación de «PlasmaQuick» se encuentra correctamente.
- No importar una disposición que no existe.
- Permitir que una miniaplicación ofrezca un objeto de prueba
- Sustituir «QMenu::exec» con «QMenu::popup».
- FrameSvg: Corregir punteros descompensados en «sharedFrames» cuando cambia el tema.
- IconItem: Programar una actualización del mapa de píxeles cuando la ventana cambia.
- IconItem: Animar los cambios «activo» y «habilitado» incluso con animaciones deshabilitadas.
- DaysModel: Hacer que «update» sea un «slot».
- [Elemento de icono] No animar desde el mapa de bits anterior cuando se ha hecho invisible.
- [Elemento de icono] No llamar a «loadPixmap» en «setColorGroup».
- [Applet] No sobrescribir el indicador «Persistente» de la notificación de deshacer.
- Permitir la sobrescritura del ajuste de mutabilidad de Plasma durante la creación de contenedores.
- Se ha añadido «icon/titleChanged».
- Eliminar dependencia de «QtScript».
- La cabecera «plasmaquick_export.h» está en la carpeta «plasmaquick».
- Instalar algunas cabeceras de «plasmaquick».

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
