---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Añadir «Qt5Network» como una dependencia pública.

### BluezQt

- Se ha corregido el directorio de inclusión en el archivo «pri».

### Iconos Brisa

- Añadir definiciones de prefijos de espacios de nombres que faltaban.
- Comprobar que los iconos SVG están formados correctamente.
- Corregir todos los iconos «edit-clear-location-ltr» (error 366519).
- Añadir compatibilidad con efectos de iconos a kwin.
- Cambiar el nombre de «caps-on» a «input-caps-on».
- Añadir iconos «caps» para la entrada de texto.
- Añadir algunos iconos específicos de GNOME de Sadi58.
- Se han añadido iconos de aplicaciones desde «gnastyle».
- Se han optimizado los iconos de Dolphin, Konsole y Umbrello para 16, 22 y 32 píxeles.
- Se ha actualizado el icono de VLC para 22, 32 y 48 píxeles.
- Se ha añadido un icono para la aplicación «Compositor de subtítulos».
- Se ha corregido el nuevo icono de «Kleopatra».
- Se ha añadido un icono para la aplicación «Kleopatra».
- Se han añadido iconos para «Wine» y «Wine-qt».
- Corregir error en palabras de presentación, gracias a Sadi58 (error 358495).
- Añadir el icono «system-log-out» en 32 píxeles.
- Añadir iconos «system-» de 32 píxeles, eliminar iconos «system-» a color.
- Añadir el uso de iconos de «pidgin» y «banshee».
- Eliminar el icono de la aplicación VLC debido a problemas con la licencia, añadir un nuevo icono para VLC (error 366490).
- Añadir el uso de iconos de «gthumb».
- Usar «HighlightedText» para iconos de carpetas.
- Los iconos de la carpeta «Lugares» usan ahora hojas de estilos (resaltado de color).

### Módulos CMake adicionales

- ecm_process_po_files_as_qm: Omitir traducciones no preparadas.
- El nivel predeterminado para las categorías de registro debe ser «Info» en lugar de «Warning».
- Documentar la variable ARGS en los destinos «create-apk-*».
- Crear una prueba para validar la información de «appstream» de los proyectos.

### Herramientas KDE Doxygen

- Añadir condición si las plataformas de los grupos no están definidas.
- Plantilla: Ordenar las plataformas alfabéticamente.

### KCodecs

- Traer de «kdelibs» el archivo usado para generar «kentities.c».

### KConfig

- Añadir la entrada «Donar» a «KStandardShortcut».

### KConfigWidgets

- Añadir la acción estándar «Donar».

### KDeclarative

- [kpackagelauncherqml] Asumir que el nombre del archivo de escritorio es el mismo que el de «pluginId».
- Cargar las preferencias de renderizado de QtQuick desde un archivo de configuración y asignar los valores predeterminados.
- icondialog.cpp: Corrección de compilación correcta que no oscurece «m_dialog».
- Corregir un fallo cuando no está disponible ninguna «QApplication».
- Exponer el dominio de traducción.

### Soporte de KDELibs 4

- Se ha corregido el error de compilación en Windows en «kstyle.h».

### KDocTools

- Añadir rutas para «config», «cache» y «data» a «general.entities».
- Actualizado con la versión en inglés.
- Añadir las entidades de las teclas «espacio» y «meta» a src/customization/en/user.entities

### KFileMetaData

- Solicitar «Xattr» solo si el sistema operativo es Linux.
- Restaurar compilación para Windows.

### KIdleTime

- [xsync] «XFlush» en «simulateUserActivity».

### KIO

- KPropertiesDialog: Eliminar nota de advertencia de la documentación, ya que el error ha desaparecido.
- [Programa de pruebas] Resolver rutas relativas usando «QUrl::fromUserInput».
- KUrlRequester: Corregir cuadro de error cuando se selecciona un archivo y se vuelve a abrir el diálogo de archivos.
- Proporcionar un método alternativo si los clientes no listan la entrada «.» (error 366795).
- Corregida la creación de enlaces simbólicos sobre el protocolo «desktop».
- KNewFileMenu: Usar «KIO::linkAs» al crear enlaces simbólicos en lugar de «KIO::link».
- KFileWidget: Se ha corregido el doble «/» en rutas.
- KUrlRequester: Usar la sintaxis estática de «connect()», era inconsistente.
- KUrlRequester: Pasar «window()» como padre de «QFileDialog».
- Evitar la llamada a «connect(null, ....)» desde «KUrlComboRequester».

### KNewStuff

- Descomprimir archivos en subcarpetas.
- Dejar de permitir la instalación en la carpeta de datos genérica debido a un potencial agujero de seguridad.

### KNotification

- Obtener la propiedad «ProtocolVersion» de «StatusNotifierWatcher» de modo asíncrono.

### Framework para paquetes

- Silenciar las advertencias de uso obsoleto de «contentHash».

### Kross

- Revertir «Eliminar dependencias de KF5 no usadas».

### KTextEditor

- Eliminar colisión de aceleradores (error 363738).
- Corregir el resaltado de direcciones de correo electrónico en doxygen (fallo 363186).
- Detectar más archivos JSON, como nuestros propios proyectos ;)
- Mejorar la detección de tipos MIME (error 357902).
- Error 363280 - resaltado de c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (error 363280).
- Error 363280 - resaltado de c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Error 351496 - El plegado de Python no funciona al empezar a teclear (error 351496).
- Error 365171 - Resaltado sintáctico de Python: no funciona correctamente con las secuencias de escape (error 365171).
- Error 344276 - «nowdoc» de PHP no se pliega correctamente (error 344276).
- Error 359613 - Algunas propiedades de CSS3 no están permitidas en el resaltado sintáctico (error 359613).
- Error 367821 - Sintaxis de wineHQ: La sección de los archivos «reg» no se resalta correctamente (error 367821).
- Mejorar el manejo de archivos de intercambio si se indica el directorio de intercambio.
- Corregir un fallo cuando se vuelven a cargar documentos con ajuste de líneas automático debido al límite de longitud de líneas (error 366493).
- Corregir fallos constantes relacionados con la barra de órdenes de «vi» (error 367786).
- Corrección: Los números de línea de los documentos impresos empiezan ahora por 1 (error 366579).
- Copia de seguridad de archivos remotos: Tratar los archivos montados como si también fueran archivos remotos.
- Simplificar la lógica de la creación de la barra de búsqueda.
- Añadir resaltado de sintaxis para Magma.
- Permitir un único nivel de recursividad.
- Corregir archivo de intercambio dañado en Windows.
- Parche: Añadir el uso de «bitbake» en el motor de resaltado sintáctico.
- Paréntesis automáticos: Contemplar el atributo de verificación ortográfica cuando se introduce el carácter (error 367539).
- Resaltar «QMAKE_CFLAGS».
- No salir del contexto principal.
- Añadir algunos nombres de ejecutables de uso común.

### KUnitConversion

- Añadir la unidad de masa «stone» británica.

### Framework KWallet

- Mover el docbook «kwallet-query» al subdirectorio correcto.
- Corregir el uso de palabras «an -&gt; one».

### KWayland

- Hacer que sea opcional «linux/input.h» en tiempo de compilación.

### KWidgetsAddons

- Corregir el fondo de los caracteres que no son BMP.
- Añadir búsqueda UTF-8 con secuencias de escape en octal de C.
- Hacer que el «KMessageBoxDontAskAgainMemoryStorage» predeterminado se guarde en «QSettings».

### KXMLGUI

- Adaptación a la acción estándar «Donar».
- Adaptar el uso obsoleto de «authorizeKAction».

### Framework de Plasma

- Se ha corregido el icono de dispositivo de 22 píxeles que no funcionaba en el archivo anterior.
- WindowThumbnail: Hacer las llamadas a GL en el hilo correcto (error 368066).
- Hacer que plasma_install_package funcione con KDE_INSTALL_DIRS_NO_DEPRECATED.
- Añadir margen y relleno al icono start.svgz
- Se han corregido cosas en la hoja de estilos del icono «computer».
- Añadir iconos de computadora y de portátil para kicker (error 367816).
- Se ha corregido la advertencia de no poder asignar «undefined» a «double» en «DayDelegate».
- Se ha corregido «Los archivos svgz con hojas de estilos no me quieren».
- Se ha cambiado el nombre de los iconos de 22 píxeles a «22-22-x» y los de 32 píxeles a «x» para kicker.
- [TextField de PlasmaComponents] No preocuparse por la carga de iconos para los botones no usados.
- Salvaguardia adicional en «Containment::corona» para el caso especial de la bandeja del sistema.
- Al marcar un contenedor como borrado, marcar también todos los subprogramas como borrados (corrige que no se borren las configuraciones de los contenedores de la bandeja del sistema).
- Corregir el icono de notificación de dispositivos.
- Añadir «system-search» a «system» en los tamaños de 32 y 22 píxeles.
- Añadir iconos monocromos para «kicker».
- Definir el esquema de color en el icono «system-search».
- Mover «system-search» a «system.svgz».
- Se ha corregido el «X-KDE-ParentApp» erróneo o ausente en las definiciones de archivos de escritorio.
- Corrección de «dox» de la API de Plasma::PluginLoader: Mezcla de «applets/dataengine/services/..».
- Se ha añadido el icono «system-search» para el tema de «sddm».
- Añadir icono de 32 píxeles para «nepomuk».
- Actualizar el icono «touchpad» para la bandeja del sistema.
- Eliminar código que no se podía ejecutar.
- [ContainmentView] Mostrar los paneles cuando la interfaz de usuario esté lista.
- No volver a declarar la propiedad «implicitHeight».
- Usar «QQuickViewSharedEngine::setTranslationDomain» (error 361513).
- Permitir el uso de iconos Brisa de Plasma de 22 y 32 píxeles.
- Eliminar los iconos a color del sistema y añadir los monocromos de 32 píxeles.
- Añadir un botón opcional para mostrar la contraseña en «TextField».
- Las ayudas emergentes estándares se escriben al revés en los idiomas de que se escriben de derecha a izquierda.
- Se ha mejorado enormemente el rendimiento durante el cambio de mes en el calendario.

### Sonnet

- No escribir con minúscula los nombres de los idiomas al analizar trigramas.
- Se ha corregido un bloqueo inmediato durante el inicio debido a un puntero nulo a un complemento.
- Manejar diccionarios con nombres incorrectos.
- Sustituir la lista hecha a mano del mapeo de guiones de idiomas y usar los nombres correctos de los idiomas.
- Añadir una herramienta para generar trigramas.
- Corregir un poco la detección de idiomas.
- Usar el lenguaje seleccionado como sugerencia para la detección.
- Usar los correctores ortográficos en caché durante la detección del idioma; se mejora un poco el rendimiento.
- Mejorar la detección de idiomas.
- Filtrar la lista de sugerencias contra los diccionarios disponibles, eliminando duplicados.
- Acordarse de añadir la última coincidencia de trigrama.
- Comprobar si alguno de los trigramas coincide realmente.
- Manejar múltiples idiomas con la misma puntuación durante la coincidencia de trigramas.
- No comprobar el tamaño mínimo dos veces.
- Reducir la lista de idiomas entre los disponibles.
- Usar la misma longitud mínima en todas partes durante la detección del idioma.
- Comprobación de saneamiento de que el modelo cargado tiene la cantidad correcta de trigramas para cada idioma.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
