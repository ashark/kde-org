---
aliases:
- ../announce-applications-16.08.1
changelog: true
date: 2016-09-08
description: KDE에서 KDE 프로그램 16.08.1 출시
layout: application
title: KDE에서 KDE 프로그램 16.08.1 출시
version: 16.08.1
---
September 8, 2016. Today KDE released the first stability update for <a href='../16.08.0'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

kdepim, Kate, Kdenlive, Konsole, Marble, Kajongg, Kopete, Umbrello 등에 45개 이상의 버그 수정 및 기능 개선이 있었습니다.

This release also includes Long Term Support version of KDE Development Platform 4.14.24.
