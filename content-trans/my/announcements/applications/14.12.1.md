---
aliases:
- ../announce-applications-14.12.1
changelog: true
date: '2015-01-08'
description: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၄.၁၂.၁ တင်ပို့ခဲ့သည်။
layout: application
title: ကေဒီအီးသည် ကေဒီအီးအပ္ပလီကေးရှင်းများ ၁၄.၁၂.၁ တင်ပို့ခဲ့သည်
version: 14.12.1
---
January 13, 2015. Today KDE released the first stability update for <a href='../14.12.0'>KDE Applications 14.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 50 recorded bugfixes include improvements to the archiving tool Ark, Umbrello UML Modeller, the document viewer Okular, the pronunciation learning application Artikulate and remote desktop client KRDC.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.15, KDE Development Platform 4.14.4 and the Kontact Suite 4.14.4.
