---
date: 2013-08-14
hidden: true
title: Les applications de KDE 4.11 font un grand pas en avant dans la gestion des
  informations personnelles et des améliorations disséminées un peu partout
---
Le gestionnaire de fichiers Dolphin reçoit plusieurs petites corrections et améliorations dans cette version. Le chargement de gros fichiers a été accéléré et nécessite jusqu'à 30&#37; de mémoire en moins. Les activités importantes avec le disque dur ou le processeur ont été éliminées en chargeant que des aperçus autour des éléments visibles. Il a eu beaucoup d'autres améliorations : par exemple, de nombreux bogues affectant le développement de dossiers dans la vue détaillée ont été corrigés ; aucune icône inconnue avec un espace réservé ne sera plus affichée en entrant dans un dossier. Un clic avec le bouton central de la souris sur une archive ouvre maintenant un nouvel onglet avec le contenu de l'archive, apportant une ergonomie générale plus cohérente.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Le nouveau processus de traitement de données pour l'envoi différé dans Kontact` width="600px">}}

## Améliorations de la suite Kontact

La suite Kontact a bénéficié une fois de plus d'une attention particulière concernant la stabilité, les performances et l'utilisation mémoire. L'importation de dossiers, le basculement entre cartes, le téléchargement de courriers électroniques, le marquage ou le déplacement de grandes quantités de messages et le temps de démarrage ont tous été améliorés durant les six derniers mois. Veuillez consulter <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>ce blog</a> pour plus de détails. La <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/ '>fonctionnalité d'archivage a reçu de nombreuses corrections de bogues</a>. Il a eu aussi des améliorations apportées dans l'assistant d'importation, permettant l'importation de paramètres venant du client de messagerie électronique Trojita et une meilleure importation venant de diverses autres applications. Pour plus d'informations, veuillez consulter <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>cette page</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`L'agent d'archivage gère l'enregistrement des courriers électroniques sous un format compressé` width="600px">}}

Cette version apporte aussi certaines nouvelles fonctionnalités significatives. Il y a un <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>nouvel éditeur de thème pour les en-têtes de courrier électronique</a> et les images de courrier électronique peuvent être redimensionnées à la volée. La <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>fonctionnalité « Envoyer plus tard » </a> vous permet de planifier vos envois de courriers électroniques selon une date et une heure données, avec la possibilité supplémentaire de renouveler l'envoi selon un intervalle donné. La prise en charge du filtre « Sieve » de KMail (une fonctionnalité « IMAP » permettant le filtrage sur le serveur) a été améliorée, les utilisateurs peuvent produire des scripts de filtrage « Sieve » <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'> avec une interface intuitive</a>. Dans le domaine de la sécurité, KMail intègre une « détection des arnaques », <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>, affichant une alarme</a> lorsque des courriers électroniques contiennent des éléments classique d'hameçonnage. Vous recevrez maintenant une <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>notification informative</a> quand un nouveau courrier électronique arrive. Enfin, mais pas le moins important, le générateur de blogs, Blogilo, intègre maintenant un éditeur « HTML » largement amélioré reposant sur « QtWebKit ».

## Prise en charge étendu de langues pour Kate

L'éditeur de texte avancé Kate bénéficie de nouveaux modules externes Python (2 et 3), JavaScript, JQuery, Django et XML. Ces moteurs prennent en charge des fonctionnalités telles que l'auto-complètement statique et dynamique, des vérificateurs syntaxiques, l'insertion de fragments de code ainsi que la possibilité d'indentation XML automatique avec un raccourci. Mais, il y beaucoup plus pour les fans de Python : une console Python fournissant des informations détaillées sur un fichier de source ouvert. Quelques améliorations mineures d'interface utilisateur ont été aussi apportées, incluant de nouvelles notifications passives pour la fonctionnalité de recherche (Voir « http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/ optimizations to the VIM mode http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/ » ) et une nouvelle fonctionnalité de retour à la ligne de texte (Voir « http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/ ).

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars affiche les évènements intéressants à venir qui sont visibles de votre position` width="600px">}}

## Autres améliorations d'applications

Dans les domaines du jeu et de l'éducation, plusieurs nouvelles fonctionnalités et optimisations plus petites mais plus larges sont apportées. Les dactylos faisant de la prospective apprécieront la prise en charge la frappe de droite à gauche dans KTouch, pendant que l'outil pour les fans d'astronomie, KStars, obtient un outil montrant les évènements intéressants qui vont arriver dans votre environnement. Les outils de mathématiques, Rocs, Kig, Cantor et KAlgebra ont tous été traités avec attention, prenant en charge plus de moteurs et de calculs. Le jeu KJumpingCube obtient maintenant des fonctionnalités pour des tailles de grilles plus importantes, de nouveaux niveaux de difficultés, des temps de réponses plus courts et une interface utilisateur améliorée.

L'application de simple coloration, KolourPaint, peut traiter le format d'images « WebP ». L'afficheur universel de documents, Okular, reçoit des outils configurables d'analyse et introduit la prise en charge du « Annuler » / « Refaire » pour les formulaires et les annotations. Le lecteur et l'outil d'annotation audio JuK prend en charge la lecture et la modification des métadonnées pour le nouveau format audio « Ogg Opus » (cependant, ceci nécessite un pilote audio et un pilote « TagLib » prennent aussi en charge « Ogg Opus »).

#### Installation des applications de KDE

Les logiciels de KDE, y compris toutes ses bibliothèques et ses applications, sont disponibles gratuitement sous des licences « Open Source ». Ils fonctionnent sur diverses configurations matérielles, sur des architectures processeurs comme « ARM » ou « x86 » et sur des différents systèmes d'exploitation. Ils utilisent tout type de gestionnaire de fenêtres ou environnements de bureaux. A côté des systèmes d'exploitation Linux ou reposant sur UNIX, vous pouvez trouver des versions « Microsoft Windows » de la plupart des applications de KDE sur le site <a href='http://windows.kde.org'>Logiciels KDE sous Windows</a> et des versions « Apple Mac OS X » sur le site <a href='http://mac.kde.org/'>Logiciels KDE sous Mac OS X</a>. Des versions expérimentales des applications de KDE peuvent être trouvées sur Internet pour diverses plate-formes mobiles comme « MeeGo », « MS Windows Mobile » et « Symbian » mais qui sont actuellement non prises en charge. <a href='http://plasma-active.org'>Plasma Active</a> est une interface utilisateur pour une large variété de périphériques comme des tablettes et d'autres matériels mobiles. <br />

Les logiciels KDE peuvent être obtenus sous forme de code source et de nombreux formats binaires à l'adresse <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a>. Ils peuvent être aussi obtenus sur <a href='/download'>CD-ROM</a> ou avec n'importe quelle distribution <a href='/distributions'>majeure de systèmes GNU / Linux et UNIX</a> publiée à ce jour.

##### Paquets

Quelques fournisseurs de systèmes Linux / Unix mettent à disposition gracieusement des paquets binaires de 4.11.0 pour certaines versions de leurs distributions. Dans les autres cas, des bénévoles de la communauté le font aussi. <br />

##### Emplacements des paquets

Pour obtenir une liste courante des paquets binaires disponibles, connus par l'équipe de publication de KDE, veuillez visiter le <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Wiki de la communauté</a>.

Le code source complet pour 4.11.0 peut être <a href='/info/4/4.11.0'>librement téléchargé</a>. Les instructions pour la compilation et l'installation des logiciels KDE 4.11.0 sont disponibles à partir de la <a href='/info/4/4.11.0#binary'>Page d'informations 4.11.0</a>.

#### Configuration minimale du système

Pour bénéficier au maximum de ces nouvelles versions, l'utilisation d'une version récente de Qt est recommandée, comme la version 4.8.4. Elle est nécessaire pour garantir un fonctionnement stable et performant, puisque certaines améliorations apportées aux logiciels de KDE ont été réalisées dans les bibliothèques Qt utilisées.<br /> Pour utiliser pleinement toutes les possibilités des logiciels de KDE, l'utilisation des tout derniers pilotes graphiques pour votre système est recommandée, puisque que ceux-ci peuvent grandement améliorer votre expérience d'utilisateur, à la fois dans les fonctionnalités optionnelles que dans les performances et la stabilité générales.

## Également annoncé aujourd'hui : 

## <a href="../plasma"><img src="/announcements/announce-4.11/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Les espaces de travail de Plasma 4.11 continuent à améliorer l'expérience utilisateur</a>.

Engagé dans une maintenance à long terme, les environnements de bureaux Plasma apportent de nombreuses améliorations aux fonctionnalités de base, avec une barre de tâches plus facile, un composant graphique de batterie plus intelligent et un mélangeur de sons amélioré. L'introduction de KScreen apporte une gestion intelligente du mode multi-écran à l'environnement de bureaux et des améliorations très importantes de performances, conjugué avec de petits réglages d'ergonomie nécessaires à un confort général agréable.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/>La plate-forme 4.11 de KDE fournit de meilleures performances</a>.

Cette version de la plate-forme KDE 4.11 continue à se focaliser sur la stabilité. De nouvelles fonctionnalités ont été implémentées pour la version 5.0 du futur environnement de développement de KDE. Mais, il a été prévu de limiter les optimisations pour l'environnement Nepomuk dans les versions stables.
