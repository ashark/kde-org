---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Correction d'un plantage durant le chargement du fournisseur en vérifiant le pointeur fourni avant son déréférencement (bogue 427974)

### Baloo

* [DocumentUrlDB] Suppression de l'entrée de la liste fille de la base de données si vide.
* Ajout d'un type de document de présentation pour le diaporama « Office OpenXML » et son modèle.
* [MetaDataMover] Correction de la recherche de l'identifiant du document parent.
* [DocumentUrlDB] Ajout d'une méthode pour les renommages et les déplacements triviaux.
* [MetaDataMover] Faire des renommages une simple opération de manipulation de base de données
* [Document] Ajout d'un identifiant de document parent et le mettre à disposition.
* Remplacement d'un appel direct à « syslog » avec un message de journalisation par catégorie

### Icônes « Breeze »

* Ajout de variantes avec champs de texte : -frameless (=> champ textuel), -framed
* Ajout d'un lien symbole de nom symbolique pour les icônes « input-* »
* Ajour d'une icône pour la police « True Type XML »
* Ajout de menu « Ajout de sous-titre »
* Modification de l'icône de « MathML » pour l'utilisation d'une formule et un type « MIME » spécifique.
* Ajout d'une icône pour l'image disque « QEMU » et les images « SquashFS ».
* Ajout d'une icône de modification / déplacement
* Ajoute une icône pour les vidages mémoire (Core Dump)
* Ajout d'un ensemble de types « MIME » pour les sous-titres
* Suppression du flou inutile à partir de l'icône de Kontrast

### Modules additionnels « CMake »

* Correction de l'extraction des catégories à partir des fichiers de bureau
* Define install dir variable for file templates
* Ajout de la génération à grande vitesse des métadonnées pour les compilations sous Android
* (Qt) WaylandScanner : marquage correct des fichiers comme « SKIP_AUTOMOC »

### KActivitiesStats

* ResultModel: distribution du type « MIME » des ressources
* Ajout d'un filtrage des évènements pour les fichiers et les dossiers (bogue 428085)

### KCalendarCore

* Correction du mainteneur, supposant être Allen et non moi :)
* Ajout de la prise en charge de la propriété « CONFERENCE »
* Ajout d'un méthode de confort « alarmsTo » à Calendar
* Vérification que les récurrences quotidiennes ne précédent pas « dtStart »

### KCMUtils

* Suppression du contournement cassant les modules « kcm » multi-niveaux dans le mode d'icônes.

### KConfig

* Correction de « KConfigGroup::copyTo » avec « KConfigBase::Notify » (bogue 428771)

### KCoreAddons

* Suppression du plantage quand l'atelier est vide (Quand un erreur est retournée)
* KFormat : ajout de plus de cas avec des heures et des dates relatives
* Activation de « KPluginFactory » pour transmettre de façon optionnelle « KPluginMetaData » vers les modules externes.

### KDAV

* Suppression de celui-ci par suite de la génération de trop d'erreurs

### KDeclarative

* Synchronisation des marges à partir de « AbstractKCM » dans « SimpleKCM »
* Suppression du texte obsolète de licence
* Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
* Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
* Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
* Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
* Ré-écriture de « KeySequenceItem » (et son assistant) pour l'utilisation de « KeySequenceRecorder » (bogue 427730)

### KDESU

* Analyse correcte avec les quotes avec double caractère d'échappement
* Ajout de la prise en charge de « DOA »pour « OpenBSD »

### KFileMetaData

* Correction de certaines fuites mémoire dans les extracteurs « OpenDocument » et « Office OpenXML ».
* Ajout de plusieurs sous-types pour les documents « OpenDocument » et « Office OpenXML ».

### KGlobalAccel

* Chargement des modules externes statiquement reliés pour l'interface de « kglobalacceld ».

### Modules externes pour l'interface graphique utilisateur de KDE

* Retour à un fonctionnement correct de l'inhibition de raccourcis à partir de « get-go » (bug 407395)
* Correction d'un plantage potentiel dans la fin de communication de l'inhibiteur de « Wayland » (Bogue 429267)
* CMake : recherche de « Qt5::GuiPrivate » quand la prise en charge de « Wayland » est activée.
* Ajout de « KeySequenceRecorder » comme base pour « KKeySequenceWidget » et « KeySequenceItem » (bogue 407395)

### KHolidays

* Correction de l'arrondi pour les évènements de position du soleil, 30 secondes avant la prochaine heure.
* Suppression du parcours du fichier des jours fériés deux fois dans « defaultRegionCode() »
* Calcul des saisons astronomiques, une seule fois par occurrence.
* Correction de la recherche de jours fériés régionaux pour les codes « ISO 3166-2 ».
* Capacité de copier / déplacer les jours fériés régionaux
* Ajout de la prise en charge du calcul pour les heures de crépuscule civil

### KIdleTime

* Chargement des modules d'interrogation du système statiquement liés

### KImageFormats

* Suppression de la diminution de la profondeur de couleurs de 8 bits pour les fichiers « psd » non compressés.

### KIO

* NewFile Dialog : autorisation d'accepter la création du dossier avant que « stat » se soit lancé (bogue 429838)
* Ne pas avoir de fuite de mémoire pour le processus « DeleteJob »
* KUrlNavBtn : vérification du fonctionnement de l'ouverture des sous-dossiers à partir du menu à liste déroulante avec un clavier (bogue 428226)
* Menu à liste déroulante : utilisation des traductions de raccourcis
* [ExecutableFileOpenDialog] Focus sur le bouton « Annulation »
* Affichage des emplacements : surlignement de l'emplacement, uniquement lors de son affichage (bogue 156678).
* Ajout d'une propriété pour l'affichage des actions de modules externes dans le sous-menu « Actions ».
* Suppression de la méthode nouvellement introduite.
* KIO::iconNameForUrl : détermination de l'icône pour les fichiers distants (bogue 429530)
* [kfilewidget] Utilisation d'un nouveau raccourci standard pour « Créer un dossier »
* Remaniement du chargement du menu contextuel et passage à plus de modularité
* RenameDialog : permission d'écraser les fichiers quand plus anciens (bug 236884)
* DropJob : utilisation d'un nouvelle icône pour modifier / déplacer pour « Déplacer ici ».
* Correction de la génération de « moc_predefs.h » quand « ccache » est activé grâce à -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* Qt 5.13 est maintenant nécessaire, supprimant ainsi « ifdef ».
* Portage de « KComboBox » vers « QComboBox »
* Correction de la documentation demandée par Méven Car @meven.
* Remaniement de certaines boucles en utilisant le récent C++
* Nettoyage du code mort
* Suppression de la vérification redondante si la clé existe
* Simplification du code pour « RequiredNumberOfUrls »
* Suppression de certaines lignes vides
* Simplifier le code et le rendre plus cohérent
* ioslaves : correction des permissions pour « remote :/ root »
* KFileItem : utilisation par « isWritable » de « KProtocolManager » pour les fichiers distants
* Ajout d'une surcharge aux actions de préfixage dans le menu « Actions »
* Utilisation d'un style de codage moderne
* Suppression des séparateurs non nécessaires (bogue 427830)
* Utilisation de « initializer_list » renforcé plutôt que << operator
* MkPathJob : ré-écriture du code avec compilation conditionnelle pour l'amélioration de la lisibilité.

### Kirigami

* Définition de l'en-tête de « GlobalDrawer » par la position des barres d'outils / barre d'onglets /boîtes de dialogue avec boutons.
* Propriété attachée « inViewport »
* Correction du curseur d'en-tête sur périphériques tactiles
* Refactor AbstractapplicationHeader with a new "ScrollIntention" concept
* on desktop always fill anchors to parent
* [controls/BasicListItem]: Don't anchor to nonexistent item
* Don't display avatar text on small size
* Remove # and @ from the extraction process of avatar initial
* Initialisation de la propriété dans « sizeGroup »
* use mouse interaction on isMobile for easier testing
* Hotfix leading/trailing using trailing values for leading separator margin
* [controls/BasicListItem]: Add leading/trailing properties
* fix sheet positioning on contents resize
* Apply old behavior in wide mode and don't add topMargin in FormLayout
* Correction de la disposition du formulaire sur les petits écrans
* You can't use an AbstractListItem in a SwipeListItem
* Fix "Unable to assign [undefined] to int" in OverlaySheet
* [overlaysheet]: Don't do a janky transition when content height changes
* [overlaysheet]: Animate height changes
* Correction du positionnement des feuilles en superposition
* always set index when clicking on a page
* Correction du déplacement des FAB en mode « RTL ».
* Refine list separator appearance (bug 428739)
* Correction des poignées de tiroir en mode « RTL »
* Fix rendering borders the proper size with software fallback (bug 427556)
* Don't place software fallback item outside of shadowed rectangle item bounds
* Use fwidth() for smoothing in low power mode (bug 427553)
* Réalisation d'un rendu de couleur d'arrière-plan dans le mode d'économie d'énergie
* Enable transparent rendering for Shadowed(Border)Texture in lowpower
* Do not cancel alpha components for shadowed rectangle in low power mode
* Don't use a lower smoothing value when rendering ShadowedBorderTexture's texture
* Remove "cut out" steps from shadowed rectangle and related shaders
* Use icon.name instead of iconName in the doc
* [Avatar] Make icon use an icon size close to the text size
* [Avatar] Make the initials use more space and improve vertical alignment
* [Avatar] Expose sourceSize and smooth properties for anyone who wants to animate the size
* [Avatar] Set the source size to prevent images from being blurry
* [Avatar] Set the foreground color once
* [Avatar] Modification du dégradé d'arrière-plan
* [Avatar] Change border width to 1px to match other bother widths
* [Avatar] Make padding, verticalPadding and horizontalPadding always work
* [avatar] : ajouter un gradient aux couleurs

### KItemModels

* KRearrangeColumnsProxyModel: only column 0 has children

### KMediaPlayer

* Installation des fichiers de définition pour les types de services pour le lecteur et le moteur, grâce à un type de correspondance par nom de fichier.

### KNewStuff

* Fix uninstalling when the entry isn't cached
* When we call to check for updates, we expect updates (bug 418082)
* Réutiliser la boîte de dialogue « QWidgets » (bogue 429302)
* Wrap compatibility block in KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* Do not write cache for intermediate states
* Use enum for uncompression instead of string values
* Fix entry disappearing too early from updatable page (bug 427801)
* Add DetailsLoadedEvent enum to new signal
* Remaniement de l'API d'adoption (bogue 417983)
* Fix a couple of stragglers with old provider url
* Remove entry from cache before inserting new entry (bug 424919)

### KNotification

* Don't pass transient hint (bug 422042)
* Fix case-sensitive error of AppKit header on macOS
* Ne pas invoquer des actions de notifications non valables
* Correction de la gestion mémoire dans « notifybysnore »

### Environnement de développement « KPackage »

* Ignorer « X-KDE-PluginInfo-Depends »

### KParts

* Deprecate embed() method, for lack of usage
* Make KParts use KPluginMetaData instead of KAboutData

### KQuickCharts

* Reprise de l'algorithme de lissage des lignes
* Move applying interpolation to the polish step
* Properly center point delegates on line chart and size them with line width
* Add a "smooth" checkbox to line chart example
* Ensure line chart point delegates are properly cleaned up
* Also show name in tooltip on Line chart page example
* Document LineChartAttached and fix a typo in LineChart docs
* Add name and shortName properties to LineChartAttached
* Document pointDelegate property more thoroughly
* Remove previousValues member and fix stacked line charts
* Use pointDelegate in Line chart example to display values on hover
* Ajout de la prise en charge de « point delegate » pour les graphiques en lignes
* LineChart: Move point calculation from updatePaintNode to polish

### KRunner

* Obsolescence des résidus des paquets de KDE4
* Make use of new KPluginMetaData plugin constructor support of KPluginLoader

### KService

* [kapplicationtrader] Correction de la documentation des « API »
* KSycoca: recreate DB when version < expected version
* KSycoca: Keep track of resources Files of KMimeAssociation

### KTextEditor

* Portage de « KComboBox » vers « QComboBox »
* Utilisation de « KSyntaxHighlighting » pour « themeForPalette »
* fix i18n call, missing argument (bug 429096)
* Amélioration de la sélection automatique de thème

### KWidgetsAddons

* Don't emit the passwordChanged signal twice
* Add KMessageDialog, an async-centric variant of KMessageBox
* Restore the old default popup mode of KActionMenu
* Port KActionMenu to QToolButton::ToolButtonPopupMode

### KWindowSystem

* Load statically linked integration plugins
* Portage de la fonction « pid() » vers « processId() »

### KXMLGUI

* Introduce HideLibraries and deprecate HideKdeVersion
* Rewrite KKeySequenceWidget to use KeySequenceRecorder (bug 407395)

### Environnement de développement de Plasma

* [Representation] Only remove top/bottom padding when header/footer is visible
* [PlasmoidHeading] Use technique from Representation for inset/margins
* Ajout d'un composant de représentation
* [Desktop theme] Rename hint-inset-side-margin to hint-side-inset
* [FrameSvg] Renommage de « insetMargin » en « inset »
* [PC3] Use PC3 Scrollbar in ScrollView
* [Brise] Indication de l'encart du rapport
* [FrameSvg*] Rename shadowMargins to inset
* [FrameSvg] Cache shadow margins and honor prefixes
* Finish the animation before changing the length of the progressbar highlight (bug 428955)
* [textfield] Fix clear button overlapping text (bug 429187)
* Show drop menu at correct global position
* Use gzip -n to prevent embedded buildtimes
* Use KPluginMetaData to list containmentActions
* Port packageStructure loading from KPluginTrader
* Utilisation de « KPluginMetaData » pour lister « DataEngines »
* [TabBar] Add highlight on keyboard focus
* [FrameSvg*] Exposer les marges avec ombrage
* Make MarginAreasSeparator value more clear
* [TabButton] Align center icon and text when text is beside the icon
* [SpinBox] Fix logic error in scroll directionality
* Correction de la barre de défilement mobile en mode « RTL »
* [PC3 ToolBar] Ne pas désactiver les bordures
* [PC3 ToolBar] Use correct svg margin properties for padding
* [pc3 / scrollview] Suppression de « Alignement sur pixel »
* Ajout des zones de marge

### Motif

* [bluetooth] Fix sharing multiple files (bug 429620)
* Read translated plugin action label (bug 429510)

### QQC2StyleBridge

* button: rely on down, not pressed for styling
* Reduce the size of round buttons on mobile
* Correction de la barre de défilement mobile en mode « RTL »
* Correction de la barre de progression en mode « RTL »
* Correction de l'affichage « RTL » pour « RangeSlider »

### Opaque

* Inclusion de « errno.h » pour « EBUSY / EPERM »
* FstabBackend: return DeviceBusy where umount failed on EBUSY (bug 411772)
* Fix detection of recent libplist and libimobiledevice

### Coloration syntaxique

* fix dependencies of the generated files
* indexer: fix some issues and disable 2 checkers (capture group and keyword with delimiter)
* indexer: load all xml files in memory for easy checking
* Coloration syntaxique C++ : mise à jour vers Qt 5.15
* relaunch syntax generators when the source file is modified
* Composant « systemd » : mise à jour vers la version v247 de systemd
* ILERPG : simplifier et tester
* Zsh, Bash, Fish, Tcsh: add truncate and tsort in unixcommand keywords
* Latex: some math environments can be nested (bug 428947)
* Bash : nombreuses corrections et améliorations
* Ajout de « --syntax-trace=stackSize »
* php.xml : correction de la correspondance « endforeach »
* Move bestThemeForApplicationPalette from KTextEditor here
* debchangelog : ajout de « Trixie »
* alert.xml: Add `NOQA` yet another popular alert in source code
* cmake.xml: Upstream decided to postpone `cmake_path` for the next release

### Informations sur la sécurité

Le code publié a été signé en « GPG » en utilisant la clé suivante : pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
