---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: 'KDE Uygulamalar 19.08''i Gönderdi. '
layout: application
major_version: '19.08'
release: applications-19.08.3
title: KDE Uygulamalar 19.08.3'ü Gönderdi
version: 19.08.3
---
{{% i18n_date %}}

Bugün KDE  <a href='../19.08.0'>KDE Uygulamaları 19.08</a>için üçüncü kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than a dozen recorded bugfixes include improvements to Kontact, Ark, Cantor, K3b, Kdenlive, Konsole, Okular, Spectacle, Umbrello, among others.

İyileştirmeler şunları içerir:

- In the video-editor Kdenlive, compositions no longer disappear when reopening a project with locked tracks
- Okular's annotation view now shows creation times in local time zone instead of UTC
- Spectacle ekran görüntüsü yardımcı programında klavye kontrolü geliştirildi
