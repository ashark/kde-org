---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE, KDE Uygulamaları 15.04.3'ü Gönderdi
layout: application
title: KDE, KDE Uygulamaları 15.04.3'ü Gönderdi
version: 15.04.3
---
1 Temmuz 2015. Bugün KDE, <a href='../15.04.0'>KDE Uygulamaları</a> 15.04 için üçüncü kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

More than 20 recorded bugfixes include improvements to kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta and umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.21, KDE Development Platform 4.14.10 and the Kontact Suite 4.14.10.
