---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (non se forneceu un historial de cambios)

### KConfig

- Xerar clases compatíbeis con QML usando kconfigcompiler

### KCoreAddons

- Novo macro de CMake kcoreaddons_add_plugin para crear complementos baseados en KPluginLoader de maneira máis doada.

### KDeclarative

- Corrixir unha quebra na caché de texturas.
- e outras correccións

### KGlobalAccel

- Engadir o novo método globalShortcut que obtén o atallo como está definido na configuración global.

### KIdleTime

- Evitar que KIdleTime quebre na plataforma Wayland

### KIO

- Engadíronse KPropertiesDialog::KPropertiesDialog(urls) e KPropertiesDialog::showDialog(urls).
- Obtención asíncrona de datos baseada en QIODevice para KIO::storedPut e KIO::AccessManager::put.
- Corrixir as condicións co valor de retorno de QFile::rename (fallo 343329)
- Corrixiuse KIO::suggestName para suxerir nomes mellores (fallo 341773)
- kioexec: Corrixiuse a ruta do lugar que permite escritura de kurl (fallo 343329)
- Só almacenar os marcadores en user-places.xbel (fallo 345174)
- Entrada duplicada de RecentDocuments se dous ficheiros distintos teñen o mesmo nome
- Mellor mensaxe de erro se un único ficheiro é grande de máis para o lixo (fallo 332692)
- Corrixir a quebra de KDirLister ao redirixir canto a rañura chama a openURL

### KNewStuff

- Novo conxunto de clases, chamadas KMoreTools e relacionadas. KMoreTools axuda a engadir consellos sobre ferramentas externas que poderían non estar instaladas aínda. Ademais, acurta os menús longos fornecendo unha sección principal e máis que tamén poden configurar os usuarios.

### KNotifications

- Corrixir KNotifications cando se usa co NotifyOSD de Ubuntu (fallo 345973)
- Non causar actualizacións de notificacións ao definir as mesmas propiedades (fallo 345973)
- Introducir a marca LoopSound, que permite que as notificacións reproduzan un son en bucle se se necesita (fallo 346148)
- Non quebrar se a notificación non ten un trebello

### KPackage

- Engadir unha función KPackage::findPackages similar a KPluginLoader::findPlugins

### KPeople

- Usar KPluginFactory en vez de KService para instalar os complementos (KService mantense por compatibilidade).

### KService

- Corrixir unha división incorrecta de ruta de entrada (fallo 344614)

### KWallet

- Agora o axente de migración tamén comproba que a carteira vella estea baleira antes de empezar (fallo 346498)

### KWidgetsAddons

- KDateTimeEdit: corrección para que a entrada do usuario se rexistre de verdade. Corrixir marxes duplas.
- KFontRequester: corrixir que se seleccionen só fontes monoespazo

### KWindowSystem

- Non requirir QX11Info en KXUtils::createPixmapFromHandle (fallo 346496)
- novo método NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- Corrixir os atallos cando o atallo secundario está definido (fallo 345411)
- Actualizar a lista de produtos e compoñentes de Bugzilla para informar de fallos (fallo 346559)
- Atallos globais: permitir configurar tamén o atallo alternativo

### NetworkManagerQt

- As cabeceiras instaladas agora organízanse como no resto de infraestruturas.

### Infraestrutura de Plasma

- Agora PlasmaComponents.Menu permite seccións
- Usar KPluginLoader en vez de ksycoca para cargar motores de datos de C++
- Considerar a rotación visualParent en popupPosition (fallo 345787)

### Sonnet

- Non intentar realzar se non se atopa un corrector ortográfico. Isto provocaría un bucle infinito co temporizador de rehighlighRequest disparándose constantemente.

### Frameworkintegration

- Fix native file dialogs from widgets QFileDialog: ** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed. ** File dialogs opened with open() or show() with parent were not opened at all.

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
