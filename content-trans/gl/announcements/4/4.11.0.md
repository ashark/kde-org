---
aliases:
- ../4.11
custom_about: true
custom_contact: true
date: 2013-08-14
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.
title: Colección de software de KDE 4.11
---
{{<figure src="/announcements/4/4.11.0/screenshots/plasma-4.11.png" class="text-center" caption=`Versión 4.11 dos espazos de traballo de Plasma de KDE` >}} <br />

August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.<br />

Esta versión vai dedicada á memoria de <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul “toolz” Chitnis</a>, un gran campión do software libre e de código aberto da India. Atul liderou as conferencias Linux Bangalore e FOSS.IN desde 2001 e ambas foron eventos clave na escena FOSS da India. KDE India naceu no primeiro FOSS.in en decembro do 2005. Moitos colaboradores indios de KDE empezaron nestes eventos. Foi grazas ao ánimo de Atul que o día do proxecto KDE foi sempre un éxito en FOSS.IN. Atul deixounos o 3 de xuño tras unha longa batalla contra o cancro. Que descanse en paz. Estamos moi agradecidos pola súa contribución por un mundo mellor.

Estas versións están todas traducidas a 54 idiomas; esperamos que se engadas máis idiomas en versións de solución de erros secundarias mensuais sucesivas publicadas por KDE. O equipo de documentación actualizou 91 manuais de aplicacións para esta versión.

## <a href="./plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" /> Plasma Workspaces 4.11 Continues to Refine User Experience</a>

En preparación para mantemento a longo prazo, os espazos de traballo de Plasma inclúen melloras adicionais en funcionalidades básicas cunha barra de tarefas máis suave, un trebello de batería máis intelixente e un mesturador de son mellorado. A introdución de KScreen trae consigo xestión intelixente de varios monitores nos espazos de traballo, e melloras de rendemento a gran escala en combinación con pequenos axustes de facilidade de uso que conseguen unha mellor experiencia xeral.

## <a href="./applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Esta versión marca melloras masivas no software de xestión de información persoal de KDE, que mellora moito o rendemento e inclúe moitas novas funcionalidades. Kate mellora a produtividade de desenvolvedores de Python e JavaScript con novos complementos, Dolphin volveuse máis rápido e as aplicacións educativas traen consigo novas funcionalidades.

## <a href="./platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Esta versión 4.11 da plataforma de KDE continúa centrándose en estabilidade. Estanse realizando novas funcionalidades para a futura versión 5.0 das infraestruturas de KDE, pero para a versión estábel conseguimos introducir optimizacións para a nosa infraestrutura Nepomuk.

<br />
When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.<br />

## Corre a voz e descubre o que está a ocorrer: Etiqueta con &quot;KDE&quot;

KDE anima á xente a difundir información na Internet social. Envía historias a sitios de noticias, usa canles como delicious, digg, reddit, twitter e identi.ca. Envía capturas de pantallas a servizos como Facebook, Flickr, ipernity e Picasa, e publíqueas nos grupos axeitados. Cree vídeos e envíeos a YouTube, Blip.tv e Vimeo. Etiquete as publicacións e os materiais enviados con &quot;KDE&quot;. Isto facilita atopalos e permite ao equipo de promoción de KDE analizar a cobertura da versión 4.11 do software de KDE.

## Festas de publicación de versión

Como de costume, membros da comunidade KDE organizan festas de publicación de versión en todo o mundo. Moitas están xa planificadas pero iranse organizando máis. Consulte <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>a lista de festas</a>. Pode unirse calquera! Haberá unha combinación de xente interesante e conversas inspiradoras, así como comida e bebida. É unha gran oportunidade para aprender máis sobre o que ocorre en KDE, involucrarse, ou simplemente coñecer a outros usuarios e colaboradores.

Animamos a todo o mundo a organizar festas de seu. Ser anfitrión pode ser toda unha experiencia e calquera pode facelo! Bótelle unha ollada aos <a href='http://community.kde.org/Promo/Events/Release_Parties'>consellos para a organización de festas</a>.

## Sobre estes anuncios de publicación de versión

Estes anuncios de publicación de versións preparáronos Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin e outros membros do equipo de promoción de KDE e a comunidade enteira de KDE. Tratan os cambios principais feitos no software de KDE durante os últimos seis meses.

#### Apoiar KDE

<a href="http://jointhegame.kde.org/"> <img src="/announcements/4/4.9.0/images/join-the-game.png" class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V.'s new <a href='http://jointhegame.kde.org/'>Supporting Member program</a> is now open. For &euro;25 a quarter you can ensure the international community of KDE continues to grow making world class Free Software.
