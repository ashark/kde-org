---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE publica a versión 16.04.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.04.3 das aplicacións de KDE
version: 16.04.3
---
July 12, 2016. Today KDE released the third stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 20 correccións de erros inclúen melloras en, entre outros, Ark, Cantor, Kate, KDE PIM e Umbrelo.

This release also includes Long Term Support version of KDE Development Platform 4.14.22.
