---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE publica a versión 17.08.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 17.08.2 das aplicacións de KDE
version: 17.08.2
---
October 12, 2017. Today KDE released the second stability update for <a href='../17.08.0'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 25 correccións de erros inclúen melloras en, entre outros, Kontact, Dolphin, Gwenview, Kdenlive, Marble e Okular.

This release also includes Long Term Support version of KDE Development Platform 4.14.37.

Entre as melloras están:

- Corrixíronse unha fuga de memoria e unha quebra na configuración do complemento de eventos de Plasma
- As mensaxes lidas xa non se retirar inmediatamente do filtro de mensaxes sen ler de Akregator
- Agora o importador de Gwenview usa a data e hora de EXIF
