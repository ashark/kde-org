---
aliases:
- ../announce-applications-15.08-rc
date: 2015-08-06
description: KDE veröffentlicht den Freigabekandidaten der Anwendungen 15.08
layout: application
release: applications-15.07.90
title: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 15.08
---
06. August 2015. Heute veröffentlicht KDE den Freigabekandidaten der neuen Version der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

Bei den verschiedenen Anwendungen auf der Grundlage der KDE Frameworks 5 sind gründliche Tests für die Veröffentlichung der KDE Anwendungen 15.08 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Anwender, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim Team und installieren Sie die Version und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.
