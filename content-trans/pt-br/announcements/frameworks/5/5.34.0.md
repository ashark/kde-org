---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: Correcção da ordem da criação do objecto QCoreApplication (erro 378539)

### Ícones Breeze

- Adição de ícones para o 'hotspot' (https://github.com/KDAB/hotspot)
- Melhores ícones para sistemas de controlo de versões (erro 377380)
- Adição do ícone do 'plasmate' (erro 376780)
- Actualização dos ícones de sensibilidade do microfone (erro 377012)
- Elevar o tamanho por omissão dos ícones do 'Painel' para 48

### Módulos extra do CMake

- Limpezas: Não usar as opções do tipo GCC p.ex. no MSVC
- KDEPackageAppTemplates: melhorias na documentação
- KDECompilerSettings: Passagem das opções -Wvla &amp; -Wdate-time
- Suporte para versões mais antigas do 'qmlplugindump'
- Introdução do 'ecm_generate_qmltypes'
- Permitir aos projectos incluírem o ficheiro duas vezes
- Correcção de RX que corresponde aos nomes dos projectos fora do URI do Git
- Introdução do comando de compilação 'fetch-translations'
- Usar mais vezes o '-Wno-gnu-zero-variadic-macro-arguments'

### KActivities

- Só estão a ser usadas plataformas de Nível 1, pelo que irá passar ao Nível 2
- Remoção do KIO das dependências

### KAuth

- Correcção de segurança: verificar se quem nos chama é realmente quem diz

### KConfig

- Correcção do cálculo do 'relativePath' no KDesktopFile::locateLocal() (erro 345100)

### KConfigWidgets

- Definição do ícone para a acção Doar
- Alívio das restrições para processar os QGroupBoxes

### KDeclarative

- Não configurar o ItemHasContents na DropArea
- Não aceitar eventos à passagem na DragArea

### KDocTools

- Solução alternativa para o MSVC e o carregamento de catálogos
- Resolução de um conflito de visibilidade com o 'meinproc5' (erro 379142)
- Colocar entre aspas outras variáveis com localizações (para evitar problemas com espaços)
- Colocar entre aspas algumas variáveis com localizações (para evitar problemas com espaços)
- Desactivação temporária da documentação local no Windows
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - pesquisa nas instalações do Homebrew

### KFileMetaData

- tornar o KArchive opcional e não compilar extracções que necessitem dele
- correcção de erro de compilação com símbolos duplicados no Mingw em Windows

### KGlobalAccel

- compilação: Remoção de dependência do KService

### KI18n

- correcção do tratamento dos nomes de base dos ficheiros .po (erro 379116)
- Correcção da inicialização do ki18n

### KIconThemes

- Não tentar pintar ícones com tamanhos vazios

### KIO

- KDirSortFilterProxyModel: reposição da ordenação natural (erro 343452)
- Preenchimento do UDS_CREATION_TIME com o valor do 'st_birthtime' no FreeBSD
- 'slave' de HTTP: enviar a página de erro após uma falha na autorização (erro 373323)
- kioexec: delegação do envio para um módulo do 'kded' (erro 370532)
- Correcção da definição dupla do URL do esquema no Teste da GUI do KDirlister
- Eliminação dos módulos do 'kiod' ao sair
- Geração de um ficheiro 'moc_predefs.h' no KIOCore (erro 371721)
- kioexec: correcção do suporte para o '--suggestedfilename'

### KNewStuff

- Permitir várias categorias com o mesmo nome
- KNewStuff: Mostrar a informação do tamanho do ficheiro no método delegado da grelha
- Se o tamanho de um item for conhecido, mostrá-lo na área da lista
- Registo e declaração do KNSCore::EntryInternal::List como um meta-tipo
- Não seguir em frente pelo 'switch'. Elementos duplicados? Não, obrigado
- fechar sempre o ficheiro obtido após a transferência

### Plataforma KPackage

- Correcção do local de inclusão no KF5PackageMacros.cmake
- Ignorar os avisos durante a geração do 'appdata' (erro 378529)

### KRunner

- Modelo: Mudar a categoria do modelo de topo para "Plasma"

### KTextEditor

- Integração do KAuth na gravação do documento - vol. 2
- Correcção de erro ao aplicar a dobragem de código em casos de mudam a posição do cursor
- Uso do elemento não-obsoleto &lt;gui&gt; no ficheiro 'ui.rc'
- Permitir marcas de barra de posicionamento também na pesquisa&amp;substituição incorporada
- Integração do KAuth na gravação do documento

### KWayland

- Validação se a superfície é válida ao enviar o evento 'leave' no TextInput

### KWidgetsAddons

- KNewPasswordWidget: não esconder a acção de visibilidade no modo de texto simples (erro 378276)
- KPasswordDialog: não esconder a acção de visibilidade no modo de texto simples (erro 378276)
- Correcção do KActionSelectorPrivate::insertionIndex()

### KXMLGUI

- O 'kcm_useraccount' morreu, viva o 'user_manager'
- Compilações reprodutíveis: eliminar a versão do XMLGUI_COMPILING_OS
- Correcção: o nome do DOCTYPE deve corresponder ao tipo do elemento de topo
- Correcção de uso errado do ANY no kpartgui.dtd
- Uso do elemento de topo não-obsoleto &lt;gui&gt;
- Correcções de documentação da API: substituir o 0 por 'nullptr' ou remover se não se aplicar

### NetworkManagerQt

- Correcção de estoiro ao obter a lista de ligações activas (erro 373993)
- Definição do valor por omissão da auto-negociação, com base na versão do NM em uso

### Ícones do Oxygen

- Adição de ícone para o 'hotspot' (https://github.com/KDAB/hotspot)
- Elevar o tamanho por omissão dos ícones do 'Painel' para 48

### Plasma Framework

- recarregamento dos ícones quando o 'usesPlasmaTheme' muda
- Instalação dos Componentes do Plasma 3 para que possam ser usados
- Introdução do units.iconSizeHints.* para oferecer sugestões de tamanho dos ícones configuradas pelo utilizador (erro 378443)
- [TextFieldStyle] Correcção do erro de 'textField não definido'
- Actualização do truque do ungrabMouse para o Qt 5.8
- Guarda contra o Applet não carregar o AppletInterface (erro 377050)
- Calendário: Usar a língua correcta para os nomes dos meses e dias
- Gerar ficheiros 'plugins.qmltypes' para os 'plugins' que forem instalados
- se o utilizador definiu de facto um tamanho implícito, mantê-lo

### Solid

- Adição de inclusão necessária no 'msys2'

### Realce de sintaxe

- Adição da extensão do Arduino
- LaTeX: Correcção da finalização incorrecta dos comentários 'iffalse' (erro 378487)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
