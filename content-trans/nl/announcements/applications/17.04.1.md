---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE stelt KDE Applicaties 17.04.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.04.1 beschikbaar
version: 17.04.1
---
11 mei 2017. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../17.04.0'>KDE Applicaties 17.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, dolphin, gwenview, kate, kdenlive, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.32.
