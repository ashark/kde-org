---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE stelt KDE Applicaties 19.08.2 beschikbaar.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE stelt Applicaties 19.08.2 beschikbaar
version: 19.08.2
---
{{% i18n_date %}}

Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../19.08.0'>KDE Applicaties 19.08</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan twintig aangegeven reparaties van bugs, die verbeteringen leveren aan Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize, Spectacle, naast andere.

Verbeteringen bevatten:

- High-DPI ondersteuning is verbeterd in Konsole en andere toepassingen
- Schakelen tussen verschillende zoekopdrachten in Dolphin werkt nu de zoekparameters op de juiste manier bij
- KMail kan opnieuw berichten direct opslaan naar mappen op afstand
