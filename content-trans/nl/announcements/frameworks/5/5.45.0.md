---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Stel inhoudstype expliciet in op formuliergegevens

### Baloo

- Vereenvoudig term-operator &amp;&amp; en ||
- Document-ID niet ophalen voor overgeslagen resultaatitems
- Ophalen van mtime niet herhaaldelijk ophalen uit database bij sorteren
- Sanitizer van database niet standaard exporteren
- baloodb: experimenteel bericht toevoegen
- baloodb CLI-hulpmiddel introduceren
- Klasse sanitizer introduceren
- [FileIndexerConfig] bevolken van mappen vertragen tot echte gebruikt
- src/kioslaves/search/CMakeLists.txt - koppeling naar Qt5Network met volgen van wijzigingento kio
- balooctl: checkDb zou ook de laatst bekende url voor de documentId moeten verifiëren
- balooctl monitor: hervat om te wachten op service

### Breeze pictogrammen

- window-pin pictogram toevoegen (bug 385170)
- 64 px pictogram voor elisa hernoemen
- 32px pictogrammen voor afspeellijst shuffle en herhalen wijzigen
- Ontbrekende pictogrammen voor inline meldingen (bug 392391)
- Nieuw pictogram voor muziekspeler Elisa
- Pictogrammen voor mediastatus toevoegen
- Frame verwijderen rondom pictogrammen voor media-acties
- media-playlist-append en afspeelpictogrammen toevoegen
- view-media-album-cover voor babe toevoegen

### Extra CMake-modules

- Gebruikmaken van upstream CMake infrastructuur om de compiler toolchain te detecteren
- API dox: enige "code-block" regels om lege regels voor/na te hebben repareren
- ECMSetupQtPluginMacroNames toevoegen
- androiddeployqt leveren met alle prefix-paden
- De "stdcpp-path" in het json-bestand invoegen
- Symlinks in QML importpaden oplossen
- QML-importpaden leveren aan androiddeployqt

### Frameworkintegratie

- kpackage-install-handlers/kns/CMakeLists.txt - koppelen aan Qt::Xml wijzigingen volgend in knewstuff

### KActivitiesStats

- Neem niet aan dat SQLite werkt en beëindig niet bij fouten

### KDE Doxygen hulpmiddelen

- Zoek eerst naar qhelpgenerator-qt5 voor generatie van help

### KArchive

- karchive, kzip: probeer dubbele bestanden een beetje netter te behandelen
- nullptr gebruiken voor doorgeven van een null-pointer naar crc32

### KCMUtils

- Maak het mogelijk om een configuratiemodule voor een plug-in programmatisch aan te vragen
- X-KDE-ServiceTypes in plaats van ServiceTypes consistent gebruiken
- X-KDE-OnlyShowOnQtPlatforms aan KCModule definitie van servicetype toevoegen

### KCoreAddons

- KTextToHTML: terugkeren wanneer url leeg is
- m_inotify_wd_to_entry opschonen alvorens Entry pointers ongeldig te maken (bug 390214)

### KDeclarative

- QQmlEngine slechts een keer in QmlObject opzetten

### KDED

- X-KDE-OnlyShowOnQtPlatforms aan KDEDModule servicetypedefinitie toevoegen

### KDocTools

- Entities voor Elisa, Markdown, KParts, DOT, SVG aan general.entities toevoegen
- customization/ru: vertaling van underCCBYSA4.docbook en underFDL.docbook repareren
- Dubbele lgpl-notice/gpl-notice/fdl-notice repareren
- customization/ru: fdl-notice.docbook vertalen
- spelling van kwave wijzigen gevraagd door de onderhouder

### KFileMetaData

- taglibextractor: opnieuw maken voor betere leesbaarheid

### KGlobalAccel

- Niet toekennen indien onjuist uit dbus gebruikt (bug 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - vakantiebestand voor India bijwerken (bug 392503)
- Dit pakket moest niet bijgewerkt worden. Misschien een probleem met script
- Vakantiebestanden voor Duitsland bewerkt (bug 373686)
- README.md indelen zoals de hulpmiddelen verwachten (met een sectie Introductie)

### KHTML

- vragen om een leeg protocol vermijden

### KI18n

- Ga na dat ki18n zijn eigen vertalingen kan bouwen
- PythonInterp.cmake in KF5I18NMacros niet aanroepen
- Het mogelijk maken om po-bestanden parallel te genereren
- Een constructor voor KLocalizedStringPrivate aanmaken

### KIconThemes

- KIconEngine commentaar accuraat laten exporteren
- Een asan runtimefout vermijden

### KInit

- Hebben van tijdelijke autorisatie van IdleSlave verwijderen

### KIO

- Verzekeren dat het model is ingesteld wanneer resetResizing wordt aangeroepen
- pwd.h is niet aanwezig op windows
- Items Recent deze maand opgeslagen en Recent laatste maand opgeslagen standaard verwijderen
- KIO bouwen voor Android
- Installatie van bestand met kauth helper en policy-bestand tijdelijk uitschakelen
- Behandel vraag om bevstiging van beveiligde bewerking in SlaveBase in plaats van in KIO::Job
- Consistentie van "Openen met" verbeteren door altijd bovenste app inline te tonen
- Crash repareren wanneer apparaat lezen gereed uitstuurt nadat job is beëindigd
- Geselecteerde items accentueren bij tonen van bovenliggende map uit de dialoog openen/opslaan (bug 392330)
- Verborgen bestanden in NTFS ondersteunen
- X-KDE-ServiceTypes in plaats van ServiceTypes consistent gebruiken
- Toekenning repareren in concatPaths bij plakken van een volledig pad in regelbewerking van KFileWidget's
- [KPropertiesDialog] Controlesom tabblad ondersteunen voor elk lokaal pad (bug 392100)
- [KFilePlacesView] aanroep van KDiskFreeSpaceInfo onlleen indien noodzakelijk
- FileUndoManager: niet-bestaande lokale bestanden niet verwijderen
- [KProtocolInfoFactory] cache niet wissen als het net is gebouwd
- Probeer ook geen pictogram te zoeken voor een relatieve URL (bijv. '~')
- Juiste item-URL gebruiken voor Nieuw contextmenu aanmaken (bug 387387)
- Meer gevallen van onjuiste parameter naar findProtocol repareren
- KUrlCompletion: vroege terugkeer als de URL ongeldig is zoals ":/"
- Geen pictogram voor een lege url proberen te vinden

### Kirigami

- Grotere pictogrammen in mobiele modus
- Een inhoudsgrootte afdwingen in het item voor achtergrondstijl
- InlineMessage type en Gallery app voorbeeldpagina toevoegen
- betere heuristics selectieve kleuring
- laden van lokale svgs echt laten werken
- methode van laden van het android pictogram ook ondersteunen
- een kleuringsstrategie gelijkend op de voormalige verschillende stijlen gebruiken
- [Card] eigen "findIndex" implementatie gebruiken
- netwerkoverdrachten afbreken als we het pictogram wijzigen tijdens actief zijn
- eerste prototype voor hergebruik van een gedelegeerde
- Clients van OverlaySheet toestaan om de ingebouwde knop voor sluiten weg te laten
- Componenten voor Cards
- Actieknopgrootte repareren
- Laat passiveNotifications langer bestaan, zodat gebruikers ze echt kunnen lezen
- Ongebruikte QQC1 afhankelijkheid verwijderen
- ToolbarApplicationHeader-indeling
- Mogelijk maken om de titel te tonen ondanks het hebben van ctx-acties

### KNewStuff

- Echt stemmen bij klikken op sterren in de lijstweergave (bug 391112)

### KPackage-framework

- De FreeBSD build proberen te repareren
- Qt5::rcc gebruiken in plaats van zoeken naar het uitvoerbare programma
- NO_DEFAULT_PATH gebruiken om er zeker van te zijn dat het juiste commando wordt opgepakt
- Ook zoeken naar rcc uitvoerbare programma's met voorvoegsel
- component instellen voor juiste qrc generatie
- De binaire rcc-pakketgeneratie repareren
- Het rcc-bestand elke keer, bij installeren, genereren
- In org.kde. componenten een doneer-URL invoegen
- kpackage_install_package markeren als niet verouderd voor plasma_install_package

### KPeople

- PersonData::phoneNumber kenbaar maken aan QML

### Kross

- Niet nodig om kdoctools als vereist te hebben

### KService

- API dox: X-KDE-ServiceTypes consistent gebruiken in plaats van ServiceTypes

### KTextEditor

- Het mogelijk maken voor KTextEditor om te bouwen op Android NDK's gcc 4.9
- Asan runtime-fout vermijden: shift-exponent -1 is negatief
- optimaliseren van TextLineData::attribute
- attribute() niet tweemaal berekenen
- Reparatie terugdraaien: weergave springt bij schuiven verder dan het eind van het document is ingeschakeld (bug 391838)
- de klembordgeschiedenis niet vervuilen met duplicaten

### KWayland

- Interface voor toegang op afstand tot KWayland toevoegen
- [server] ondersteuning toevoegen voor de frame semantiek van Pointer versie 5 (bug 389189)

### KWidgetsAddons

- KColorButtonTest: tedoen-code verwijderen
- ktooltipwidget: marges aftrekken van beschikbare grootte
- [KAcceleratorManager] Alleen iconText() instellen indien echt gewijzigd (bug 391002)
- ktooltipwidget: weergeven buiten het scherm voorkomen
- KCapacityBar: QStyle::State_Horizontal status instellen
- Synchroniseren met KColorScheme wijzigingen
- ktooltipwidget: positionering van tekstballon repareren (bug 388583)

### KWindowSystem

- "SkipSwitcher" toevoegen aan API
- [xcb] implementatie van _NET_WM_FULLSCREEN_MONITORS repareren (bug 391960)
- Plasmashell bevroren tijd verminderen

### ModemManagerQt

- cmake: libnm-util niet aangeven als gevonden wanneer ModemManager is gevonden

### NetworkManagerQt

- De include-mappen van NetworkManager exporteren
- Start met vereisen van NM 1.0.0
- device: StateChangeReason en MeteredStatus als Q_ENUMs definiëren
- Conversie van AccessPoint vlaggen naar mogelijkheden repareren

### Plasma Framework

- Achtergrondsjablonen: achtergrondkleur instellen om contrast te verzekeren met inhoud voorbeeldtekst
- Sjabloon voor Plasma bureaubladachtergrond met QML extensie toevoegen
- [ToolTipArea] "aboutToShow" signaal toevoegen
- windowthumbnail: gamma gecorrigeerde schaling gebruiken
- windowthumbnail: mipmap textuur filtering gebruiken (bug 390457)
- Ongebruikte X-Plasma-RemoteLocation items verwijderen
- Slablonen: ongebruikte X-Plasma-DefaultSize uit applet metagegevens laten vallen
- X-KDE-ServiceTypes in plaats van ServiceTypes consistent gebruiken
- Slablonen: ongebruikte X-Plasma-Requires-* items uit applet metagegevens laten vallen
- ankers van item in een indeling verwijderen
- Plasmashell bevroren tijd verminderen
- alleen nadat het containment uiReadyChanged uitgeeft vooraf laden
- breken van keuzelijst repareren (bug 392026)
- Tekstschalen met niet-integer schaalfactoren wanneer PLASMA_USE_QT_SCALING=1 is ingesteld repareren (bug 356446)
- nieuwe pictogrammen voor verbroken/uitgeschakelde apparaten
- [Dialog] instellen van outputOnly voor NoBackground dialoog toestaan
- [ToolTip] bestandsnaam in KDirWatch behandelaar controleren
- Waarschuwing over verouderd uit kpackage_install_package voor nu uitschakelen
- [Breeze Plasma Theme] currentColorFix.sh toepassen bij gewijzigde mediapictogrammen
- [Breeze Plasma Theme] mediastatuspictogrammen met cirkels toevoegen
- Frames rond mediaknoppen verwijderen
- [Window Thumbnail] gebruik van atlas-textuur toestaan
- [Dialog] nu verouderde aanroepen van KWindowSystem::setState verwijderen
- Atlas texturen in FadingNode ondersteunen
- FadingMaterial fragment met kernprofiel repareren

### QQC2StyleBridge

- rendering repareren indien uitgeschakeld
- betere opmaak
- experimentele ondersteuning voor auto mnemonics
- Zorg er voor dat er rekening wordt gehouden met de grootte van het element bij het stijlen
- Lettertype-rendering voor niet-HiDPI en schaalfactoren met gehele getallen repareren (bug 391780)
- pictogramkleuren met kleursets repareren
- pictogramkleuren in hulpmiddelknoppen repareren

### Solid

- Solid kan nu zoeken naar batterijen in bijv. draadloze spelpads en joysticks
- Recent geïntroduceerde UP-enums gebruiken
- gaming_input apparaten en anderen aan Batterij toevoegen
- Batterij-apparaten Enum worden toegevoegd
- [UDevManager] ook expliciet zoeken naar camera's
- [UDevManager] Al filteren op subsysteem alvorens te zoeken (bug 391738)

### Sonnet

- De standaard client niet opleggen, kies er een die de gevraagde taal ondersteunt.
- Vervangende tekenreeksen in de lijst met suggesties meenemen
- NSSpellCheckerDict::addPersonal() implementeren
- NSSpellCheckerDict::suggest() geeft een lijst met suggesties terug
- NSSpellChecker taal initialiseren in NSSpellCheckerDict ctor
- NSSpellChecker log-categorie implementeren
- NSSpellChecker vereist AppKit
- NSSpellCheckerClient::reliability() uit de regel verplaatsen
- het Mac-platformtoken met voorkeur gebruiken
- Juiste map gebruiken om trigrams op te zoeken in windows bouwmap

### Accentuering van syntaxis

- Mogelijk maken om het project volledig te bouwen bij crosscompiling
- CMake syntaxisgenerator opnieuw ontworpen
- Optimaliseer accentuering Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Syntaxisaccentuering voor MIB-bestanden toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
