---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nieuwe module: ModemManagerQt (Qt-wrapper voor ModemManager API) Waardeer anders op naar Plasma-NM 5.3 Beta bij opwaardering naar ModemManagerQt 5.9.0.

### KActivities

- Geïmplementeerd met vergeten van een hulpbron
- Reparaties bij bouwen
- Een plug-in toegevoegd voor registratie van gebeurtenissen voor KRecentDocument meldingen

### KArchive

- KZip::extraField instelling respecteren, ook bij schrijven van centrale header-items
- Twee foutieve toekenningen verwijderd, die geburen wanneer de schijf vol is, bug 343214

### KBookmarks

- Bouwen met Qt 5.5 gerepareerd

### KCMUtils

- Nieuw op json gebaseerd plug-in systeem gebruiken. KCM's worden gezocht onder kcms/. Voor nu heeft een desktop-bestand nog steeds nodig om geïnstalleerd te worden voor compatibiliteit onder kservices5/
- De QML-only versie van kcms laden en inpakken, indien mogelijk

### KConfig

- Bij gebruik van KSharedConfig toekenning gerepareerd in een globale destructor.
- kconfig_compiler: ondersteuning voor CategoryLoggingName toevoegen in *.kcfgc bestanden, om qCDebug(category) aanroepen te genereren.

### KI18n

- de globale Qt catalog vooraf laden bij gebruikt van i18n()

### KIconThemes

- KIconDialog kan nu getoond worden met de reguliere methoden QDialog show() en exec()
- KIconEngine::paint gerepareerd om verschillende devicePixelRatios te behandelen

### KIO

- KPropertiesDialog inschakelen om informatie over vrije ruimte van bestandssystemen op afstand te tonen evenals (bijv. smb)
- KUrlNavigator repareren van pixmaps met hoge DPI
- Zorg dat KFileItemDelegate niet-standaard devicePixelRatio in animaties kan behandelen

### KItemModels

- KRecursiveFilterProxyModel: opnieuw bewerkt om de juiste signalen uit te sturen op het juiste moment
- KDescendantsProxyModel: behandel verplaatsingen garapporteerd door het bronmodel.
- KDescendantsProxyModel: gedrag repareren wanneer een selectie is gemaakt bij resetten.
- KDescendantsProxyModel: KSelectionProxyModel uit QML toestaan te worden gemaakt en gebruikt.

### KJobWidgets

- Geef foutcode door aan JobView DBus interface

### KNotifications

- Een versie van event() toegevoegd die geen pictogram neemt en een standaard pictogram zal gebruiken
- Een versie van event() toegevoegd die StandardEvent eventId en QString iconName aanneemt

### KPeople

- Sta uitbreiden van actiemetagegevens toe door voorgedefinieerde typen te gebruiken
- Repareer model dat niet juist is bijgewerkt nar verwijdering van een contact van een persoon

### KPty

- Laat aan de wereld zien of KPty is gebouwd met de bibliotheek utempter

### KTextEditor

- voeg het accentueringsbestand van kdesrc-buildrc toe
- syntaxis: ondersteuning toegevoegd voor constanten met gehele getallen in het accentueren in PHP-bestand

### KWidgetsAddons

- Laat KRatingWidget animatie geleidelijk werken met hoge Device Pixel Ratio

### KWindowSystem

- Een dummy Wayland implementatie toevoegen voor KWindowSystemPrivate
- KWindowSystem::icon met NETWinInfo niet gebonden aan platform X11.

### KXmlGui

- Vertaaldomein behouden bij samenvoegen van .rc-bestanden
- Waarschuwing tijdens uitvoeren van QWidget::setWindowModified repareren: De venstertitel bevat geen  '[*]' plaatshouder

### KXmlRpcClient

- Vertalingen installeren

### Plasma framework

- Verloren tekstballonnen gerepareerd wanneer tijdelijk de eigenaar van tekstballonnen verdwijnt of leeg werd
- TabBar die initieel niet juist was neergezet, wat gezien werd in bijv. Kickoff
- PageStack overgangen gebruiken nu Animators voor soepelere animaties
- TabGroup overgangen gebruiken nu Animators voor soepelere animaties
- Zorg dat Svg,FrameSvg werkt met QT_DEVICE_PIXELRATIO

### Solid

- ververs de batterijeigenschappen bij hervatten

### Wijzigingen aan het bouwsysteem

- Extra CMake Modules (ECM) zijn nu van een versie voorzien zoals KDE Frameworks, het is daarom nu 5.9, terwijl het eerder 1.8 was.
- Veel frameworks zijn gerepareerd om bruikbaar te zijn zonder zoeken naar hun privé afhankelijkheden. Dwz. toepassingen die een framework zoeken hebben alleen hun publieke afhankelijkheden nodig, niet de private.
- Sta instellen van SHARE_INSTALL_DIR toe, om multi-arch-indelingen beter af te handelen

### Frameworkintegratie

- Mogelijke crash repareren bij verwijderen van een QSystemTrayIcon (geactiveerd door bijv. Trojita), bug 343976
- Inheemse modale bestandsdialogen in QML repareren, bug 334963

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
