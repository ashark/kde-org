---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE stelt de eerste uitgave van Framework 5 beschikbaar.
layout: framework
qtversion: 5.2
title: Eerste uitgave van KDE Framework 5
---
7 juli 2014. De KDE gemeenschap is er trots op om KDE Frameworks 5.0 aan te kondigen. Frameworks 5 is de volgende generatie van KDE bibliotheken, in modules opgedeeld en geoptimaliseerd voor gemakkelijke integratie in Qt toepassingen. Frameworks biedt een brede variëteit van algemeen benodigde functionaliteit in volwassene, door anderen beoordeelde en goed geteste bibliotheken met vriendelijke bepalingen in licenties. Er zijn meer dan 50 verschillende Frameworks als onderdeel van deze uitgave die oplossingen bieden inclusief integratie met hardware, ondersteuning voor bestandsformaten, extra widgets, functies voor plotten, spellingcontrole en meer.  Veel Frameworks zijn op meerdere platforms te gebruiken en hebben minimale of geen extra afhankelijkheden waarmee ze gemakkelijk zijn te bouwen en toe te voegen aan elke Qt toepassing.

KDE Frameworks representeert een inspanning om de krachtige KDE Platform 4 bibliotheken om te werken in een set van onafhankelijke, cross-platform modules die goed beschikbaar zijn voor alle Qt ontwikkelaars om Qt ontwikkeling te vereenvoudigen, te versnellen en de kosten ervan te verminderen. De individuele Frameworks zijn cross-platform en goed gedocumenteerd en getest en hun gebruik zal bekend zijn bij Qt ontwikkelaars, de stijl en standaards volgend ingesteld door het Qt project. Frameworks zijn ontworpen onder het bewezen KDE bustuursmodel met een voorspelbaar uitgaveplan, een helder en leveranciersneutraal proces voor bijdragen, open bestuur en flexibele licenties (LGPL).

De Frameworks hebben een heldere afhankelijkheidsstructuur, opgedeeld in categorieën en lagen. De categorieën verwijzen naar afhankelijkheden tijdens uitvoeren:

- <strong>Functionele</strong> elementen hebben geen afhankelijkheden tijden uitvoeren.
- <strong>Integratie</strong> geeft code aan die afhankelijkheden voor integratie kan vereisen tijdens uitvoeren afhankelijk van wat het OS of platform biedt.
- <strong>Oplossingen</strong> hebben verplichte afhankelijkheden tijdens uitvoeren.

De <strong>Tiers (banden)</strong> verwijzen naar afhankelijkheden tijdens compileren op andere Frameworks. Tier 1 Frameworks hebben geen afhankelijkheden binnen Frameworks en hebben alleen Qt nodig en andere relevante bibliotheken. Tier 2 Frameworks kunnen alleen afhangen van Tier 1. Tier 3 Frameworks kunnen afhangen van andere Tier 3 Frameworks evenals van Tier 2 en Tier 1.

De transitie van Platform naar Frameworks is al meer dan 3 jaar bezig, geleid door top KDE technische medewerkers. Leer meer over Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>in dit artikel van vorig jaar</a>.

## Accentueringen

Er zijn meer dan 50 Frameworks op dit moment beschikbaar.  Blader door de complete set <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>in de online API documentatie</a>. Hier onder een impressie van enige van de functionaliteiten die Frameworks biedt aan ontwikkelaars van Qt toepassingen.

<strong>KArchive</strong> biedt ondersteuning voor veel populaire codecs voor compressie in een omvattend, vol functies en gemakkelijk te gebruiken bibliotheek voor archivering van bestanden en deze weer uitpakken. Stop er bestanden in; er is geen noodzaak om archivering in uw op Qt gebaseerde applicatie opnieuw uit te vinden!

<strong>ThreadWeaver</strong> biedt een API op hoog niveau om threads te beheren met op taken en wachtrij gebaseerde interfaces. Het biedt gemakkelijke planning van uitvoering van threads door afhankelijkheden te definiëren tussen de threads en ze uit te voeren met handhaving van afhankelijkheden, waarmee het gebruik van meerdere threads vergemakkelijkt wordt.

<strong>KConfig</strong> is een Framework om het opslaan en ophalen van instellingen in de configuratie. Het bevat een groep-georiënteerde API. Het werkt met INI-bestanden en XDG-compliant gestapeld mappen. Het genereert code gebaseerd op XML-bestanden.

<strong>Solid</strong> biedt detectie van hardware en kan een toepassing informeren over opslagapparaten en volumes, CPU, batterijstatus, energiebeheer, netwerkstatus en interfaces, en Bluetooth. Voor versleutelde partities, energie en het netwerk, zijn actieve daemons vereist.

<strong>KI18n</strong> voegt ondersteuning van Gettext aan toepassingen toe, waarmee het gemakkelijker is de werkmethode voor vertalen te integreren in de algemene infrastructuur voor vertalen van vele projecten.
