---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: KDE rilascia Applications 17.12 Beta.
layout: application
release: applications-17.11.80
title: KDE rilascia la beta di KDE Applications 17.12
---
17 novembre 2017. Oggi KDE ha rilasciato la beta della nuova versione delle KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Controlla le <a href='https://community.kde.org/Applications/17.12_Release_Notes'>note di rilascio della comunità</a> per informazioni sui nuovi archivi tar che ora sono basati su KF5 e sui problemi noti. Un annuncio più completo verrà reso disponibile in concomitanza con la versione finale

I rilasci di KDE Applications 17.12 hanno bisogno di una verifica accurata per mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare gli errori il più presto possibile affinché possano essere eliminati prima della versione finale. Valutate la possibilità di partecipare alla squadra installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando qualsiasi problema</a>.

#### Installazione dei pacchetti binari di KDE Applications 17.12 Beta

<em>Pacchetti</em>. Alcuni fornitori di sistemi Linux/UNIX hanno gentilmente messo a disposizione pacchetti binari di KDE Applications 17.12 Beta (internamente 17.11.80) per alcune versioni delle rispettive distribuzioni, e in altri casi hanno provveduto i volontari della comunità. Altri pacchetti binari, così come aggiornamenti ai pacchetti ora disponibili, potrebbero essere disponibili nelle prossime settimane.

<em>Posizioni dei pacchetti</em>. Per l'elenco aggiornato dei pacchetti binari disponibili di cui il progetto KDE è stato informato, visita il <a href='http://community.kde.org/Binary_Packages'>wiki Community</a>.

#### Compilazione di KDE Applications 17.12 Beta

Il codice sorgente completo per KDE Applications 17.12 Beta può essere <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>scaricato liberamente</a>. Le istruzioni per la compilazione e l'installazione sono disponibili dalla <a href='/info/applications/applications-17.11.80'>pagina di informazioni di KDE Applications 17.12 Beta</a>.
