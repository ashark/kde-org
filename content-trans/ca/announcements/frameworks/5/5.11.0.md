---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Mòduls extres del CMake

- Arguments nous per a «ecm_add_tests()» (error 345797)

### Integració del marc de treball

- Usar l'«initialDirectory» correcte per al «KDirSelectDialog»
- Assegura que s'especifica l'esquema quan se substitueix el valor URL inicial
- Acceptar només directoris existents en el mode FileMode::Directory

### KActivities

(No s'ha proporcionat cap registre de canvis)

### KAuth

- Fer disponible KAUTH_HELPER_INSTALL_ABSOLUTE_DIR a tots els usuaris del KAuth

### KCodecs

- KEmailAddress: Afegir una sobrecàrrega per extractEmailAddress i firstEmailAddress que retorni un missatge d'error.

### KCompletion

- Esmenar una selecció no desitjada en editar el nom de fitxer en el diàleg de fitxer (error 344525)

### KConfig

- Evitar una falla si QWindow::screen() és nul
- Afegir KConfigGui::setSessionConfig() (error 346768)

### KCoreAddons

- API d'utilitat KPluginLoader::findPluginById() nova

### KDeclarative

- Permet la creació de ConfigModule des del KPluginMetdata
- Esmenar els esdeveniments «pressAndhold»

### Compatibilitat amb les KDELibs 4

- Usar QTemporaryFile en lloc de codificar un fitxer temporal.

### KDocTools

- Actualització de traduccions
- Actualització de customization/ru
- Esmenar entitats amb enllaços incorrectes

### KEmoticons

- Memòria cau del tema en el connector d'integració

### KGlobalAccel

- [runtime] Moure el codi específic de plataforma als connectors

### KIconThemes

- Optimitzar KIconEngine::availableSizes()

### KIO

- No intentar completar els usuaris i fer una asserció quan la preposició no és buida (error 346920)
- Usar KPluginLoader::factory() en carregar KIO::DndPopupMenuPlugin
- Esmenar un interbloqueig en utilitzar servidors intermediaris de xarxa (error 346214)
- S'ha esmenat KIO::suggestName per mantenir les extensions dels fitxers
- Inicialitzar kbuildsycoca4 en actualitzar sycoca5.
- KFileWidget: No acceptar fitxers en el mode només directoris
- KIO::AccessManager: Fer possible tractar asíncronament un QIODevice seqüencial

### KNewStuff

- Afegir el mètode nou fillMenuFromGroupingNames
- KMoreTools: afegir molts agrupaments nous
- KMoreToolsMenuFactory: gestió per «git-clients-and-actions»
- createMenuFromGroupingNames: fer opcional el paràmetre URL

### KNotification

- Esmenar una fallada a NotifyByExecute quan no s'ha definit cap giny (error 348510)
- Millorar la gestió de les notificacions que es tanquen (error 342752)
- Reemplaçar l'ús del QDesktopWidget amb el QScreen
- Assegura que el KNotification es pot usar des d'un fil no IGU

### Paquets dels Frameworks

- Protegir l'accés a l'estructura «qpointer» (error 347231)

### KPeople

- Usar QTemporaryFile en lloc de codificar /tmp.

### KPty

- Usar «tcgetattr» i «tcsetattr» si són disponibles

### Kross

- Esmenar la càrrega dels mòduls Kross «forms» i «kdetranslation»

### KService

- En executar com a root, mantenir la propietat del fitxer en la memòria cau existent dels fitxers (error 342438)
- Protegir-se contra no poder obrir un flux (error 342438)
- Esmenar la comprovació per permisos no vàlids d'escriptura de fitxers (error 342438)
- Esmenar la consulta de ksycoca per pseudo tipus MIME x-scheme-handler/* (error 347353)

### KTextEditor

- Permetre, com en els temps del KDE 4.x, que aplicacions/connectors de terceres parts instal·lin els seus propis fitxers XML de ressaltat a katepart5/syntax
- Afegir KTextEditor::Document::searchText()
- Retornar l'ús de KEncodingFileDialog (error 343255)

### KTextWidgets

- Afegir un mètode per netejar el decorador
- Permetre l'ús d'un decorador personalitzat del «sonnet»
- Implementar «Cerca l'anterior» en el KTextEdit.
- Tornar a afegir la implementació per al text a veu

### KWidgetsAddons

- KAssistantDialog: Tornar a afegir el botó d'ajuda que es mostrava en la versió de les KDELibs4

### KXMLGUI

- Afegir la gestió de sessions a KMainWindow (error 346768)

### NetworkManagerQt

- Eliminar la implementació del WiMAX en el NM 1.2.0+

### Frameworks del Plasma

- Els components de calendari ara poden mostrar els números de les setmanes (error 338195)
- Usar «QtRendering» per als tipus de lletra en els camps de contrasenya
- Esmenar la cerca d'AssociatedApplicationManager quan un tipus MIME té àlies (error 340326)
- Esmenar el color de fons del plafó (error 347143)
- Desfer-se del missatge «No s'ha pogut carregar la miniaplicació»
- Capacitat per a carregar els KCM escrits en QML a les finestres de configuració dels plasmoides
- No usar la DataEngineStructure per les miniaplicacions
- Desfer-se de la «libplasma» en el sycoca, tant com sigui possible
- [plasmacomponents] Fer que SectionScroller segueixi el ListView.section.criteria
- Les barres de desplaçament ja no s'oculten automàticament quan es produeix un toc de pantalla (error 347254)

### Sonnet

- Usar una memòria cau central per als «SpellerPlugins».
- Reduir les assignacions temporals.
- Optimització: No esborrar la memòria cau del «dict» en copiar objectes del verificador ortogràfic.
- Optimitzar les crides save() invocant-les una vegada al final, si cal.

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
