---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Mòduls extres del CMake

- Millorar la informació dels errors de la macro «query_qmake»

### BluezQt

- Eliminar tots els dispositius de l'adaptador abans d'eliminar l'adaptador (error 349363)
- Actualitza els enllaços del README.md

### KActivities

- S'ha afegit l'opció de no seguir l'usuari en activitats específiques (similar al mode de «navegació privada» d'un navegador web)

### KArchive

- Conserva els permisos d'execució dels fitxers en el copyTo()
- Aclarir el ~KArchive eliminant codi obsolet.

### KAuth

- Fer possible usar el kauth-policy-gen des d'orígens diferents

### KBookmarks

- No afegir una adreça d'interès amb l'URL buit i el text buit
- Codificar l'URL del KBookmark per esmenar la compatibilitat amb les aplicacions del KDE 4

### KCodecs

- Eliminar el provador x-euc-tw

### KConfig

- Instal·lar el kconfig_compiler en la «libexec»
- Nova opció de generació de codi TranslationDomain=, per usar amb TranslationSystem=kde; normalment cal en biblioteques.
- Fer possible usar el kconfig_compiler des d'orígens diferents

### KCoreAddons

- KDirWatch: Establir una connexió al FAM només si es requereix
- Permetre el filtratge de connectors i aplicacions pel factor de forma
- Fer possible usar el «desktoptojson» des d'orígens diferents

### KDBusAddons

- Aclarir el valor de sortida per les instàncies «Unique»

### KDeclarative

- Afegir un clon QQC del KColorButton
- Assignar un QmlObject per cada instància del «kdeclarative» quan sigui possible
- Fer funcionar el Qt.quit() des del codi QML
- Fusionar la branca «mart/singleQmlEngineExperiment»
- Implementar el «sizeHint» basat en «implicitWidth/Height»
- Subclasse de QmlObject amb un motor estàtic

### Compatibilitat amb les KDELibs 4

- Esmenar la implementació de «KMimeType::Ptr::isNull».
- Reactivar la implementació de fluxos del KDateTime al kDebug/qDebug, per a més compatibilitat de codi font
- Carregar el catàleg de traducció correcte per al «kdebugdialog»
- No ometre la documentació de mètodes obsolets, per tal que es puguin llegir els consells d'adaptació

### KDESU

- Esmenar el CMakeLists.txt per passar la KDESU_USE_SUDO_DEFAULT a la compilació de manera que es pugui usar en el suprocess.cpp

### KDocTools

- Actualitzar les plantilles «docbook» del K5

### KGlobalAccel

- S'instal·la una API privada en temps d'execució per a permetre al KWin proporcionar un connector per al Wayland.
- Alternativa per a la resolució de noms del «componentFriendlyForAction»

### KIconThemes

- No intentar dibuixar la icona si la mida no és vàlida

### KItemModels

- Model nou de servidor intermediari: KRearrangeColumnsProxyModel. Permet reordenar i ocultar columnes des del model origen.

### KNotification

- Esmenar els tipus de mapes de píxels a «org.kde.StatusNotifierItem.xml»
- [ksni] Afegir un mètode per a recuperar una acció pel seu nom (error 349513)

### KPeople

- Implementar facilitats de filtratge del PersonsModel

### KPlotting

- KPlotWidget: Afegir «setAutoDeletePlotObjects», esmenar una fuita de memòria en el «replacePlotObject»
- Esmenar les marques d'escala que manquen si x0 &gt; 0.
- KPlotWidget: No cal «setMinimumSize» o redimensionar.

### KTextEditor

- debianchangelog.xml: Afegir Debian/Stretch, Debian/Buster, Ubuntu-Wily
- Esmenar el comportament de la substitució UTF-16 del parell retrocés/supressió.
- Permetre que «QScrollBar» gestioni els «WheelEvents» (error 340936)
- Aplicar un pedaç des del desenvolupament del KWrite per actualitzar el HL del Pure Basic, «Alexander Clay» &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Esmenar activació/desactivació el botó d'acord

### Framework del KWallet

- S'ha importat i millorat l'eina de línia d'ordres kwallet-query.
- Implementació per sobreescriure entrades de mapes.

### KXMLGUI

- No mostrar la «Versió dels Frameworks del KDE» en el diàleg Quant al KDE

### Frameworks del Plasma

- Fer el tema fosc completament fosc, també el grup complementari
- Memòria cau «naturalsize» separadament per factor d'escala
- ContainmentView: No fallar en una metadada de corona no vàlida
- AppletQuickItem: No accedir al KPluginInfo si no és vàlid
- Esmenar pàgines de configuració buides ocasionals de la miniaplicació (error 349250)
- Millorar la implementació del «hidpi» en el component de graella del calendari
- Verificar que el KService té un connector vàlid abans d'usar-lo
- [calendari] Assegura que la graella es torna a dibuixar en canviar el tema
- [calendari] Començar a comptar sempre les setmanes des de dilluns (error 349044)
- [calendari] Tornar a dibuixar la graella quan canvia la configuració dels números de setmana mostrats
- Ara s'usa un tema opac quan només és disponible l'efecte de difuminat (error 348154)
- Llista blanca de miniaplicacions/versions per un motor separat
- Presentar la classe ContainmentView nova

### Sonnet

- Permetre usar el ressaltat de sintaxi de verificació ortogràfica en un QPlainTextEdit

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
