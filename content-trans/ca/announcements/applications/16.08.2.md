---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE distribueix les aplicacions 16.08.2 del KDE
layout: application
title: KDE distribueix les aplicacions 16.08.2 del KDE
version: 16.08.2
---
13 d'octubre de 2016. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../16.08.0'>aplicacions 16.08 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 30 esmenes registrades d'errors que inclouen millores al Kdepim, Ark, Dolphin, Kgpg, Kolourpaint i Okular entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.25.
