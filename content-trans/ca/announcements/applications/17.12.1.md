---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE distribueix les aplicacions 17.12.1 del KDE
layout: application
title: KDE distribueix les aplicacions 17.12.1 del KDE
version: 17.12.1
---
11 de gener de 2018. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../17.12.0'>aplicacions 17.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores al Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta, i Umbrello entre d'altres.

Les millores inclouen:

- S'ha solucionat l'enviament de correus al Kontact per a determinats servidors SMTP
- S'ha millorat la línia de temps del Gwenview i les cerques d'etiquetes
- S'ha solucionat la importació JAVA a l'eina de diagrames UML Umbrello
