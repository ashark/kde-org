---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: Es distribueixen les aplicacions 14.12.2 del KDE.
layout: application
title: KDE distribueix les aplicacions 14.12.2 del KDE
version: 14.12.2
---
3 de febrer de 2015. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../14.12.0'>aplicacions 14.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 20 esmenes registrades d'errors que inclouen millores al joc d'anagrames Kanagram, el modelador UML Umbrello, el visualitzador de documents Okular i el globus virtual Marble.

Aquest llançament també inclou les versions de suport a llarg termini dels espais de treball Plasma 4.11.16, la plataforma de desenvolupament KDE 4.14.5 i el paquet Kontact 4.14.5.
