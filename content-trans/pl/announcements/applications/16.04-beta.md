---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE wydało Aplikacje KDE 16.04 Beta.
layout: application
release: applications-16.03.80
title: KDE wydało betę Aplikacji KDE 16.04
---
24 marzec 2016. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Sprawdź <a href='https://community.kde.org/Applications/16.04_Release_Notes'>listę zmian społeczności</a>, aby uzyskać informację dotyczącą archiwów, obecnie opartych na KF5 oraz ich znanych błędów. Bardziej kompletny komunikat będzie dostępny po wydaniu wersji ostatecznej

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 16.04 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu 4.12 poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 16.04 Beta 

<em>Pakiety</em>. Niektórzy wydawcy systemów operacyjnych Linux/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 16.04 Beta (wewnętrznie 16.03.80) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 16.04 Beta

Pełny kod źródłowy dla Aplikacji KDE 16.04 Beta można <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-16.03.80.php'>Stronie informacyjnej Aplikacji KDE Beta</a>.
