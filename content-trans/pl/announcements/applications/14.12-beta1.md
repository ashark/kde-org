---
aliases:
- ../announce-applications-14.12-beta1
custom_spread_install: true
date: '2014-11-06'
description: KDE wydało Aplikacje KDE 14.12 Beta 1.
layout: application
title: KDE wydało pierwszą wersję beta Aplikacji KDE 14.12
---
6 listopad 2014. Dzisiaj KDE wydało betę nowej wersji Aplikacji KDE. Wersja ta zamraża wszelkie zmiany w zależnościach i funkcjonalności, a zespół KDE będzie się skupiał jedynie na naprawianiu w niej błędów i dalszym polerowaniu.

Ze względu na obecność wielu programów opartych na Szkieletach KDE 5, wydanie Aplikacji 14.12 wymaga dokładnego przetestowania w celu utrzymania, a nawet poprawienia jakości wrażeń użytkownika. Obecnie użytkownicy są znaczącym czynnikiem przy utrzymywaniu wysokiej jakości KDE, bo programiści po prostu nie mogą wypróbować każdej możliwej konfiguracji. Liczymy, że wcześnie znajdziesz błędy, tak aby mogły zostać poprawione przed wydaniem końcowym. Proszę rozważyć dołączenie do zespołu 4.12 poprzez zainstalowanie bety i <a href='https://bugs.kde.org/'>zgłaszanie wszystkich błędów</a>.

#### Instalowanie pakietów binarnych Aplikacji KDE 14.12 Beta1

<em>Pakiety</em>. Niektórzy wydawcy systemów operacyjnych Linux/UNIX  uprzejmie dostarczyli pakiety binarne Aplikacji KDE 14.12 Beta1  (wewnętrznie 4.11.80) dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. Dodatkowe pakiety binarne, tak samo jak uaktualnienia do pakietów już dostępnych, mogą stać się dostępne w przeciągu nadchodzących tygodni.

<em>Położenie Pakietów</em>. Po bieżącą listę dostępnych pakietów  binarnych, o których został poinformowany Projekt KDE, zajrzyj na  stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages'> Społeczności Wiki</a>.

#### Kompilowanie Aplikacji KDE 14.12 Beta1

Pełny kod źródłowy dla Aplikacji KDE 14.12 Beta1 można <a href='http://download.kde.org/unstable/applications/14.11.80/src/'>pobrać bez opłaty</a>.Instrukcje na temat kompilowania i instalowania są dostępne na <a href='/info/applications/applications-14.11.80.php'>Stronie informacyjnej Aplikacji KDE Beta 1</a>.
