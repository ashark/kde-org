---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE toob välja KDE rakendused 17.04.1
layout: application
title: KDE toob välja KDE rakendused 17.04.1
version: 17.04.1
---
May 11, 2017. Today KDE released the first stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Dolphini, Gwenview, Kate, Kdenlive'i ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.32.
