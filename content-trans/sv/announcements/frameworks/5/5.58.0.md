---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Vänta på att extraheringsprocessen startar
- [balooctl] Lägg till kommando för att visa filer som misslyckades indexeras (fel 406116)
- Lägg till QML i källkodstyper
- [balooctl] Fånga den konstanta totala storleken i lambda
- [balooctl] Flytta flerraders utmatning till ny hjälpfunktion
- [balooctl] Använd ny hjälpfunktion i json utmatning
- [balooctl] Använd ny hjälpfunktion för utmatning av enkelt format
- [balooctl] Ta bort filindex statussamling från utmatning
- Håll tomma Json metadatadokument utanför DocumentData databasen
- [balooshow] Tillåt referens till filer med webbadress från hård länk
- [balooshow] Undertryck varning när webbadress refererar till oindexerad fil
- [MTimeDB] Tillåt tidsstämpel nyare än det nyaste dokumentet i intervallmatchning
- [MTimeDB] Använd exakt träff när exakt träff efterfrågas
- [balooctl] Städa hantering av olika positionsargument
- [balooctl] Utöka hjälptexten för väljare, förbättra felkontroll
- [balooctl] Använd mer förståeliga namn för storlek i statusutmatning
- [balooctl] rensningskommando: Ta bort falsk kontroll för documentData, städning
- [kio_search] Rätta varning, lägg till UDSEntry för "." i listDir
- Använd hexadecimal notation för DocumentOperation flaggans uppräkningsvärde
- Beräkna total DB-storlek korrekt
- Fördröj termtolkning till det behövs, ange inte både term och söksträng
- Lägg inte till datumfilter med förvalda värden i json
- Använd kompakt Json-formatering när frågewebbadresser konverteras
- [balooshow] Skriv inte ut en falsk varning för icke-indexerad fil

### Breeze-ikoner

- Lägg till icke-symboliska 16 bildpunkters find-location och mark-location
- Lägg till symbolisk länk från preferences-system-windows-effect-flipswitch till preferences-system-tabbox
- Lägg till symbolisk länk för "edit-delete-remove" ikon och lägg till 22 bildpunkters version för "paint-none" och "edit-none"
- Använd en konsekvent förvald användarikon för Kickoff
- Lägg till en ikon för Thunderbolt inställningsmodul
- Gör Z:an i system-suspend* ikoner skarpare
- Förbättra "widget-alternatives" ikoner
- Lägg till go-up/down/next/previous-skip
- Uppdatera KDE-logotyp för att bli närmare originalet
- Lägg till alternatives ikon

### Extra CMake-moduler

- Felrättning: sök c++ stl genom att använda reguljära uttryck
- Aktivera -DQT_STRICT_ITERATORS utan villkor, bara inte i avlusningsläge

### KArchive

- KTar: Skydda mot negativa longlink-storlekar
- Rätta ogiltig minnesskrivning vid felformade tar-filer
- Rätta minnesläcka vid läsning av vissa tar-filer
- Rätta oinitierat minne använt vid läsning av felformade tar-filer
- Rätta överflöde av stackbuffer vid läsning av felformade filer
- Rätta avreferering av null för felformade tar-filer
- Installera krcc.h deklarationsfil
- Rätta dubbel borttagning för felaktiga filer
- Tillåt inte kopia av KArchiveDirectoryPrivate och KArchivePrivate
- Rätta KArchive::findOrCreate som får slut på stack vid MYCKET LÅNGA sökvägar
- Introducera och använd KArchiveDirectory::addEntryV2
- removeEntry kan misslyckas så det är bra att veta om det hände
- KZip: rätta Heap-use-after-free i felaktiga filer

### KAuth

- Tvinga KAuth-hjälpfunktioner att ha UTF-8 stöd (fel 384294)

### KBookmarks

- Lägg till stöd så att KBookmarkOwner kan kommunicera om den har öppna flikar

### KCMUtils

- Använd storlekstips från själva ApplicationItem
- Rätta Oxygen bakgrundstoning för QML-moduler

### KConfig

- Lägg till underrättelsekapacitet i KConfigXT

### KCoreAddons

- Rätta fel varningar om "Unable to find service type"
- Ny klass KOSRelease - en tolk för utgivningsfiler för operativsystem

### KDeclarative

- [KeySequenceItem] Gör så att rensningsknappen har samma höjd som genvägsknappen
- Plotter: Begränsa GL Program till livstiden för scengrafikens nod (fel 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: Visa inte engelsk text för användaren
- [ConfigModule] Skicka initiala egenskaper i push()
- Aktivera stöd för glGetGraphicsResetStatus som förval för Qt &gt;= 5.13 (fel 364766)

### KDED

- Installera .desktop-fil för kded5 (fel 387556)

### KFileMetaData

- [TagLibExtractor] Rätta krasch för ogiltiga Speex filer (fel 403902)
- Rätta krasch i exivextractor med felformade filer (fel 405210)
- Deklarera egenskaper som metatyp
- Ändra egenskapernas attribut för konsekvens
- Hantera variantlistor i formateringsfunktioner
- Rätta för Windows' LARGE_INTEGER typ
- Rätta (kompilerings) fel för Windows UserMetaData implementering
- Lägg till saknade Mime-typer i taglib writer
- [UserMetaData] Hantera ändringar i egenskapers datastorlek riktig
- [UserMetaData] Red ut Windows, Linux/BSD/Mac och stubbkod

### KGlobalAccel

- Kopiera behåller i Component::cleanUp innan iteration
- Använd inte qAsConst över en tillfällig variabel (fel 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb - lägg till helger för Zambia
- holidays/plan2/holiday_lv_lv - rätta midsommardagen
- holiday_mu_en - Helger 2019 i Mauritius
- holiday_th_en-gb - uppdatera för 2019 (fel 402277)
- Uppdatera japanska helger
- Lägg till allmänna helger för Niedersachsen (Tyskland)

### KImageFormats

- tga: försök inte läsa mer än max_palette_size till palette
- tga: memset dst om läsning misslyckas
- tga: memset hela palettfältet, inte bara palette_size
- Initiera olästa bitar i _starttab
- xcf: Rätta användning av oinitierat minne för felaktiga dokument
- ras: Läs inte för mycket indata vid felformade filer
- xcf: layer är const i kopiering och sammanfogning, markera den som sådan

### KIO

- [FileWidget] Ersätt "Filter:" med "File type:" vid spara med en begränsad lista av Mime-typer (fel 79903)
- Nyskapade 'Länk till program' filer har en generisk ikon
- [Egenskapsdialogruta] Använd strängen "Free space" istället för "Disk usage" (fel 406630)
- Fyll UDSEntry::UDS_CREATION_TIME på linux när glibc &gt;= 2.28
- [KUrlNavigator] Rätta navigering med webbadress när arkiv lämnas med krarc och Dolphin (fel 386448)
- [KDynamicJobTracker] När kuiserver inte är tillgänglig, återgå också till grafisk komponentdialogruta (fel 379887)

### Kirigami

- [aboutpage] dölj rubriken Authors om det inte finns några upphovsmän
- Uppdatera qrc.in för att matcha .qrc (saknar ActionMenuItem)
- Säkerställ att vi inte trycker ut ActionButton (fel 406678)
- Sidor: exportera korrekt storlek på contentHeight/implicit
- [ColumnView] Kontrollera också index i underliggande filter..
- [ColumnView] Låt inte musens bakåtknapp gå bakåt förbi första sidan
- huvud har omedelbart riktig storlek

### KJobWidgets

- [KUiServerJobTracker] Spåra livslängden för tjänsten kuiserver och registrera om jobb om nödvändigt

### KNewStuff

- Ta bort taggig kant (fel 391108)

### KNotification

- [Underrättelse av Portal] Stöd standardåtgärder och prioritetstips
- [KNotification] Lägg till HighUrgency
- [KNotifications] Uppdatering när flaggor, webbadresser, eller vikt ändras
- Tillåt att ställa in vikt för underrättelser

### Ramverket KPackage

- Lägg till saknade egenskaper i kpackage-generic.desktop
- kpackagetool: läs kpackage-generic.desktop från qrc
- AppStream-generering: säkerställ att vi också slår upp paketstrukturen för paket som har metadata.desktop/json

### KTextEditor

- Granska kate config sidor för att förbättra underhållsvänlighet
- Tillåt att läget ändras, efter att färgläggningen har ändrats
- ViewConfig: Använd nytt generellt inställningsgränssnitt
- Rätta att bokmärkens bildpunktsavbildning ritar på ikonraden
- Säkerställ att vänster kant inte missar någon ändring av antalet radnummersiffror
- Rätta för att visa vikningsförhandsgranskning när musen flyttas från längs ner till längst upp
- Granska IconBorder
- Lägg till inmatningsmetoder i statusradknappen för inmatningsmetod
- Måla vikningsmarkören med riktig färg och gör den synligare
- ta bort förvald genväg F6 för att visa ikonkant
- Lägg till åtgärd för att ändra vikning av underliggande intervall (fel 344414)
- Byt namn på knappen "Close" till "Close file" när en fil har tagits bort från disk (fel 406305)
- upp copyright, kanske det också ska vara en define
- undvik genvägar med konflikt för att byta flikar
- KateIconBorder: Rätta vikning av bredd och höjd på meddelanderuta
- undvik att vyn hoppar längst ner vid vikningsändringar
- DocumentPrivate: Respektera indenteringsläge vid blockmarkering (fel 395430)
- ViewInternal: Rätta makeVisible(..) (fel 306745)
- DocumentPrivate: Gör parenteshantering smart (fel 368580)
- ViewInternal: Granska släpphändelse
- Tillåt att stänga ett dokument vars fil har tagits bort på disk
- KateIconBorder: Använd UTF-8 tecken istället för en speciell bildpunktsavbildning som dynamisk radbrytningsindikator
- KateIconBorder: Säkerställ att dynamisk radbrytningsmarkör visas
- KateIconBorder: Kodkosmetik
- DocumentPrivate: Stöd automatiska parenteser i blockmarkeringsläge (fel 382213)

### KUnitConversion

- Rätta konvertering av l/100 km till MPG (fel 378967)

### Ramverket KWallet

- Ställ in riktig kwalletd_bin_path
- Exportera sökväg för kwalletd binärprogram för kwallet_pam

### Kwayland

- Lägg till CriticalNotification fönstertyp i protokollet PlasmaShellSurface
- Implementera wl_eglstream_controller servergränssnitt

### KWidgetsAddons

- Uppdatera kcharselect-data till Unicode 12.1
- KCharSelect's intern modell: säkerställ att rowCount() är 0 för giltiga index

### KWindowSystem

- Introducera CriticalNotificationType
- Stöd NET_WM_STATE_FOCUSED
- Dokumentera att modToStringUser och stringUserToMod bara hanterar engelska strängar

### KXMLGUI

- KKeySequenceWidget: Visa inte engelska strängar för användaren

### NetworkManagerQt

- WireGuard: Kräv inte att 'private-key' ej ska vara tom för 'private-key-flags'
- WireGuard: provisorisk lösning för felaktig hemlig flaggtyp
- WireGuard: private-key och preshared-keys kan begäras tillsamman

### Plasma ramverk

- PlatformComponentsPlugin: rätta iid för insticksprogram till QQmlExtensionInterface
- IconItem: ta bort återstående och oanvända jämna egenskapsbitar
- [Dialogruta] Lägg till typen CriticalNotification
- Rätta fel gruppnamn för 22, 32 bildpunkter i audio.svg
- gör så att mobil textverktygsrad bara visas vid tryck
- använd den nya Kirigami.WheelHandler
- Lägg till fler ikonstorlekar för audio, configure, distribute
- [FrameSvgItem] Uppdatera filtrering för jämna ändringar
- Air/Oxygen-skrivbordstema: rätta förloppsradens höjd genom att använda "hint-bar-size"
- Rätta stilmallsstöd för audio-volume-medium
- Uppdatera audio, drive, edit, go, list, media, plasmavault ikoner för att matcha breeze-icons
- Justera z:an till bildpunktsrutnät i system.svg
- använd mobiletextcursor från rätt namnrymd
- [FrameSvgItem] Respektera egenskapen smooth
- Oxygen-skrivbordstema: lägg till vaddering för visare, mot ojämn kontur vid rotation
- SvgItem, IconItem: använd inte "smooth" egenskapsöverskridande, uppdatera nod vid ändring
- Stöd för att använda gzip för svgz på windows, med 7z
- Air/Oxygen-skrivbordstema: rätta visarposition med hint-*-rotation-center-offset
- Lägg öppet programmeringsgränssnitt för att skicka contextualActionsAboutToShow som går att anropa
- Breeze-skrivbordstema för klocka: stöd positionstips av visarens skugga i Plasma 5.16
- Behåll SVG-skrivbordstemafiler uppackade i arkivet, installera svgz
- separat mobil textmarkering för att undvika rekursiva importer
- Använd lämpligare ikon och text för "Alternatives"
- FrameSvgItem: lägg till egenskapen "mask"

### Prison

- Aztec: Rätta vaddering om det sista partiella kodordet är bara bitar med värdet ett

### QQC2StyleBridge

- Undvik nästlade Controls i TextField (fel 406851)
- gör så att mobil textverktygsrad bara visas vid tryck
- [TabBar] Uppdatera höjd när TabButtons läggs till dynamisk
- använd den nya Kirigami.WheelHandler
- Stöd egen ikonstorlek för ToolButton
- Det kompilerar bra utan foreach

### Solid

- [Fstab] Lägg till stöd för icke-nätverksfilsystem
- [FsTab] Lägg till cache för systemtypen enhetsfil
- [Fstab] Förberedande arbete för att aktivera filsystem bortom NFS/SMB
- Rätta ingen medlem vid namn 'setTime_t' i 'QDateTime' fel vid byggning (fel 405554)

### Syntaxfärgläggning

- Lägg till syntaxfärgläggning för skalet fish
- AppArmor: försök inte färglägga variabeltilldelningar och aliasregler inom profiler

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
