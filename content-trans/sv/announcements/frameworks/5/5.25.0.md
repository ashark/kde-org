---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Allmänt

- Qt ≥ 5.5 krävs nu

### Attica

- Följ HTTP-omdirigeringar

### Breeze-ikoner

- Uppdatera 16 bildpunkters e-postikoner för att bättre känna igen dem
- Uppdatera ikoner för mikrofon och ljudstatus så de har samma layout och storlek
- Ny programikon för systeminställningar
- Lägg till Gnome symboliska statusikoner
- Lägg till stöd för Gnome 3 symboliska ikoner
- Lägg till ikoner för Diaspora och Vector, se phabricator.kde.org/M59
- Nya ikoner för Dolphin och Gwenview
- Väderikoner är statusikoner, inte programikoner
- Lägg till några länkar till xliff, tack till gnastyle
- Lägg till ikon för kig
- Lägg till Mime-typ ikoner, ikon för krdc, och andra programikoner från gnastyle
- Lägg till Mime-typ ikon för certifikat (fel 365094)
- Uppdatera GIMP-ikoner, tack till gnastyle (fel 354370)
- Ikonen för globåtgärd är nu inte någon länkad fil, använd den i Digikam
- Uppdatera ikoner för labplot enligt e-post från Alexander Semke 13/7
- Lägg till programikoner från gnastyle
- Lägg till ikon för kruler från Yuri Fabirovsky
- Rätta felaktiga SVG-filer, tack till fuchs (fel 365035)

### Extra CMake-moduler

- Rätta inkludering när Qt5 inte finns
- Lägg till en reservmetod för query_qmake() när det inte finns någon installation av Qt5
- Säkerställ att ECMGeneratePriFile.cmake beter sig som resten av ECM
- Appstream-data har ändrat föredragen plats

### KActivities

- [KActivities-CLI] Kommandon för att starta och stoppa en aktivitet
- [KActivities-CLI] Ställa in och hämta aktivitetsnamn, -ikon och -beskrivning
- Tillägg av ett kommandoradsprogram för att kontrollera aktiviteter
- Tillägg av skript för att byta till föregående och nästa aktivitet
- Metod för att infoga i QFlatSet returnerar nu ett index tillsammans med iteratorn (fel 365610)
- Lägg till ZSH-funktioner för att stoppa och ta bort icke-aktuella aktiviteter
- Lägg till egenskapen isCurrent i KActivities::info
- Använd konstanta iteratorer vid sök efter aktivitet

### KDE Doxygen-verktyg

- Många förbättringar av utdataformatering
- Mainpage.dox har nu högre prioritet än README.md

### KArchive

- Hantera flera gzip-strömmar (fel 232843)
- Antag att en katalog är en katalog, även om rättighetsbiten är felinställd (fel 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: Rätta returvärde när objekt redan är på rätt ställe

### KConfig

- Lägg till standardgenvägar för DeleteFile och RenameFile

### KConfigWidgets

- Lägg till standardåtgärder DeleteFile och RenameFile
- Inställningssidan har nu rullningslister när det behövs (fel 362234)

### KCoreAddons

- Installera kända licenser och hitta dem vid körning, regressionsrättning (fel 353939)

### KDeclarative

- Skicka verkligen valueChanged

### KFileMetaData

- Kontrollera xattr vid konfigurationssteget, annars kan bygget misslyckas (om xattr.h saknas)

### KGlobalAccel

- Använd klauncher dbus istället för KRun (fel 366415)
- Starta hopplisteåtgärder via KGlobalAccel
- KGlobalAccel: Rätta låsning vid avslutning i Windows

### KHTML

- Stöd procentenhet för kantradie
- Ta bort version av egenskaper med prefix för bakgrunds- och kantradie
- Rättningar av 4-värdes snabbversioner av konstruktorfunktioner
- Skapa bara strängobjekt om de kommer att användas

### KIconThemes

- Förbättra prestanda av makeCacheKey avsevärt, eftersom det är en kritisk kodväg vid ikonuppslagning
- KIconLoader: Reducera antal uppslagningar när reserver används
- KIconLoader: Massiv hastighetsförbättring vid laddning av otillgängliga ikoner
- Töm inte sökrad vid byte av kategori
- KIconEngine: Rätta att QIcon::hasThemeIcon alltid returnerar sant (fel 365130)

### KInit

- Anpassa KInit till Mac OS X

### KIO

- Rätta KIO::linkAs() så att det fungerar som utlovat, dvs. misslyckas om dest redan finns
- Rätta KIO::put("file:///sökväg") så att umask respekteras (fel 359581)
- Rätta KIO::pasteActionText för null dest objekt och för tom webbadress
- Lägg till stöd för att ångra skapa symboliska länk
- Alternativ i grafiskt användargränssnitt för att anpassa global MarkPartial för I/O-slavar
- Rätta MaxCacheSize begränsad till 99 KiB
- Lägg till klippbordsknappar under checksummefliken
- KNewFileMenu: Rätta kopiering av mallfil från inbäddad resurs (fel 359581)
- KNewFileMenu: Rätta skapa länk till program (fel 363673)
- KNewFileMenu: Rätta förslag på nytt filnamn när filen redan finns på skrivbordet
- KNewFileMenu: Säkerställ att fileCreated() också skickas för programfiler på skrivbordet
- KNewFileMenu: Rätta att skapa symboliska länkar med ett relativt mål
- KPropertiesDialog: Förenkla användning av knappruta, rätta beteende vid Esc
- KProtocolInfo: Fyll cachen igen för att hitta nyinstallerade protokoll
- KIO::CopyJob: konvertera till qCDebug (med en egen area, eftersom det kan vara ganska mångordigt)
- KPropertiesDialog: Lägg till checksummeflik
- Töm webbadressens sökväg innan KUrlNavigator initieras

### KItemModels

- KRearrangeColumnsProxyModel: Rätta assert i index(0, 0) vid tom modell
- Rätta att KDescendantsProxyModel::setSourceModel() inte tömmer intern cache
- KRecursiveFilterProxyModel: Rätta förstörd QSFPM på grund av bortfiltrering av signalen rowsRemoved (fel 349789)
- KExtraColumnsProxyModel: Implementera hasChildren()

### KNotification

- Ställ inte in överliggande objekt för dellayout manuellt, tystar varning

### Paketet Framework

- Härled ParentApp från insticksmodulen PackageStructure
- Låt kpackagetool5 skapa appstream-information för kpackage-komponenter
- Gör det möjligt att läsa in filen metadata.json från kpackagetool5

### Kross

- Ta bort oanvända KF5-beroenden

### KService

- applications.menu: Ta bort referenser till oanvända kategorier
- Uppdatera alltid Trader-tolken från yacc/lex-källkod

### KTextEditor

- Fråga inte om överskrivning av en fil två gånger med inbyggda dialogrutor
- Lägg till FASTQ-syntax

### Kwayland

- [klient] Använd en QPointer för enteredSurface i Pointer
- Exponera Geometry i PlasmaWindowModel
- Lägg till geometrihändelse i PlasmaWindow
- [src/server] Verifiera att ytan har en resurs innan "pointer enter" skickas
- Lägg till stöd för xdg-skal
- [server] Skicka en "selection clear" innan tangentbordets "focus enter"
- [server] Hantera situation utan XDG_RUNTIME_DIR elegantare

### KWidgetsAddons

- [KCharSelect] Rätta krasch vid sökning utan förinställd datafil (fel 300521)
- [KCharSelect] Hantera tecken utanför BMP (fel 142625)
- [KCharSelect] Uppdatera kcharselect-data till Unicode 9.0.0 (fel 336360)
- KCollapsibleGroupBox: Stoppa animering i destruktor om den fortfarande pågår
- Uppdatera Breeze-palett (synkronisera från KColorScheme)

### KWindowSystem

- [xcb] Säkerställ att signalen compositingChanged skickas om NETEventFilter skapas om (fel 362531)
- Lägg till ett bekvämlighetsprogrammeringsgränssnitt för  att fråga om fönstersystem eller plattform som används av Qt

### KXMLGUI

- Rätta minimalt storlekstips, avkortad text (fel 312667)
- [KToggleToolBarAction] Lyd begränsning för action/options_show_toolbar

### NetworkManagerQt

- Använd WPA2-PSK och WPA2-EAP som förval när säkerhetstyp hämtas från anslutningsinställningar

### Oxygen-ikoner

- Lägg till programmeny till Oxygen (fel 365629)

### Plasma ramverk

- Behåll slot createApplet kompatibel med Frameworks 5.24
- Ta inte bort gl-struktur två gånger i miniatyrbild (fel 365946)
- Lägg till översättningsdomän i QML-objekt för skrivbordsunderlägg
- Ta inte manuellt bord miniprogram
- Lägg till en kapptemplate för Plasma skrivbordsunderlägg
- Mallar: Registrera mallar in en egen toppnivåkategori "Plasma/"
- Mallar: Uppdatera länkar till teknikbasens Wiki i README-filer
- Definiera vad Plasma packagestructures utökar plasmashell
- Stöd en storlek för att lägga till miniprogram
- Definiera Plasma PackageStructure som vanliga KPackage PackageStructure insticksprogram
- Rättning: Uppdatera config.xml för exempelskrivbordsunderlägget Höst till QtQuick-kontroller
- Använd KPackage för att installera Plasma-paket
- Om en QIcon skickas som argument till IconItem::Source, använd den
- Lägg till överlagringsstöd till Plasma IconItem
- Lägg till Qt::Dialog till standardflaggor för att få QXcbWindow::isTransient() nöjd (fel 366278)
- [Breeze Plasmatema] Lägg till ikoner för nätverksflygläge på/av
- Skicka contextualActionsAboutToShow innan miniprogrammets meny contextualActions visas (fel 366294)
- [Textfält] Bind till textfältslängd istället för text
- [Knappstilar] Centrera horisontellt i läget enbart ikon (fel 365947)
- [Omgivning] Behandla HiddenStatus som låg status
- Lägg till ikon i systembrickan för Skärmlinjal från Yuri Fabirovsky
- Rätta det ökända felet 'dialogrutor dyker upp i aktivitetshanteraren' en gång till
- Rätta nätverksikonen för trådlös tillgänglighet med ett ? emblem (fel 355490)
- IconItem: Använd bättre tillvägagångssätt för att inaktivera animering vid byte från osynlig till synlig
- Ställ in fönstertyp Tooltip för ToolTipDialog via programmeringsgränssnittet i KWindowSystem

### Solid

- Uppdatera alltid Predicate-tolken från yacc/lex-källkod

### Sonnet

- hunspell: Städa kod för sökning efter ordlistor, lägg till XDG-kataloger (fel 361409)
- Försök att rätta språkfiltrets användning i språkdetektering något

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
