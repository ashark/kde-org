---
aliases:
- ../announce-applications-16.04.3
changelog: true
date: 2016-07-12
description: KDE levererar KDE-program 16.04.3
layout: application
title: KDE levererar KDE-program 16.04.3
version: 16.04.3
---
12:e juli, 2016. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../16.04.0'>KDE-program 16.04</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av bland annat ark, cantor, kate, kdepim och umbrello.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.22.
