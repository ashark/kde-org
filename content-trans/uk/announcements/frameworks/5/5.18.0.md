---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Виправлено декілька вад у пов'язаних із mtime процедурах пошуку
- Ітерація у PostingDB: скасовано оцінку для MDB_NOTFOUND
- Стан balooctl: скасовано показу повідомлення «Індексування даних» щодо тек
- StatusCommand: виправлено показ стану для тек
- SearchStore: реалізовано правильну обробку порожніх значень термінів (виправлено ваду 356176)

### Піктограми Breeze

- оновлення та розширення набору піктограм
- Реалізовано використання піктограм стану розміром у 22 пікселів для розміру у 32 пікселі, якщо це потрібно у системному лотку
- Фіксований розмір для піктограм тек у 32 пікселі у Breeze Dark змінено на масштабований

### Додаткові модулі CMake

- Модуль CMake KAppTemplate зроблено загальним
- Усунено причини попереджень CMP0063 у KDECompilerSettings
- ECMQtDeclareLoggingCategory: у створений файл включено &lt;QDebug&gt;
- Усунено причини попереджень CMP0054

### KActivities

- Спрощено завантаження QML для KCM (виправлено ваду 356832)
- Реалізовано обхідну стратегію щодо вади реалізації SQL у Qt, через яку належним чином не вилучалися з'єднання (виправлено ваду 348194)
- Включено додаток, який виконує програми у відповідь на зміну стану простору дій
- Виконано портування з KService на KPluginLoader
- Додатки портовано на використання kcoreaddons_desktop_to_json()

### KBookmarks

- Реалізовано повну ініціалізацію DynMenuInfo у повернутому значенні

### KCMUtils

- KPluginSelector::addPlugins: виправлено оцінку, якщо параметр «config» має типове значення (виправлено ваду 352471)

### KCodecs

- Усунено довільне переповнення повного буфера

### KConfig

- Забезпечено належне екранування символів у групах kconf_update

### KCoreAddons

- Додано KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Додано KPluginMetaData::copyrightText(), extraInformation() і otherContributors()
- Додано KPluginMetaData::translators() та KAboutPerson::fromJson()
- Виправлено використання після вилучення у обробнику файлів desktop
- Реалізовано побудову KPluginMetaData constructible на основі шляху JSON
- desktoptojson: тепер нестача файла типу служби є помилкою для бібліотеки
- Тепер виклик kcoreaddons_add_plugin без SOURCES є помилкою

### KDBusAddons

- Виконано адаптацію до D-Bus у вторинному потоці у Qt 5.6

### KDeclarative

- [DragArea] Додано властивість dragActive
- [KQuickControlsAddons MimeDatabase] Реалізовано доступ до коментаря QMimeType

### KDED

- kded: виконано адаптацію до того, що потокова dbus:messageFilter у Qt 5.6 має вмикати завантаження модуля у головному потоці обробки

### Підтримка KDELibs 4

- kdelibs4support тепер потребує kded (для kdedmodule.desktop)
- Усунено причини попередження CMP0064 у відповідь на встановлення правила CMP0054 у значення NEW
- Скасовано експортування символів, які вже існують у KWidgetsAddons

### KDESU

- Усунено виток дескриптора файла під час створення сокета

### KHTML

- Windows: вилучено залежність від kdewin

### KI18n

- Створено документацію щодо правила першого аргументу для форм множини у QML
- Зменшено кількість небажаних змін типів
- Уможливлено використання значень подвійної точності (double) як індексів для викликів i18np*() у QML

### KIO

- Виправлено kiod, оскільки потокова dbus:messageFilter у Qt 5.6 має чекати, доки буде завершено завантаження модуля, перш ніж повертати результат
- Змінено код помилки вставлення або пересування до підкаталогу
- Виправлено помилку, пов'язану із блокуванням emptyTrash
- Виправлено помилкову кнопку у KUrlNavigator для віддалених адрес
- KUrlComboBox: виправлено повернення абсолютного шляху функцією urls()
- kiod: вимкнено керування сеансами
- Додано автоматичне доповнення для «.». Тепер пропонується список прихованих файлів та тек (виправлено ваду 354981).
- ktelnetservice: виправлено помилку на одиницю у перевірці argc, латку надано Стівеном Бромлі (Steven Bromley)

### KNotification

- [Сповіщення контекстним вікном] Виправлено пересилання ідентифікатора події
- Встановлено типову непорожню причину для запобігання запуску засобу збереження екрана (виправлено ваду 334525)
- Додано підказку щодо запобігання групуванню сповіщень (виправлено ваду 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Уможливлено позначення окремого запису події

### Бібліотека Package

- Уможливлено надання метаданих у форматі JSON

### KPeople

- Виправлено можливе подвійне вилучення у DeclarativePersonData

### KTextEditor

- Підсвічування синтаксичний конструкцій у pli: додано вбудовані функції та розширювані області

### Бібліотека KWallet

- kwalletd: виправлено виток пам'яті у FILE*

### KWindowSystem

- Додано варіант xcb для статичних методів KStartupInfo::sendFoo

### NetworkManagerQt

- Реалізовано можливість роботи із застарілими версіями NM

### Бібліотеки Plasma

- [ToolButtonStyle] Реалізовано постійний показ activeFocus
- Реалізовано використання прапорця SkipGrouping для сповіщення «віджет вилучено» (виправлено ваду 356653)
- Реалізовано належну обробку символічних посилань у шляхах до пакунків
- Додано HiddenStatus для самостійного приховування плазмоїдів
- Припинено переспрямовування вікон, якщо запис вимкнено або приховано (виправлено ваду 356938)
- Усунено видання сигналу statusChanged, якщо стан не було змінено
- Виправлено ідентифікатори елементів для орієнтації праворуч
- Контейнер: усунено видання сигналу appletCreated для порожнього аплету (виправлено ваду 356428)
- [Інтерфейс контейнера] Виправлено помилку, пов'язану із високоточним гортанням
- Тепер властивість X-Plasma-ComponentTypes KPluginMetada читається як stringlist
- [Мініатюри вікон] Усунено причини аварійного завершення, якщо вимкнено Composite
- Контейнерам тепер дозволено перевизначати дані CompactApplet.qml

Обговорити цей випуск та поділитися ідеями можна у розділі коментарів до <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>статті з новиною</a>.
